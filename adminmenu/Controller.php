<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\adminmenu;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use JTL\Alert\Alert;
use JTL\DB\DbInterface;
use JTL\Helpers\Request;
use JTL\Plugin\PluginInterface;
use JTL\Session\Backend;
use JTL\Shop;
use Plugin\jtl_paypal_commerce\CronJob\CronHelper;
use Plugin\jtl_paypal_commerce\PPC\BackendUIsettings;
use Plugin\jtl_paypal_commerce\PPC\Logger;
use Plugin\jtl_paypal_commerce\paymentmethod\Helper;
use Plugin\jtl_paypal_commerce\PPC\Authorization\AuthorizationException;
use Plugin\jtl_paypal_commerce\PPC\Authorization\MerchantCredentials;
use Plugin\jtl_paypal_commerce\PPC\Authorization\Token;
use Plugin\jtl_paypal_commerce\PPC\Configuration;
use Plugin\jtl_paypal_commerce\PPC\Environment\EnvironmentInterface;
use Plugin\jtl_paypal_commerce\PPC\HttpClient\PPCClient;
use Plugin\jtl_paypal_commerce\PPC\Onboarding\Endpoint;
use Plugin\jtl_paypal_commerce\PPC\Onboarding\OAuthRequest;
use Plugin\jtl_paypal_commerce\PPC\Onboarding\OAuthResponse;
use Plugin\jtl_paypal_commerce\PPC\Onboarding\ReferralsRequest;
use Plugin\jtl_paypal_commerce\PPC\Onboarding\ReferralsResponse;
use Plugin\jtl_paypal_commerce\PPC\Request\UnexpectedResponseException;
use Plugin\jtl_paypal_commerce\PPC\Webhook\Webhook;
use Plugin\jtl_paypal_commerce\PPC\Webhook\WebhookException;

/**
 * Class Controller
 * @package Plugin\jtl_paypal_commerce\adminmenu
 */
final class Controller
{
    /** @var static[] */
    private static $instance;

    /** @var PluginInterface */
    private $plugin;

    /** @var DbInterface */
    private $db;

    /** @var Configuration */
    private $config;

    /** @var Logger */
    private $logger;

    /**
     * Controller constructor.
     * @param PluginInterface $plugin
     * @param DbInterface     $db
     */
    protected function __construct(PluginInterface $plugin, DbInterface $db)
    {
        $this->plugin = $plugin;
        $this->db     = $db;
        $this->config = Shop::Container()->get(Configuration::class);
        $this->logger = new Logger(Logger::TYPE_ONBOARDING);

        self::$instance[$plugin->getPluginID()] = $this;
    }

    /**
     * @param PluginInterface  $plugin
     * @param DbInterface|null $db
     * @return self
     */
    public static function getInstance(PluginInterface $plugin, ?DbInterface $db = null): self
    {
        return self::$instance[$plugin->getPluginID()] ?? new self($plugin, $db ?? Shop::Container()->getDB());
    }

    /**
     * @param array $params
     */
    private function redirectSelf(array $params = []): void
    {
        $params = \array_merge([
            'kPlugin=' . $this->plugin->getID(),
            'kPluginAdminMenu=' . Request::postInt('kPluginAdminMenu'),
            'panelActive=' . Request::postInt('panelActive')
        ], $params);

        $this->redirect($params);
    }

    /**
     * @param array  $params
     * @param string $file
     */
    private function redirect(array $params = [], string $file = 'plugin'): void
    {
        \header(
            'Location: ' . Shop::getURL() . '/' . \PFAD_ADMIN . $file . '.php?' . \implode('&', $params),
            true,
            302
        );
        exit();
    }

    /**
     * @param string     $name
     * @param null|mixed $default
     * @return mixed
     */
    public function getStatic(string $name, $default = null)
    {
        $statics = Backend::get('static.' . $this->plugin->getPluginID(), []);

        return $statics[$name] ?? $default;
    }

    /**
     * @param string $name
     * @param mixed  $value
     */
    public function setStatic(string $name, $value): void
    {
        $statics        = Backend::get('static.' . $this->plugin->getPluginID(), []);
        $statics[$name] = $value;
        Backend::set('static.' . $this->plugin->getPluginID(), $statics);
    }

    /**
     * @param string $name
     */
    public function clearStatic(string $name): void
    {
        $statics = Backend::get('static.' . $this->plugin->getPluginID(), []);
        unset($statics[$name]);
        Backend::set('static.' . $this->plugin->getPluginID(), $statics);
    }

    /**
     * @param string $task
     * @uses runSaveCredentialsManually
     * @uses runSaveSettings
     * @uses runResetCredentialsController
     * @uses runCheckPayment
     * @uses runChangeWorkingMode
     * @uses runFinishOnboarding
     * @uses runCreateWebhook
     * @uses runRefreshWebhook
     * @uses runTestWebhook
     * @uses runResetSettings
     * @uses runDeleteWebhook
     * @uses runChangeShipmentTracking
     * @uses runSaveCarrierMapping
     * @uses runDeleteCarrierMapping
     */
    public function run(string $task): void
    {
        $method = 'run' . \ucfirst($task);
        if (\method_exists($this, $method)) {
            $this->$method();
        }
    }

    /**
     * @return void
     */
    private function runSaveCredentialsManually(): void
    {
        $merchantID   = Request::postVar('ppcManualCredentials', [])['merchantID'] ?? null;
        $clientID     = Request::postVar('ppcManualCredentials', [])['clientID'] ?? null;
        $clientSecret = Request::postVar('ppcManualCredentials', [])['clientSecret'] ?? null;

        if (!empty($clientID) && !empty($clientSecret)) {
            $this->config->saveConfigItems(['merchantID_' . $this->config->getWorkingMode() => $merchantID]);
            $this->config->setClientID($clientID);
            $this->config->setClientSecret($clientSecret);
            try {
                Token::inValidate();
                /** @var EnvironmentInterface $environment */
                $environment = Shop::Container()->get(EnvironmentInterface::class);
                $environment->reInit($clientID, $clientSecret);
                Token::getInstance()->refresh();
            } catch (AuthorizationException $e) {
                Shop::Container()->getAlertService()->addAlert(
                    Alert::TYPE_ERROR,
                    \__('Anmeldung nicht möglich') . ': ' . \__($e->getMessage()),
                    'authFailed',
                    ['saveInSession' => true]
                );
            }
        }

        $this->redirectSelf();
    }

    /**
     * @return void
     */
    private function runSaveSettings(): void
    {
        $settings = Request::postVar('settings', []);

        if (!isset($settings['paymentMethods_enabled'])) {
            $settings['paymentMethods_enabled'] = 'false';
        }

        if (isset($settings)) {
            $this->config->saveConfigItems($settings);
        }

        $this->redirectSelf();
    }

    /**
     * @return void
     */
    private function runResetCredentialsController(): void
    {
        $webhook = new Webhook($this->plugin, $this->config);
        try {
            $webhookId = $webhook->loadWebhook()->getId();
            if ($webhookId !== null) {
                $webhook->deleteWebhook($webhookId);
            }
        } catch (Exception $e) {
        }
        $this->config->setClientID('');
        $this->config->setClientSecret('');
        $this->config->saveConfigItems(
            [
                'merchantEmail_' . $this->config->getWorkingMode() => '',
                'merchantID_' . $this->config->getWorkingMode()    => '',
                'PaymentPUIAvail'  => 0,
                'PaymentACDCAvail' => 0,
            ]
        );
        unset($_POST['kPluginAdminMenu']);

        $this->logger->write(\LOGLEVEL_DEBUG, 'PayPal connection reset! (credentials removed)');
        $this->redirectSelf();
    }

    /**
     * @return void
     */
    private function runCheckPayment(): void
    {
        $factory = Helper::getInstance($this->plugin);
        if (($paymentID = Request::getInt('kZahlungsart')) === 0) {
            $paymentID = Request::postInt('kZahlungsart');
        }
        if ($paymentID > 0 && ($paymentMethod = $factory->getPaymentFromID($paymentID)) !== null) {
            $method = $factory->getMethodFromID($paymentID);
            if ($method !== null) {
                $paymentMethod->validatePaymentConfiguration($method);
            }
        }
    }

    /**
     * @return void
     */
    private function runChangeWorkingMode(): void
    {
        $this->config->setWorkingMode($this->config->getWorkingMode() === 'sandbox'
           ? 'production' : 'sandbox');
        $this->redirectSelf();
    }

    /**
     * @return void
     * @throws GuzzleException
     */
    private function runFinishOnboarding(): void
    {
        $nonce              = Request::getVar('nonce');
        $sharedID           = Request::getVar('sharedID');
        $authCode           = Request::getVar('authCode');
        $merchantIdInPayPal = Request::getVar('merchantIdInPayPal');
        $storedNonce        = $this->config->getNonce();

        if ($storedNonce !== $nonce || empty($storedNonce)) {
            $this->logger->write(\LOGLEVEL_ERROR, 'Onboarding Request failed, wrong nonce.');
            $this->redirectSelf();
        }
        if (isset($sharedID, $authCode)) {
            $this->retrieveClientCredentials($nonce, $sharedID, $authCode);
        }
        if (isset($merchantIdInPayPal)) {
            $this->config->saveConfigItems([
                'merchantID_' . $this->config->getWorkingMode() => $merchantIdInPayPal,
            ]);
            $this->config->setNonce('');

            try {
                $webhook = (new Webhook($this->plugin, $this->config))->createWebhook();
                if (empty($webhook)) {
                    throw new WebhookException('Webhook::create returned no data!');
                }
                $this->config->setWebhookId($webhook->id);
                $this->config->setWebhookUrl($webhook->url);
            } catch (WebhookException $e) {
                $this->logger->write(\LOGLEVEL_ERROR, 'Webhook not created during onboarding: ' . $e);
            }
        }

        $this->redirectSelf();
    }

    /**
     * @return void
     */
    private function runCreateWebhook(): void
    {
        try {
            $webhook = (new Webhook($this->plugin, $this->config))->createWebhook();
            if (empty($webhook)) {
                throw new WebhookException('Webhook::create returned no data!');
            }
            $this->config->setWebhookId($webhook->id);
            $this->config->setWebhookUrl($webhook->url);
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                \__('Webhook efolgreich erstellt.'),
                'runCreateWebhook',
                ['saveInSession' => true]
            );
        } catch (WebhookException $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                \__('Webhook konnte nicht angelegt werden. (Siehe Systemlog für Details)'),
                'runCreateWebhook',
                ['saveInSession' => true]
            );
        }

        $this->redirectSelf();
    }

    /**
     * remove webhook
     */
    private function runDeleteWebhook(): void
    {
        $webhook = new Webhook($this->plugin, $this->config);
        try {
            $webhookId = $webhook->loadWebhook()->getId();
            if (!empty($webhookId)) {
                $webhook->deleteWebhook($webhookId);
                $this->logger->write(\LOGLEVEL_DEBUG, 'Webhook removed: ' . $webhookId);
            }
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                \__('Webhook erfolgreich gelöscht.'),
                'runDeleteWebhook',
                ['saveInSession' => true]
            );
        } catch (WebhookException | UnexpectedResponseException $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                \__('Webhook konnte nicht gelöscht werden. (Siehe Systemlog für Details)'),
                'runDeleteWebhook',
                ['saveInSession' => true]
            );
            $this->logger->write(\LOGLEVEL_NOTICE, 'Errors during webhook remove: ' . $e);
        }

        $this->redirectSelf();
    }

    /**
     * @return void
     */
    private function runRefreshWebhook(): void
    {
        $hook = new Webhook($this->plugin, $this->config);
        try {
            $webhookId = $hook->loadWebhook()->getId();
            if ($webhookId !== null) {
                $hook->deleteWebhook($webhookId);
            }

            $webhook = $hook->createWebhook();
            if (empty($webhook)) {
                throw new WebhookException('Webhook::create returned no data!');
            }
            $this->config->setWebhookId($webhook->id);
            $this->config->setWebhookUrl($webhook->url);

            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                \__('Webhook erfolgreich aktualisiert.'),
                'runRefreshWebhook',
                ['saveInSession' => true]
            );
        } catch (WebhookException | UnexpectedResponseException $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                \__('Webhook konnte nicht aktualisiert werden. (Siehe Systemlog für Details)'),
                'runRefreshWebhook',
                ['saveInSession' => true]
            );
        }

        $this->redirectSelf();
    }

    /**
     * reset default setting given by @see BackendUIsettings
     * @throws Exception
     */
    public function runResetSettings(): void
    {
        $settings        = [];
        $defaultSettings = BackendUIsettings::getDefaultSettings();
        foreach ($defaultSettings as $index => $value) {
            if ($index === 'clientID' || $index === 'clientSecret') {
                continue;
            }
            $settings[$index] = $value['value'];
        }
        $this->config->saveConfigItems($settings);
        $this->logger->write(\LOGLEVEL_DEBUG, 'Reset UI configuration.');
    }

    /**
     * @return void
     */
    private function runChangeShipmentTracking(): void
    {
        $settingName = BackendUIsettings::BACKEND_SETTINGS_SECTION_GENERAL . '_shipmenttracking';

        if ($this->config->getPrefixedConfigItem($settingName, 'N') === 'N') {
            $this->config->saveConfigItems([$settingName => 'Y']);
            CronHelper::createCron(Request::postInt('frequency', 6), Request::postVar('time', '2:00'));
        } else {
            $this->config->saveConfigItems([$settingName => 'N']);
            CronHelper::dropCron();
        }
    }

    /**
     * @return void
     */
    private function runSaveCarrierMapping(): void
    {
        if ((new CarrierMapping($this->db))->addMapping(
            Request::postInt('id'),
            Request::postVar('carrier_wawi'),
            Request::postVar('carrier_paypal')
        ) >= 0) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                \__('Carrier-Mapping gespeichert'),
                'carrierMappingSaved',
                [
                    'saveInSession' => true,
                ]
            );
        } else {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                \__('Carrier-Mapping konnte nicht gespeichert werden'),
                'carrierMappingFailed',
                [
                    'saveInSession' => true,
                ]
            );
        }

        $this->redirectSelf();
    }

    /**
     * @return void
     */
    private function runDeleteCarrierMapping(): void
    {
        if ((new CarrierMapping($this->db))->deleteMapping(Request::postInt('id')) >= 0) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                \__('Carrier-Mapping gelöscht'),
                'carrierMappingSaved',
                [
                    'saveInSession' => true,
                ]
            );
        } else {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                \__('Carrier-Mapping konnte nicht gelöscht werden'),
                'carrierMappingFailed',
                [
                    'saveInSession' => true,
                ]
            );
        }

        $this->redirectSelf();
    }

    /**
     * @param $nonce
     * @param $sharedID
     * @param $authCode
     * @throws GuzzleException
     */
    private function retrieveClientCredentials($nonce, $sharedID, $authCode): void
    {
        if (!isset($nonce, $sharedID, $authCode)) {
            $this->logger->write(\LOGLEVEL_ERROR, 'Onboarding Request failed, missing parameters.');
            $this->redirectSelf();
        }

        $client = new PPCClient(Shop::Container()->get(EnvironmentInterface::class));
        $this->logger->write(\LOGLEVEL_DEBUG, 'Onboarding data retrieved.');

        try {
            $oAuthResponse     = new OAuthResponse(
                $client->send(new OAuthRequest($sharedID, $nonce, $authCode))
            );
            $referralsResponse = new ReferralsResponse(
                $client->send(new ReferralsRequest(
                    \base64_decode(MerchantCredentials::partnerID($this->config->getWorkingMode())),
                    $oAuthResponse->getToken(),
                    Endpoint::PARTNER_CREDENTIALS
                ))
            );
            $clientId          = $referralsResponse->getClientId();
            $clientSecret      = $referralsResponse->getClientSecret();

            if (!empty($clientId) && !empty($clientSecret)) {
                $this->config->setClientID($clientId);
                $this->config->setClientSecret($clientSecret);

                $this->logger->write(\LOGLEVEL_DEBUG, 'Onboarding was successfull.');
            }
        } catch (Exception $e) {
            $this->logger->write(\LOGLEVEL_ERROR, $e->getMessage());
        }
    }
}
