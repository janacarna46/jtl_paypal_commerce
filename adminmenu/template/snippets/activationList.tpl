{if !is_array($setting['value'])}
    {assign var="enabledMethods" value=","|explode:$setting['value']}
{else}
    {assign var="enabledMethods" value=$setting['value']}
{/if}
<style>
    .activation-switch .custom-control-input:checked ~ .custom-control-label::before,
    .activation-switch .custom-control-input.checked ~ .custom-control-label::before {
        color: #435a6b;
        border-color: #435a6b;
        background-color: #5cbcf6;
    }
    .activation-switch.custom-switch .custom-control-input:checked ~ .custom-control-label::after,
    .activation-switch.custom-switch .custom-control-input.checked ~ .custom-control-label::after {
        background-color: #ffffff;
        transform: translateX(0.75rem);
    }
    .activation-switch .custom-control-input:focus:not(:checked) ~ .custom-control-label::before {
        border-color: #435a6b;
    }
</style>
<div id="component_{$settingsName}" class="table-responsive {$setting['class']}">
    {if isset($setting.vars.selectGroups) && count($setting.vars.selectGroups) > 0}
        <div class="pl-sm-5 pr-sm-5 pb-sm-5">
            <div class="row text-sm-left">
                <div class="col ml-2 custom-control custom-switch activation-switch">
                    <input type="checkbox" class="custom-control-input selectGroup-switch" value="all" id="selectgroups_all" aria-label="{__('Alle')}">
                    <label class="custom-control-label" for="selectgroups_all">{__('Alle')}</label>
                </div>
                <div class="col ml-2 custom-control custom-switch activation-switch">
                    <input type="checkbox" class="custom-control-input selectGroup-switch" value="none" id="selectgroups_none" aria-label="{__('Keine')}">
                    <label class="custom-control-label" for="selectgroups_none">{__('Keine')}</label>
                </div>
                {foreach $setting.vars.selectGroups as $group => $val}
                    <div class="col ml-2 custom-control custom-switch activation-switch">
                        <input type="checkbox" class="custom-control-input selectGroup-switch" value="{$group}" id="selectgroups_{$group}" aria-label="{__($group)}">
                        <label class="custom-control-label" for="selectgroups_{$group}">{__($group)}</label>
                    </div>
                {/foreach}
            </div>
        </div>
    {/if}
    <table class="table table-striped activation-table">
        <thead>
            <tr>
                <th>#</th>
                <th class="text-nowrap">
                    {$setting['label']}&nbsp;<span data-html="true" data-toggle="tooltip" data-placement="left" title="{$setting['description']}" data-original-title="{$setting['description']}">
                        <span class="fas fa-info-circle fa-fw"></span>
                    </span>
                </th>
                <th>{if isset($setting['label2'])}{$setting['label2']}{/if}</th>
                <th>{__('Aktiv')}</th>
            </tr>
        </thead>
        <tbody>
        {foreach $setting['options'] as $options}
            <tr>
                <td></td>
                <td>{$options.label}</td>
                <td>
                    {if isset($options['extended'])}
                        {$options['extended'][$options.value]}
                    {/if}
                </td>
                <td>
                    <div class="ml-2 custom-control custom-switch activation-switch">
                        <input type="checkbox" class="custom-control-input" id="setting_{$settingsName}_{$options.value}"
                               name="settings[{$settingsName}][]"
                               value="{$options.value}"
                               aria-label="{$options.label} {__('aktiv')}"
                                {if in_array($options['value'], $enabledMethods)} checked="checked" aria-checked="true"{/if}
                        >
                        <label class="custom-control-label" for="setting_{$settingsName}_{$options.value}">&nbsp;</label>
                    </div>
                </td>
            </tr>
        {/foreach}
        </tbody>
    </table>
</div>
{if isset($setting.vars.selectGroups) && count($setting.vars.selectGroups) > 0}
<script>
    let selectGroups        = {json_encode($setting.vars.selectGroups)},
        $activationSwitches = $('.activation-table .activation-switch .custom-control-input');
    {literal}
    $('.selectGroup-switch').on('change', function(e) {
        let $target = $(e.target);
        if (e.target.value === 'all' && $target.prop('checked')) {
            $activationSwitches.prop('checked', true);
            checkActivationSelection();
        } else if (e.target.value === 'none' && $target.prop('checked')) {
            $activationSwitches.prop('checked', false);
            checkActivationSelection();
        } else {
            for (let group in selectGroups[e.target.value]) {
                $('.activation-table .activation-switch .custom-control-input[value="' + selectGroups[e.target.value][group] + '"]')
                    .prop('checked', $target.prop('checked'))
            }
            checkActivationSelection();
        }
    });
    $activationSwitches.on('change', function(e) {
        checkActivationSelection();
    });
    function checkActivationSelection() {
        let $checkedSelections = $('.activation-table .activation-switch .custom-control-input:checked');
        $('#selectgroups_all').prop('checked', $checkedSelections.length === $activationSwitches.length);
        $('#selectgroups_none').prop('checked', $checkedSelections.length === 0);
        for (let group in selectGroups) {
            let activated = 0;
            for (let part in selectGroups[group]) {
                if ($('.activation-table .activation-switch .custom-control-input[value="' + selectGroups[group][part] + '"]:checked').length) {
                    ++activated;
                }
            }
            $('.selectGroup-switch[value="' + group + '"]')
                .prop('checked', selectGroups[group].length === activated);
        }
    }
    checkActivationSelection();
    {/literal}
</script>
{/if}
