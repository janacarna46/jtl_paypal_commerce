<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\adminmenu;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use JTL\Alert\Alert;
use JTL\Helpers\Request;
use JTL\Plugin\PluginInterface;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_paypal_commerce\CronJob\CronHelper;
use Plugin\jtl_paypal_commerce\Onboarding\Onboarding;
use Plugin\jtl_paypal_commerce\paymentmethod\Helper;
use Plugin\jtl_paypal_commerce\PPC\APM;
use Plugin\jtl_paypal_commerce\PPC\Authorization\AuthorizationException;
use Plugin\jtl_paypal_commerce\PPC\Authorization\MerchantCredentials;
use Plugin\jtl_paypal_commerce\PPC\Authorization\Token;
use Plugin\jtl_paypal_commerce\PPC\BackendUIsettings;
use Plugin\jtl_paypal_commerce\PPC\Configuration;
use Plugin\jtl_paypal_commerce\PPC\Environment\EnvironmentInterface;
use Plugin\jtl_paypal_commerce\PPC\HttpClient\PPCClient;
use Plugin\jtl_paypal_commerce\PPC\Logger;
use Plugin\jtl_paypal_commerce\PPC\Onboarding\Endpoint;
use Plugin\jtl_paypal_commerce\PPC\Onboarding\ReferralsRequest;
use Plugin\jtl_paypal_commerce\PPC\Onboarding\ReferralsResponse;
use Plugin\jtl_paypal_commerce\PPC\Request\UnexpectedResponseException;
use Plugin\jtl_paypal_commerce\PPC\Tracking\Tracker;
use Plugin\jtl_paypal_commerce\PPC\Webhook\Webhook;
use Plugin\jtl_paypal_commerce\PPC\Webhook\WebhookException;

/**
 * Class Renderer
 * @package Plugin\jtl_paypal_commerce\adminmenu
 */
final class Renderer
{
    public const TAB_MAPPINGS = [
        'Infos'                 => 'infos',
        'Zugangsdaten'          => 'credentials',
        'Einstellungen'         => 'settings',
        'Versandinformationen'  => 'shipmentState',
        'Webhook'               => 'webhook',
    ];

    /** @var PluginInterface */
    private PluginInterface $plugin;

    /** @var int */
    private int $menuID;

    /** @var JTLSmarty */
    private JTLSmarty $smarty;

    /** @var Configuration */
    private Configuration $config;

    /** @var Logger */
    private Logger $logger;

    private const JTL_LOGO_URL   = 'img/JTL-Shop-Logo-rgb.svg';
    private const PPOB_URL       = 'https://www.paypal.com/bizsignup/partner/entry';
    private const PPOB_SB_URL    = 'https://www.sandbox.paypal.com/bizsignup/partner/entry';
    private const PPOB_SCRIPT    = 'https://paypal.com/%s/webapps/merchantboarding/js/lib/lightbox/partner.js';
    private const PPOB_SB_SCRIPT = 'https://sandbox.paypal.com/%s/webapps/merchantboarding/js/lib/lightbox/partner.js';

    /**
     * Renderer constructor.
     * @param PluginInterface $plugin
     * @param int             $menuID
     * @param JTLSmarty       $smarty
     */
    public function __construct(PluginInterface $plugin, int $menuID, JTLSmarty $smarty)
    {
        $this->plugin = $plugin;
        $this->menuID = $menuID;
        $this->smarty = $smarty;
        $this->config = Shop::Container()->get(Configuration::class);
        $this->logger = new Logger(Logger::TYPE_ONBOARDING);
    }

    /**
     * @param string $tabName
     * @return string
     * @throws TabNotAvailException
     */
    public function render(string $tabName): string
    {
        $method = 'render' . \ucfirst($tabName);
        try {
            if (\method_exists($this, $method)) {
                $this->$method();
            }

            return $this->smarty
                ->assign('kPlugin', $this->plugin->getID())
                ->assign('kPluginAdminMenu', $this->menuID)
                ->fetch($this->plugin->getPaths()->getAdminPath() . '/template/' . $tabName . '.tpl');
        } catch (TabNotAvailException $e) {
            throw $e;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @throws TabNotAvailException
     */
    private function checkTabRendering(): void
    {
        $payment = Helper::getInstance($this->plugin)->getPaymentFromName('PayPalCommerce');
        if ($payment === null || !$payment->isValidIntern()) {
            throw new TabNotAvailException(\sprintf(
                \__('Die Zahlungsmethode %s ist nicht verfügbar.'),
                'PayPalCommerce'
            ));
        }
    }

    /**
     * @return void
     * @noinspection PhpUnusedPrivateMethodInspection
     * @throws Exception
     */
    private function renderInfos(): void
    {
        $isAuthConfigured = $this->config->isAuthConfigured();

        try {
            $token = $isAuthConfigured ? Token::getInstance() : null;
        } catch (AuthorizationException $e) {
            $token = null;
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_WARNING,
                \__('Anmeldung bei PayPal fehlgeschlagen.'),
                'authFailed'
            );
        }
        if ($token !== null) {
            try {
                $merchantID = $this->config->getPrefixedConfigItem('merchantID_' . $this->config->getWorkingMode());
                $this->getAdditionalMerchantInformations($merchantID, $token->getToken());
            } catch (GuzzleException | Exception $e) {
                $this->logger->write(\LOGLEVEL_ERROR, $e->getMessage());
            }
        }

        $locale             = Helper::sanitizeLocale(Shop::Container()->getGetText()->getLanguage(), true);
        $ppPopupLanguage    = Shop::getCurAdminLangTag() === 'de-DE' ? 'DE' : 'US';
        $workingMode        = $this->config->getWorkingMode();
        $partnerCredentials = [
            'partnerID'       => MerchantCredentials::partnerID($workingMode),
            'partnerClientID' => MerchantCredentials::partnerClientID($workingMode),
            'nonce'           => '',
            'fetchUrl'        => ''
        ];

        if ($isAuthConfigured === false) {
            $isSandbox                      = $workingMode === Configuration::WORKING_MODE_SANDBOX;
            $partnerCredentials['nonce']    = \bin2hex(\random_bytes(40));
            $partnerCredentials['fetchUrl'] = Onboarding::getOnboardingFetchURL($this->plugin);
            $this->config->setNonce($partnerCredentials['nonce']);

            $ppc_partnerLogoUrl = $this->plugin->getPaths()->getFrontendURL() . self::JTL_LOGO_URL;
            $onboardingUrl      = \sprintf($isSandbox ? self::PPOB_SB_URL : self::PPOB_URL, $ppPopupLanguage);
            $scriptUrl          = \sprintf($isSandbox ? self::PPOB_SB_SCRIPT : self::PPOB_SCRIPT, $ppPopupLanguage);
            $ppc_features       = [
                  'PAYMENT'
                , 'REFUND'
                , 'ACCESS_MERCHANT_INFORMATION'
                , 'DELAY_FUNDS_DISBURSEMENT'
                , 'PARTNER_FEE'
                , 'ADVANCED_TRANSACTIONS_SEARCH'
            ];
            $ppc_products       = ['payment_methods'];
            $ppc_capabilities   = ['PAY_UPON_INVOICE'];
            $onboardingUri      = $onboardingUrl . '?channelId=partner'
                . '&partnerId=' . \base64_decode($partnerCredentials['partnerID'])
                . '&showPermissions=true'
                . '&integrationType=FO'
                . '&features=' . \implode(',', $ppc_features)
                . '&product=ppcp'
                . '&secondaryProducts=' . \implode(',', $ppc_products)
                . '&capabilities=' . \implode(',', $ppc_capabilities)
                . '&partnerClientId=' . \base64_decode($partnerCredentials['partnerClientID'])
                . '&partnerLogoUrl=' . \urlencode($ppc_partnerLogoUrl)
                . '&returnToPartnerUrl=' . \urlencode($partnerCredentials['fetchUrl']
                    . '?nonce=' . $partnerCredentials['nonce']
                    . '&kPlugin=' . $this->plugin->getID())
                . '&displayMode=minibrowser'
                . '&sellerNonce=' . $partnerCredentials['nonce']
                . '&country.x=' . $ppPopupLanguage
                . '&locale.x=' . $locale;

            $this->logger->write(\LOGLEVEL_DEBUG, 'Onboard with: ', \str_replace('&', "\n&", $onboardingUri));

            $this->smarty
                ->assign('partnerCredentials', $partnerCredentials)
                ->assign('cookieSettings', \session_get_cookie_params())
                ->assign('onboardingUri', $onboardingUri)
                ->assign('scriptUrl', $scriptUrl);
        }

        $payMethods = $this->plugin->getPaymentMethods()->getMethods();
        $ppcMethods = [];
        foreach ($payMethods as $method) {
            $payment = Helper::getInstance($this->plugin)->getPaymentFromID($method->getMethodID());
            if ($payment !== null) {
                $payment->renderBackendInformation($this->smarty, $this->plugin, $this);
                $ppcMethods[] = $payment;
            }
        }

        $this->smarty
            ->assign('locale', $locale)
            ->assign('baseUrl', $this->plugin->getPaths()->getBaseURL())
            ->assign('basePath', $this->plugin->getPaths()->getBasePath())
            ->assign('isAuthConfigured', $isAuthConfigured)
            ->assign('ppcMerchantID', $this->config->getPrefixedConfigItem('merchantID_'
                . $this->config->getWorkingMode()))
            ->assign('ppcClientID', $this->config->getClientID())
            ->assign('ppcClientSecret', $this->config->getClientSecret())
            ->assign('ppcToken', $token !== null ? $token->getToken() : '')
            ->assign('ppc_methods', $ppcMethods)
            ->assign('ppc_mode', $workingMode)
            ->assign('paymentAlertList', Shop::Container()->getAlertService()->getAlertlist()->filter(
                static function (Alert $item) {
                    return $item->getShowInAlertListTemplate() === false;
                }
            ));
    }

    /**
     * @return void
     * @throws TabNotAvailException
     * @noinspection PhpUnusedPrivateMethodInspection
     */
    private function renderCredentials(): void
    {
        $this->checkTabRendering();

        try {
            $config = $this->config->mapBackendSettings(BackendUIsettings::BACKEND_SETTINGS_SECTION_CREDENTIALS);
        } catch (Exception $e) {
            throw new TabNotAvailException($e->getMessage(), $e->getCode(), $e);
        }

        $config[BackendUIsettings::BACKEND_SETTINGS_SECTION_CREDENTIALS]['heading'] = \__('PayPal-Account') . ' : '
            . $this->config->getPrefixedConfigItem('merchantEmail_' . $this->config->getWorkingMode());

        $this->smarty->assign('settingSections', $config);
    }

    /**
     * @return void
     * @throws TabNotAvailException
     * @noinspection PhpUnusedPrivateMethodInspection
     */
    private function renderWebhook(): void
    {
        $this->checkTabRendering();

        $paymethod = Helper::getInstance($this->plugin)->getPaymentFromName('PayPalCommerce');
        if ($paymethod === null) {
            throw new TabNotAvailException(\sprintf(
                \__('Die Zahlungsmethode %s ist nicht verfügbar.'),
                'PayPalCommerce'
            ));
        }
        $isWebhookRegistred = false;
        $webhook            = new Webhook($this->plugin, $this->config);
        try {
            $webhookRemote = $webhook->loadWebhook();
            $webhookURL    = $webhookRemote->getUrl();
            $webhookEvents = $webhookRemote->getEventTypes();
            $webhookShopId = $this->config->getWebhookId();
            if (!empty($webhookURL)) {
                $isWebhookRegistred = ($webhookURL === $webhook->getWebHookURL());
            }
        } catch (WebhookException | UnexpectedResponseException $e) {
            $webhookShopId = null;
            $this->logger->write(\LOGLEVEL_ERROR, $e->getMessage());
        }

        $this->smarty
            ->assign('isWebhookConfigured', !empty($webhookShopId))
            ->assign('isWebhookRegistred', $isWebhookRegistred)
            ->assign('PaymentMethod', $paymethod->getMethod()->getName())
            ->assign('webhookID', $webhookShopId)
            ->assign('webhookURL', $webhook->getWebHookURL())
            ->assign('webhookEvents', $webhookEvents ?? []);
    }

    /**
     * @return void
     * @noinspection PhpUnusedPrivateMethodInspection
     * @throws TabNotAvailException
     */
    private function renderSettings(): void
    {
        $this->checkTabRendering();
        try {
            $this->smarty
                ->assign(
                    'settingsPanels',
                    $this->config->mapBackendSettings(null, [BackendUIsettings::BACKEND_SETTINGS_SECTION_CREDENTIALS])
                )
                ->assign('clientID', $this->config->getClientID())
                ->assign('panelActive', Request::getInt('panelActive'));
        } catch (Exception $e) {
            throw new TabNotAvailException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @return void
     * @noinspection PhpUnusedPrivateMethodInspection
     * @throws TabNotAvailException
     */
    private function renderShipmentState(): void
    {
        $this->checkTabRendering();
        $settingName = BackendUIsettings::BACKEND_SETTINGS_SECTION_GENERAL . '_shipmenttracking';
        $cron        = CronHelper::getCron();
        $frequency   = 6;
        $cronLink    = '';
        Shop::Container()->getGetText()->loadAdminLocale('pages/cron');

        if ($cron !== null) {
            $frequency = $cron->getFrequency();
            $cronLink  = ' (<a class="small" href="' . Shop::getAdminURL() . '/cron.php' . '">'
                . \__($cron->getType()) . '</a>)';
        }
        $this->smarty
            ->assign('settingDescription', \__(
                'Die Versandinformationen umfassen Tracking-ID, Versandart und Versanddatum',
                $frequency,
                $cronLink
            ))
            ->assign('settingName', $settingName)
            ->assign('trackingEnabled', $this->config->getPrefixedConfigItem($settingName, 'N'))
            ->assign('mappingItems', (new CarrierMapping(Shop::Container()->getDB()))->getMappings())
            ->assign('paypalCarriers', Tracker::CARRIERS);
    }

    /**
     * @param $merchantID
     * @param $token
     * @throws GuzzleException
     */
    private function getAdditionalMerchantInformations($merchantID, $token): void
    {
        if (!isset($merchantID, $token)
            || !empty($this->config->getPrefixedConfigItem('merchantEmail_' . $this->config->getWorkingMode()))) {
            return;
        }

        $client = new PPCClient(Shop::Container()->get(EnvironmentInterface::class));
        try {
            $merchantInfoResponse = new ReferralsResponse(
                $client->send(new ReferralsRequest(
                    \base64_decode(MerchantCredentials::partnerID($this->config->getWorkingMode())),
                    $token,
                    Endpoint::PARTNER_MERCHANTINFO . $merchantID
                ))
            );

            foreach ([
                'PaymentPUI'  => ['PAYMENT_METHODS', 'PAY_UPON_INVOICE'],
                'PaymentACDC' => ['PPCP_CUSTOM', 'CUSTOM_CARD_PROCESSING'],
             ] as $payment => $productName) {
                $avail          = false;
                $limited        = false;
                $paymentProduct = $merchantInfoResponse->getProductByName($productName[0]);
                if ($paymentProduct !== null && \in_array($productName[1], $paymentProduct->getCapabilities(), true)) {
                    $product = $merchantInfoResponse->getCapabilityByName($productName[1]);
                    $avail   = $product !== null && $product->isActive();
                    $limited = $avail && $product->hasLimits();
                }
                $this->config->saveConfigItems([
                    $payment . 'Avail' => $avail ? '1' : '0',
                    $payment . 'Limit' => $limited ? '1' : '0',
                ]);
            }

            $this->config->saveConfigItems([
                'merchantEmail_' . $this->config->getWorkingMode() => $merchantInfoResponse->getTrackingId(),
            ]);
            $acdcAvail = (int)$this->config->getPrefixedConfigItem('PaymentACDCAvail', '0');
            if ($acdcAvail > 0) {
                $apm     = new APM($this->config);
                $enabled = $apm->getEnabled(false);
                $apm->setEnabled(\array_diff($enabled, [APM::CREDIT_CARD]));
            }
        } catch (Exception $e) {
            $this->logger->write(\LOGLEVEL_ERROR, $e->getMessage());
        }
    }
}
