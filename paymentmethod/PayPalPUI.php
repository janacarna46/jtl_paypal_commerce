<?php /** @noinspection PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\paymentmethod;

use DateInterval;
use DateTime;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use InvalidArgumentException;
use JsonException;
use JTL\Alert\Alert;
use JTL\Backend\NotificationEntry;
use JTL\Cart\Cart;
use JTL\Catalog\Product\Artikel;
use JTL\Checkout\Bestellung;
use JTL\Checkout\Lieferadresse;
use JTL\Customer\Customer;
use JTL\Customer\CustomerGroup;
use JTL\DB\ReturnType;
use JTL\Firma;
use JTL\Helpers\Request;
use JTL\Link\LinkInterface;
use JTL\Plugin\PluginInterface;
use JTL\Session\Frontend;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_paypal_commerce\adminmenu\Renderer;
use Plugin\jtl_paypal_commerce\frontend\Handler;
use Plugin\jtl_paypal_commerce\frontend\PaymentFrontendInterface;
use Plugin\jtl_paypal_commerce\frontend\PUIFrontend;
use Plugin\jtl_paypal_commerce\PPC\Authorization\AuthorizationException;
use Plugin\jtl_paypal_commerce\PPC\Authorization\MerchantCredentials;
use Plugin\jtl_paypal_commerce\PPC\Authorization\Token;
use Plugin\jtl_paypal_commerce\PPC\Configuration;
use Plugin\jtl_paypal_commerce\PPC\Order\Amount;
use Plugin\jtl_paypal_commerce\PPC\Order\AppContext;
use Plugin\jtl_paypal_commerce\PPC\Order\Capture;
use Plugin\jtl_paypal_commerce\PPC\Order\ExperienceContext;
use Plugin\jtl_paypal_commerce\PPC\Order\Order;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderStatus;
use Plugin\jtl_paypal_commerce\PPC\Order\Payer;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Transaction;
use Plugin\jtl_paypal_commerce\PPC\Request\PPCRequestException;
use Plugin\jtl_paypal_commerce\PPC\Request\UnexpectedResponseException;

/**
 * Class PayPalPUI
 * @package Plugin\jtl_paypal_commerce\paymentmethod
 */
class PayPalPUI extends PayPalPayment
{
    /**
     * @inheritDoc
     */
    public function paymentDuringOrderSupported(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function paymentAfterOrderSupported(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function mappedLocalizedPaymentName(?string $isoCode = null): string
    {
        return Handler::getBackendTranslation('Rechnungskauf');
    }

    /**
     * @inheritDoc
     */
    protected function usePurchaseItems(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    protected function isSessionPayed(object $paymentSession): bool
    {
        return empty($paymentSession->txn_id);
    }

    /**
     * @inheritDoc
     */
    public function getFrontendInterface(Configuration $config, JTLSmarty $smarty): PaymentFrontendInterface
    {
        return new PUIFrontend($this->plugin, $this, $smarty);
    }

    /**
     * @return bool
     */
    private function checkPUIReferrals(): bool
    {
        $response = $this->getMerchantIntegration($this->config);
        if ($response === null) {
            return false;
        }

        $paymentProduct = $response->getProductByName('PAYMENT_METHODS');
        $puiAvail       = false;
        $limited        = false;
        if ($paymentProduct !== null && \in_array('PAY_UPON_INVOICE', $paymentProduct->getCapabilities(), true)) {
            $pui      = $response->getCapabilityByName('PAY_UPON_INVOICE');
            $puiAvail = $pui !== null && $pui->isActive();
            $limited  = $puiAvail && $pui->hasLimits();
        }
        $this->config->saveConfigItems([
            'PaymentPUIAvail' => $puiAvail ? '1' : '0',
            'PaymentPUILimit' => $limited ? '1' : '0',
        ]);

        return $puiAvail;
    }

    /**
     * @return string
     */
    private function getCustomerServiceInstructions(): string
    {
        $csi = $this->plugin->getLocalization()
                     ->getTranslation('pui_customer_service_instructions');
        if (\strpos($csi, '%s') !== false) {
            $csi = \sprintf($csi, $this->getShopTitle()
                . ' (' . Shop::Container()->getLinkService()->getStaticRoute() . ')');
        }

        return $csi;
    }

    /**
     * @param Bestellung    $order
     * @param PaymentSource $paymentSource
     * @param Amount        $amount
     */
    private function updatePUIOrderData(Bestellung $order, PaymentSource $paymentSource, Amount $amount): void
    {
        $date        = (new DateTime())->add(new DateInterval('P30D')); // hardcoded 30 days
        $company     = new Firma();
        $bankDetails = $paymentSource->getBankDetails();
        $puiData     = [
            '%reference_number%'                  => $paymentSource->getPaymentReference(),
            '%bank_name%'                         => $bankDetails->getBankName(),
            '%account_holder_name%'               => $bankDetails->getAccountHolder(),
            '%international_bank_account_number%' => $bankDetails->getIBAN(),
            '%bank_identifier_code%'              => $bankDetails->getBIC(),
            '%value%'                             => \number_format(
                $amount->getValue(),
                2,
                Frontend::getCurrency()->getDecimalSeparator(),
                ''
            ),
            '%currency%'                          => $amount->getCurrencyCode(),
            '%payment_due_date%'                  => $date->format('d.m.Y'),
            '%company%'                           => $company->cName,
        ];

        $order->cPUIZahlungsdaten = \str_replace(
            \array_keys($puiData),
            \array_values($puiData),
            \sprintf(
                "%s\r\n\r\n%s",
                \str_replace(
                    '<br>',
                    "\r\n",
                    $this->plugin->getLocalization()->getTranslation('jtl_paypal_pui_banktransfer')
                ),
                \str_replace(
                    '<br>',
                    "\r\n",
                    $this->plugin->getLocalization()->getTranslation('jtl_paypal_pui_legal')
                )
            )
        );
        Shop::Container()->getDB()->update('tbestellung', 'kBestellung', $order->kBestellung, (object)[
            'cPUIZahlungsdaten' => $order->cPUIZahlungsdaten,
            'cAbgeholt'         => 'N',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function isValidIntern(array $args_arr = []): bool
    {
        if (!parent::isValidIntern($args_arr)) {
            return false;
        }

        $puiAvail = (int)$this->config->getPrefixedConfigItem('PaymentPUIAvail', '0');
        $conf     = Shop::getSettings([\CONF_KUNDEN]);
        if (($args_arr['doOnlineCheck'] ?? false)) {
            $puiAvail = $this->checkPUIReferrals();
        }

        if (!($args_arr['checkConnectionOnly'] ?? false)
            && ($conf['kunden']['kundenregistrierung_abfragen_geburtstag'] === 'N'
            || $conf['kunden']['kundenregistrierung_abfragen_tel'] === 'N')) {
            $puiAvail = 0;
        }
        try {
            return $puiAvail > 0
                && $this->method->getDuringOrder()
                && Token::getInstance()->getToken() !== null;
        } catch (AuthorizationException $e) {
            $this->logger->write(\LOGLEVEL_ERROR, 'AuthorizationException:' . $e->getMessage());

            return false;
        }
    }

    /**
     * @inheritDoc
     */
    public function isValid(object $customer, Cart $cart): bool
    {
        if (parent::isValid($customer, $cart)) {
            $cartTotal = $cart->gibGesamtsummeWarenOhne([\C_WARENKORBPOS_TYP_VERSANDPOS], true);
            if (!($customer instanceof Customer)) {
                $customer = new Customer($customer->kKunde);
            }
            $customerGroup = new CustomerGroup($customer->getGroupID());

            return (
                !$customerGroup->isMerchant()
                && $customer->cLand === 'DE'
                && $cartTotal >= 5 && $cartTotal <= 2500
            );
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function isValidExpressPayment(object $customer, Cart $cart): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function isValidExpressProduct(object $customer, ?Artikel $product): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function isValidBannerPayment(object $customer, Cart $cart): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function isValidBannerProduct(object $customer, ?Artikel $product): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function getBackendNotification(PluginInterface $plugin, bool $force = false): ?NotificationEntry
    {
        $entry = parent::getBackendNotification($plugin);
        if ($entry !== null) {
            return $entry;
        }

        if (!($force || $this->isAssigned())) {
            return null;
        }

        $puiAvail = (int)$this->config->getPrefixedConfigItem('PaymentPUIAvail', '0');
        if ($puiAvail === 0) {
            $entry = new NotificationEntry(
                NotificationEntry::TYPE_DANGER,
                \__($this->method->getName()),
                \__('Rechnungskauf wird von Ihrem PayPal-Account nicht unterstützt.'),
                Shop::getAdminURL() . '/plugin.php?kPlugin=' . $this->plugin->getID()
            );
            $entry->setPluginId($plugin->getPluginID());

            return $entry;
        }

        $conf = Shop::getSettings([\CONF_KUNDEN]);
        if ($conf['kunden']['kundenregistrierung_abfragen_geburtstag'] === 'N'
            || $conf['kunden']['kundenregistrierung_abfragen_tel'] === 'N'
        ) {
            $entry = new NotificationEntry(
                NotificationEntry::TYPE_DANGER,
                \__($this->method->getName()),
                \__('Angabe von Telefonnummer und Geburtsdatum ist erforderlich'),
                Shop::getAdminURL() . '/einstellungen.php?kSektion=' . \CONF_KUNDEN
            );
            $entry->setPluginId($plugin->getPluginID());

            return $entry;
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function renderBackendInformation(JTLSmarty $smarty, PluginInterface $plugin, Renderer $renderer): void
    {
        parent::renderBackendInformation($smarty, $plugin, $renderer);

        if (!$this->config->isAuthConfigured() || !$this->isAssigned()) {
            return;
        }

        $puiAvail = (int)$this->config->getPrefixedConfigItem('PaymentPUIAvail', '0');
        if ($puiAvail === 0) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_DANGER,
                \sprintf(
                    \__('Die Zahlungsart Rechnungskauf wird von Ihrem PayPal-Account nicht unterstützt'),
                    '<strong>' . \__($this->method->getName()) . '</strong><br>',
                    \__($this->method->getName())
                ),
                'puiNotSupported',
                [
                    'showInAlertListTemplate' => false,
                ]
            );

            return;
        }

        $conf = Shop::getSettings([\CONF_KUNDEN]);
        if ($conf['kunden']['kundenregistrierung_abfragen_geburtstag'] === 'N'
            || $conf['kunden']['kundenregistrierung_abfragen_tel'] === 'N'
        ) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_WARNING,
                \sprintf(
                    \__('Für %s sind zusätzliche Angaben erforderlich'),
                    '<strong>' . \__($this->method->getName()) . '</strong><br>'
                ),
                'puiNotSupported',
                [
                    'showInAlertListTemplate' => false,
                ]
            );

            return;
        }

        $puiLimit = (int)$this->config->getPrefixedConfigItem('PaymentPUILimit', '0');
        if ($puiLimit === 1) {
            $this->logger->write(\LOGLEVEL_NOTICE, \sprintf(
                \__('Die Zahlungsart "%s" steht momentan nur eingeschränkt zur Verfügung'),
                \__($this->method->getName())
            ));
        }
    }

    /**
     * @inheritDoc
     */
    public function validatePayerData(Customer $customer, ?Lieferadresse $shippingAdr = null, ?Cart $cart = null): void
    {
        parent::validatePayerData($customer, $shippingAdr, $cart);

        $puiLegalInfoShown = Request::postVar('puiLegalInfoShown');
        if ($puiLegalInfoShown !== null) {
            $this->addCache('puiLegalInfoShown', $puiLegalInfoShown);
        }

        if ((int)$this->getCache('puiLegalInfoShown') !== 1) {
            throw new InvalidPayerDataException(
                Handler::getBackendTranslation('Bitte bestätigen Sie die Rechtlichen Informationen.'),
                Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php')
                . '?editVersandart=1'
            );
        }
        $customerGroup = new CustomerGroup($customer->getGroupID());
        if ($customerGroup->isMerchant()) {
            throw new InvalidPayerDataException(
                \sprintf(
                    Handler::getBackendTranslation('%s ist nur für Privatkunden verfügbar'),
                    $this->getMethod()->getName()
                ),
                Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php')
                . '?editRechnungsadresse=1'
            );
        }

        if ($customer->cLand !== 'DE') {
            throw new InvalidPayerDataException(
                \sprintf(
                    Handler::getBackendTranslation('%s ist nur mit einer Rechnungsanschrift in Deutschland verfügbar'),
                    $this->getMethod()->getName()
                ),
                Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php')
                . '?editRechnungsadresse=1'
            );
        }

        $currency = Frontend::getCurrency();
        if ($currency->getCode() !== 'EUR') {
            throw new InvalidPayerDataException(
                \sprintf(
                    Handler::getBackendTranslation('%s ist nur in Euro möglich.'),
                    $this->getMethod()->getName()
                ),
                $this->getPaymentCancelURL()
            );
        }

        $exception = new InvalidPayerDataException();
        if (empty($customer->cTel)) {
            $exception->addAlert(new Alert(Alert::TYPE_ERROR, \sprintf(
                Handler::getBackendTranslation('Für %s ist die Angabe einer Telefonnummer erforderlich'),
                $this->getMethod()->getName()
            ), 'confirmPUI_tel', ['saveInSession' => true]));
        }
        try {
            $birthDate = new DateTime($customer->dGeburtstag ?? '');
        } catch (Exception $e) {
            $birthDate = null;
        }
        if (empty($customer->dGeburtstag) || $birthDate === null) {
            $exception->addAlert(new Alert(Alert::TYPE_ERROR, \sprintf(
                Handler::getBackendTranslation('Für %s ist die Angabe des Geburtsdatums erforderlich'),
                $this->getMethod()->getName()
            ), 'confirmPUI_birthdate', ['saveInSession' => true]));
        }

        if ($exception->hasAlerts()) {
            throw $exception->setRedirectURL(
                Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php') . '?editRechnungsadresse=1'
            );
        }
    }

    /**
     * @inheritDoc
     */
    public function createPPOrder(
        Customer $customer,
        Cart     $cart,
        string   $fundingSource,
        string   $shippingContext,
        string   $payAction,
        string   $bnCode = MerchantCredentials::BNCODE_CHECKOUT
    ): ?string {
        if ($fundingSource !== 'paypalPUI') {
            $this->logger->write(\LOGLEVEL_ERROR, 'createPPOrder: funding source is invalid');

            return null;
        }
        $transaction = Transaction::instance();
        $orderHash   = $this->generateHash(new Bestellung());
        $this->addCache('orderHash', $orderHash);
        $transaction->startTransaction(Transaction::CONTEXT_CREATE);

        try {
            $response = $this->createOrder(
                (new Order())
                    ->addPurchase($this->createPurchase($orderHash, Frontend::getDeliveryAddress(), $cart)
                         ->setInvoiceId(\baueBestellnummer()))
                    ->setIntent(Order::INTENT_CAPTURE)
                    ->setProcessingInstruction('ORDER_COMPLETE_ON_PAYMENT_APPROVAL')
                    ->setPaymentSource(
                        'pay_upon_invoice',
                        (new PaymentSource())
                            ->applyPayer($this->createPayer($customer, Payer::PAYER_ALL))
                            ->setExperienceContext(
                                (new ExperienceContext())
                                    ->setLocale(Helper::getLocaleFromISO(
                                        Helper::sanitizeISOCode($customer->cLand)
                                    ))
                                    ->setBrandName($this->getShopTitle())
                                    ->addCustomerServiceInstruction($this->getCustomerServiceInstructions())
                            )
                    ),
                $bnCode
            )->setExpectedResponseCode([200, 201]);
            $ppOrder  = $this->getPPOrder();
            $this->logger->write(\LOGLEVEL_DEBUG, 'createPPOrder: createOrderResponse', $response);

            if ($ppOrder !== null) {
                $ppOrder->setId($response->getId())
                        ->setStatus($response->getStatus())
                        ->setPurchases($response->getPurchases())
                        ->getPurchase()->setInvoiceId($ppOrder->getInvoiceId());
            } else {
                $this->logger->write(\LOGLEVEL_ERROR, 'createPPOrder: order creation failed (order object is null');

                return null;
            }
            $transaction->clearTransaction(Transaction::CONTEXT_CREATE);
        } catch (PPCRequestException $e) {
            $this->unsetCache();
            if (!$this->showErrorResponse($e->getResponse())) {
                try {
                    $this->logger->write(\LOGLEVEL_NOTICE, 'createPPOrder: ' . $e->getName(), $e->getResponse()
                                                                                               ->getData());
                } catch (JsonException | UnexpectedResponseException $e2) {
                    $this->logger->write(\LOGLEVEL_NOTICE, 'createPPOrder: ' . $e->getName(), $e->getResponse());
                }
                Shop::Container()->getAlertService()->addAlert(
                    Alert::TYPE_ERROR,
                    Handler::getBackendTranslation($e->getMessage()),
                    'createOrderRequest',
                    ['saveInSession' => true]
                );
            }

            return null;
        } catch (InvalidArgumentException $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                Handler::getBackendTranslation($e->getMessage()),
                'createOrderRequest',
                ['saveInSession' => true]
            );

            return null;
        } catch (Exception | GuzzleException $e) {
            $this->unsetCache();
            $this->logger->write(\LOGLEVEL_ERROR, 'createPPOrder: OrderResponseFailed - ' . $e->getMessage());

            return null;
        }

        if (!\in_array($ppOrder->getStatus(), [
            OrderStatus::STATUS_APPROVED,
            OrderStatus::STATUS_COMPLETED,
            OrderStatus::STATUS_PENDING_APPROVAL
        ], true)) {
            // ToDo: Not the expected order state ...
            $this->logger->write(\LOGLEVEL_ERROR, 'createPPOrder: UnexpectedOrderState get '
                . $ppOrder->getStatus() . ' expected ' . OrderStatus::STATUS_PENDING_APPROVAL);
        }

        return $ppOrder->getId();
    }

    /**
     * @param string|null $msg
     */
    private function raisePaymentError(?string $msg = null): void
    {
        $localMsg = $this->plugin->getLocalization()
                                 ->getTranslation($msg ?? 'jtl_paypal_commerce_payment_error') ?? $msg;

        if ($localMsg !== null) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                $localMsg,
                'preparePaymentProcess',
                ['saveInSession' => true]
            );
        }

        Helper::redirectAndExit($this->getPaymentCancelURL());
    }

    /**
     * @inheritDoc
     */
    public function preparePaymentProcess(Bestellung $order): void
    {
        parent::preparePaymentProcess($order);

        $ppOrder = $this->getPPOrder($this->createPPOrder(
            Frontend::getCustomer(),
            Frontend::getCart(),
            'paypalPUI',
            AppContext::SHIPPING_PROVIDED,
            AppContext::PAY_NOW
        ));
        if ($ppOrder === null) {
            $this->raisePaymentError();
            exit();
        }

        switch ($ppOrder->getStatus()) {
            case OrderStatus::STATUS_PENDING_APPROVAL:
            case OrderStatus::STATUS_APPROVED:
            case OrderStatus::STATUS_COMPLETED:
                $orderHash   = $this->getCache('orderHash') ?? $this->generateHash($order);
                $redirectUrl = $this->getNotificationURL($orderHash);
                $ppOrder->getPurchase()->setCustomId($orderHash);
                $ppOrder->setLink((object)[
                    'rel'  => 'paymentRedirect',
                    'href' => $redirectUrl,
                ]);

                Helper::redirectAndExit($redirectUrl);
                exit();
            default:
                $this->raisePaymentError();
                exit();
        }
    }

    /**
     * @inheritDoc
     */
    public function addIncomingPayment(Bestellung $order, object $payment)
    {
        if ($payment instanceof Order) {
            $paymenSource = $payment->getPaymentSource('pay_upon_invoice');
            if ($paymenSource !== null) {
                $payment->setPayer($paymenSource);
            }
        }

        parent::addIncomingPayment($order, $payment);
    }

    /**
     * @inheritDoc
     */
    public function finalizeOrder(Bestellung $order, string $hash, array $args): bool
    {
        $ppOrder = $this->getPPOrder();
        if ($ppOrder === null || empty($ppOrder->getId())) {
            $this->logger->write(\LOGLEVEL_ERROR, 'finalizeOrder: ppOrder is empty or has no id.');
            Shop::Container()->getDB()->delete('tzahlungsession', 'cZahlungsID', $hash);

            return false;
        }

        try {
            $response = $this->verifyPPOrder($ppOrder->getId());
            $this->logger->write(\LOGLEVEL_DEBUG, 'finalizeOrder: verifyOrderResponse', $response);
            $ppOrder->setStatus($response->getStatus());
        } catch (Exception | GuzzleException $e) {
            $this->logger->write(\LOGLEVEL_ERROR, 'finalizeOrder: verifyOrderResponse failed' . $e->getMessage());
        }

        $ppOrder->unsetLink('paymentRedirect');
        if (\in_array($ppOrder->getStatus(), [
            OrderStatus::STATUS_PENDING_APPROVAL,
            OrderStatus::STATUS_APPROVED,
            OrderStatus::STATUS_COMPLETED,
        ], true)) {
            // STATUS_PENDING_APPROVAL:
            //    This indicates that Ratepay performed the buyer risk assessment successfully and approved the payment
            // STATUS_APPROVED, STATUS_COMPLETED:
            //    indicates a successful order capture.
            $ppOrder->setLink((object)[
                'rel'  => 'paymentRedirect',
                'href' => $this->getReturnURL($order),
            ]);

            return true;
        }

        // ToDo: Not the expected order state ...
        $this->logger->write(\LOGLEVEL_ERROR, 'finalizeOrder: UnexpectedOrderState get '
            . $ppOrder->getStatus() . ' expected ' . OrderStatus::STATUS_PENDING_APPROVAL
            . ' or ' . OrderStatus::STATUS_APPROVED
            . ' or ' . OrderStatus::STATUS_COMPLETED);

        return false;
    }

    /**
     * @inheritDoc
     */
    public function finalizeOrderInDB(Bestellung $order): void
    {
        parent::finalizeOrderInDB($order);

        // prevent wawi synchronisation on a pending order
        $order->cAbgeholt = 'P';
    }

    /**
     * @inheritDoc
     */
    public function sendConfirmationMail(Bestellung $order)
    {
        $this->sendMail($order->kBestellung, 'kPlugin_' . $this->plugin->getID() . '_paymentinformation');
    }

    /**
     * @inheritDoc
     */
    public function handleNotification(Bestellung $order, string $hash, array $args): void
    {
        parent::handleNotification($order, $hash, $args);

        $ppOrder = $this->getPPOrder();
        if ($ppOrder === null || $ppOrder->getPurchase()->getCustomId() !== $hash) {
            $this->logger->write(\LOGLEVEL_NOTICE, 'handleNotification: ppOrder is empty or custom id does not match.');
            $this->unsetCache();

            return;
        }
        if ($ppOrder->getOrderId() > 0) {
            $this->logger->write(\LOGLEVEL_DEBUG, 'finalizeOrder: ppOrder is always finalized.');

            return;
        }

        try {
            $response = $this->verifyPPOrder($ppOrder->getId());
            $ppOrder->setStatus($response->getStatus())
                    ->setPurchases($response->getPurchases());
            $paymentSources = $response->getPaymentSourceNames();
            foreach ($paymentSources as $paymentName) {
                $ppOrder->setPaymentSource($paymentName, $response->getPaymentSource($paymentName));
            }
            $this->logger->write(\LOGLEVEL_DEBUG, 'handleNotification: verifyOrderResponse', $response);
        } catch (Exception | GuzzleException $e) {
            $this->logger->write(\LOGLEVEL_ERROR, 'handleNotification: verifyOrderResponse failed' . $e->getMessage());
        }

        $ppOrder->unsetLink('paymentRedirect')
                ->setOrderId($order->kBestellung);
        $this->storePPOrder($ppOrder);
        if ($ppOrder->getStatus() === OrderStatus::STATUS_PENDING_APPROVAL) {
            $stateLink = $this->plugin->getLinks()->getLinks()->first(static function (LinkInterface $link) {
                return $link->getTemplate() === 'pendingpayment.tpl';
            });
            $redirUrl  = $stateLink->getURL();
            $redirUrl .= (\strpos($redirUrl, '?') !== false ? '&' : '?')
                . 'payment=' . $this->getMethod()->getMethodID();

            Helper::redirectAndExit($redirUrl);
            exit();
        }

        $this->handlePaymentState($ppOrder->getStatus(), $ppOrder, $order);
    }

    /**
     * @inheritDoc
     */
    public function checkPaymentState(): string
    {
        $ppState = parent::checkPaymentState();
        $db      = Shop::Container()->getDB();
        $ppOrder = $this->getPPOrder();

        if ($ppOrder === null) {
            return OrderStatus::STATUS_UNKONWN;
        }

        $customId  = $ppOrder->getCustomId();
        $processId = \strpos($customId, '_') === 0 ? \substr($customId, 1) : $customId;
        $payment   = $db->queryPrepared(
            'SELECT tzahlungsession.kBestellung, tzahlungsid.cId, tzahlungsid.txn_id
                FROM tzahlungsid
                INNER JOIN tzahlungsession ON tzahlungsession.cZahlungsID = tzahlungsid.cId
                WHERE tzahlungsid.cId = :cuid',
            ['cuid' => $processId],
            ReturnType::SINGLE_OBJECT
        );
        if (!$payment || ($ppOrder->getId() !== $payment->txn_id)) {
            return OrderStatus::STATUS_UNKONWN;
        }

        $this->handlePaymentState($ppState, $ppOrder, new Bestellung((int)$payment->kBestellung, true));

        return $ppState;
    }

    /**
     * @inheritDoc
     */
    public function handlePaymentStateTimeout(object $stateResult): void
    {
        parent::handlePaymentStateTimeout($stateResult);

        if ($stateResult->state === OrderStatus::STATUS_PENDING_APPROVAL) {
            $stateResult->message = $this->plugin->getLocalization()->getTranslation('pui_pending_approval_info');
        }
    }

    /**
     * @param string     $ppState
     * @param Order      $ppOrder
     * @param Bestellung $order
     */
    private function handlePaymentState(string $ppState, Order $ppOrder, Bestellung $order): void
    {
        if (\in_array($ppState, [
            OrderStatus::STATUS_COMPLETED,
            OrderStatus::STATUS_SAVED
        ], true)) {
            $paymentSource = $ppOrder->getPaymentSource('pay_upon_invoice');
            $this->deletePaymentHash($ppOrder->getCustomId());
            $ppOrder->unsetLink('paymentRedirect');
            $ppOrder->setOrderId($order->kBestellung ?? 0);
            $this->addIncomingPayment($order, $ppOrder);
            $this->setOrderStatusToPaid($order);
            $this->updatePaymentState($this->getPPHash($ppOrder), $order);
            if ($paymentSource !== null) {
                $capture = $ppOrder->getPurchase()->getCapture();
                $amount  = $capture !== null ? $capture->getAmount() : $ppOrder->getPurchase()->getAmount();
                $this->updatePUIOrderData($order, $paymentSource, $amount);
                $this->sendConfirmationMail($order);
                Shop::Container()->getAlertService()->addAlert(
                    Alert::TYPE_INFO,
                    \nl2br($order->cPUIZahlungsdaten),
                    'paymentInformation',
                    ['saveInSession' => true]
                );
            } else {
                $this->logger->write(\LOGLEVEL_ERROR, 'handlePaymentState: paymentSource is empty');
            }
            $this->unsetCache();
        } elseif ($ppState === OrderStatus::STATUS_DECLINED) {
            Shop::Container()->getDB()->delete('tzahlungsession', 'cZahlungsID', $ppOrder->getCustomId());
            $this->deletePaymentHash($ppOrder->getCustomId());
            $this->sendDeclineMail($order);
            $this->unsetCache();
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_INFO,
                \sprintf(
                    $this->plugin->getLocalization()->getTranslation('jtl_paypal_commerce_payment_declined'),
                    $order->cBestellNr,
                    $this->getMethod()->getName()
                ),
                'paymentInformation',
                ['saveInSession' => true]
            );
        }
    }

    /**
     * @inheritDoc
     */
    protected function handleCaptureCompleted(Capture $capture, object $paymentSession): bool
    {
        $paymentId = $capture->getSupplementaryData()->related_ids->order_id ?? '';
        if ($paymentId !== $paymentSession->txn_id) {
            $this->logger->write(
                \LOGLEVEL_ERROR,
                'Webhook::handleCaptureCompleted - Payment id (' . $paymentId . ') for process id '
                . $paymentSession->cZahlungsID . ' does not match stored id '
                . '(' . $paymentSession->txn_id . ').'
            );

            return false;
        }

        $order = new Bestellung((int)$paymentSession->kBestellung);
        try {
            $orderResponse = $this->verifyPPOrder($paymentId);
            $paymentSource = $orderResponse->getPaymentSource('pay_upon_invoice');
            if ($paymentSource !== null) {
                $this->updatePUIOrderData($order, $paymentSource, $capture->getAmount());
            }
            $this->logger->write(\LOGLEVEL_DEBUG, 'handleCaptureCompleted: verifyOrderResponse', $orderResponse);
        } catch (Exception | GuzzleException $e) {
            $this->logger->write(
                \LOGLEVEL_ERROR,
                'Webhook::handleCaptureCompleted - Payment id (' . $paymentId . ') '
                . 'verifyPPOrder throws an error: ' . $e->getMessage()
            );
        }

        return parent::handleCaptureCompleted($capture, $paymentSession);
    }

    /**
     * @inheritDoc
     */
    protected function handleCaptureDenied(Capture $capture, object $paymentSession): bool
    {
        $paymentId = $capture->getSupplementaryData()->related_ids->order_id ?? '';
        if ($paymentId !== $paymentSession->txn_id) {
            $this->logger->write(
                \LOGLEVEL_ERROR,
                'Webhook::handleCaptureDenied - Payment id (' . $paymentId . ') for process id '
                . $paymentSession->cZahlungsID . ' does not match stored id '
                . '(' . $paymentSession->txn_id . ').'
            );

            return false;
        }

        return parent::handleCaptureDenied($capture, $paymentSession);
    }
}
