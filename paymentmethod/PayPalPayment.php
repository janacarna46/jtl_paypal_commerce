<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\paymentmethod;

use DateTime;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JTL\Alert\Alert;
use JTL\Backend\NotificationEntry;
use JTL\Cart\Cart;
use JTL\Catalog\Product\Artikel;
use JTL\Checkout\Bestellung;
use JTL\Checkout\Lieferadresse;
use JTL\Customer\Customer;
use JTL\Customer\CustomerGroup;
use JTL\DB\ReturnType;
use JTL\Helpers\GeneralObject;
use JTL\Helpers\Request;
use JTL\Helpers\ShippingMethod;
use JTL\Helpers\Text;
use JTL\Link\LinkInterface;
use JTL\Mail\Mail\Mail;
use JTL\Mail\Mailer;
use JTL\Plugin\Data\PaymentMethod;
use JTL\Plugin\Helper as PluginHelper;
use JTL\Plugin\Payment\Method;
use JTL\Plugin\PluginInterface;
use JTL\Plugin\State;
use JTL\Session\Frontend;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_paypal_commerce\adminmenu\Renderer;
use Plugin\jtl_paypal_commerce\frontend\Handler;
use Plugin\jtl_paypal_commerce\PPC\Authorization\AuthorizationException;
use Plugin\jtl_paypal_commerce\PPC\Authorization\MerchantCredentials;
use Plugin\jtl_paypal_commerce\PPC\Authorization\Token;
use Plugin\jtl_paypal_commerce\PPC\BackendUIsettings;
use Plugin\jtl_paypal_commerce\PPC\Configuration;
use Plugin\jtl_paypal_commerce\PPC\Environment\EnvironmentInterface;
use Plugin\jtl_paypal_commerce\PPC\HttpClient\PPCClient;
use Plugin\jtl_paypal_commerce\PPC\Logger;
use Plugin\jtl_paypal_commerce\PPC\Onboarding\MerchantIntegrationRequest;
use Plugin\jtl_paypal_commerce\PPC\Onboarding\MerchantIntegrationResponse;
use Plugin\jtl_paypal_commerce\PPC\Order\Address;
use Plugin\jtl_paypal_commerce\PPC\Order\AmountWithBreakdown;
use Plugin\jtl_paypal_commerce\PPC\Order\AppContext;
use Plugin\jtl_paypal_commerce\PPC\Order\Capture;
use Plugin\jtl_paypal_commerce\PPC\Order\Order;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderCaptureRequest;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderCaptureResponse;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderCreateRequest;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderCreateResponse;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderFullResponse;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderGetRequest;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderGetResponse;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderPatchRequest;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderPatchResponse;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderStatus;
use Plugin\jtl_paypal_commerce\PPC\Order\Patch;
use Plugin\jtl_paypal_commerce\PPC\Order\PatchInvoiceId;
use Plugin\jtl_paypal_commerce\PPC\Order\PatchPayer;
use Plugin\jtl_paypal_commerce\PPC\Order\PatchPurchase;
use Plugin\jtl_paypal_commerce\PPC\Order\PatchShippingAddress;
use Plugin\jtl_paypal_commerce\PPC\Order\PatchShippingName;
use Plugin\jtl_paypal_commerce\PPC\Order\Payer;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\AuthResult;
use Plugin\jtl_paypal_commerce\PPC\Order\Phone;
use Plugin\jtl_paypal_commerce\PPC\Order\Purchase\PurchaseUnit;
use Plugin\jtl_paypal_commerce\PPC\Order\Shipping;
use Plugin\jtl_paypal_commerce\PPC\Order\Transaction;
use Plugin\jtl_paypal_commerce\PPC\Order\TransactionException;
use Plugin\jtl_paypal_commerce\PPC\Request\ClientErrorResponse;
use Plugin\jtl_paypal_commerce\PPC\Request\PPCRequestException;
use Plugin\jtl_paypal_commerce\PPC\Request\UnexpectedResponseException;
use Plugin\jtl_paypal_commerce\PPC\Webhook\EventType;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

/**
 * Class PayPalPayment
 * @package Plugin\jtl_paypal_commerce\paymentmethod
 */
abstract class PayPalPayment extends Method implements PayPalPaymentInterface
{
    /** @var PaymentMethod */
    protected PaymentMethod $method;

    /** @var PluginInterface */
    protected PluginInterface $plugin;

    /** @var Logger */
    protected Logger $logger;

    /** @var int */
    protected int $handling = self::HANDLING_DEFAULT;

    /** @var array */
    protected array $localizations = [];

    /** @var Configuration|null */
    protected ?Configuration $config = null;

    /**
     * @inheritDoc
     */
    public function init(int $nAgainCheckout = 0)
    {
        parent::init($nAgainCheckout);

        $this->plugin       = PluginHelper::getPluginById('jtl_paypal_commerce');
        $this->method       = $this->plugin->getPaymentMethods()->getMethodByID($this->moduleID);
        $this->kZahlungsart = $this->method->getMethodID();
        $this->logger       = new Logger(Logger::TYPE_PAYMENT, $this);
        $this->name         = $this->method->getName();

        if ($this->plugin->getState() !== State::ACTIVATED) {
            return $this;
        }

        try {
            $this->config = Shop::Container()->get(Configuration::class);
        } catch (NotFoundExceptionInterface | ContainerExceptionInterface) {
            $this->config = null;

            return $this;
        }

        foreach ($this->method->getLocalization() as $localization) {
            $this->localizations[$localization->cISOSprache] = $localization;
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isValidIntern(array $args_arr = []): bool
    {
        if ($this->config === null || $this->plugin === null || $this->plugin->getState() !== State::ACTIVATED) {
            return false;
        }

        return parent::isValidIntern($args_arr);
    }

    /**
     * @inheritDoc
     */
    public function mappedLocalizedPaymentName(?string $isoCode = null): string
    {
        return $this->getLocalizedPaymentName($isoCode);
    }

    /**
     * @inheritDoc
     */
    public function getLocalizedPaymentName(?string $isoCode = null): string
    {
        if ($isoCode === null) {
            $isoCode = Shop::getLanguageCode();
        }

        if (!isset($this->localizations[$isoCode])) {
            return $this->getMethod()->getName();
        }

        return $this->localizations[$isoCode]->cName;
    }

    /**
     * @inheritDoc
     */
    public function getBackendNotification(PluginInterface $plugin, bool $force = false): ?NotificationEntry
    {
        if (!$this->method->getDuringOrder() && !$this->paymentAfterOrderSupported()
            && ($this->isAssigned() || $force)
        ) {
            $entry = new NotificationEntry(
                NotificationEntry::TYPE_WARNING,
                \__($this->method->getName()),
                \__('Zahlung nach Bestellabschluß wird nicht unterstützt.'),
                Shop::getAdminURL() . '/zahlungsarten.php?kZahlungsart=' . $this->method->getMethodID()
                . '&token=' . $_SESSION['jtl_token']
            );
            $entry->setPluginId($plugin->getPluginID());

            return $entry;
        }

        if ($this->method->getDuringOrder() && !$this->paymentDuringOrderSupported()
            && ($this->isAssigned() || $force)
        ) {
            $entry = new NotificationEntry(
                NotificationEntry::TYPE_WARNING,
                \__($this->method->getName()),
                \__('Zahlung vor Bestellabschluß wird nicht unterstützt.'),
                Shop::getAdminURL() . '/zahlungsarten.php?kZahlungsart=' . $this->method->getMethodID()
                . '&token=' . $_SESSION['jtl_token']
            );
            $entry->setPluginId($plugin->getPluginID());

            return $entry;
        }

        return null;
    }

    /**
     * @return bool
     */
    abstract protected function usePurchaseItems(): bool;

    /**
     * @param object $paymentSession
     * @return bool
     */
    abstract protected function isSessionPayed(object $paymentSession): bool;

    /**
     * @return string|null
     */
    protected function getCUID(): ?string
    {
        $payment = Shop::Container()->getDB()->queryPrepared(
            'SELECT tbestellstatus.cUID
                FROM tzahlungsession
                INNER JOIN tbestellstatus ON tzahlungsession.kBestellung = tbestellstatus.kBestellung
                WHERE tzahlungsession.cSID = :sessID
                ORDER BY tzahlungsession.dZeitBezahlt DESC',
            [
                'sessID' => \session_id(),
            ],
            ReturnType::SINGLE_OBJECT
        );

        return $payment->cUID ?? null;
    }

    /**
     * @param Configuration $config
     * @return MerchantIntegrationResponse|null
     */
    protected function getMerchantIntegration(Configuration $config): ?MerchantIntegrationResponse
    {
        $client      = new PPCClient(Shop::Container()->get(EnvironmentInterface::class));
        $workingMode = $config->getWorkingMode();
        $merchantID  = $config->getPrefixedConfigItem('merchantID_' . $workingMode);
        $partnerID   = \base64_decode(MerchantCredentials::partnerID($workingMode));

        try {
            return new MerchantIntegrationResponse($client->send(new MerchantIntegrationRequest(
                Token::getInstance()->getToken(),
                $partnerID ?? '',
                $merchantID ?? ''
            )));
        } catch (GuzzleException | AuthorizationException | PPCRequestException $e) {
            $this->logger->write(\LOGLEVEL_ERROR, 'MerchantIntegrationRequest: ' . $e->getMessage());
        }

        return null;
    }

    /**
     * @param Customer $customer
     * @param int      $needed
     * @return Payer
     */
    protected function createPayer(Customer $customer, int $needed = Payer::PAYER_DEFAULT): Payer
    {
        $payer = new Payer();
        if (($needed & Payer::PAYER_NAME) > 0) {
            if (!empty($customer->cNachname)) {
                $payer->setSurname(Text::unhtmlentities($customer->cNachname));
            }
            if (!empty($customer->cVorname)) {
                $payer->setGivenName(Text::unhtmlentities($customer->cVorname));
            }
        }
        if (($needed & Payer::PAYER_EMAIL) > 0 && !empty($customer->cMail)) {
            $payer->setEmail($customer->cMail);
        }
        if (($needed & Payer::PAYER_LOCATION) > 0) {
            $payer->setAddress(Address::createFromCustomer($customer));
        }

        if (($needed & Payer::PAYER_BIRTH) > 0) {
            try {
                $payer->setBirthDate(!empty($customer->dGeburtstag) && $customer->dGeburtstag !== '0000-00-00'
                    ? new DateTime($customer->dGeburtstag)
                    : null);
            } catch (Exception $e) {
                $payer->setBirthDate(null);
            }
        }

        if (($needed & Payer::PAYER_PHONE) > 0) {
            if (!empty($customer->cTel)) {
                $payer->setPhone((new Phone())->setNumber($customer->cTel));
            } elseif (!empty($customer->cMobil)) {
                $payer->setPhone((new Phone())->setNumber($customer->cMobil));
            }
        }

        return $payer;
    }

    /**
     * @param int    $languageID
     * @param string $sppContext
     * @param string $payAction
     * @return AppContext
     */
    protected function createAppContext(
        int $languageID,
        string $sppContext = AppContext::SHIPPING_PROVIDED,
        string $payAction = AppContext::PAY_NOW
    ): AppContext {
        return (new AppContext())
            ->setLocale(Helper::getLocaleFromISO(
                Helper::sanitizeISOCode(Shop::Lang()->getIsoFromLangID($languageID)->cISO)
            ))
            ->setBrandName($this->getShopTitle())
            ->setShipping($sppContext)
            ->setPayAction($payAction);
    }

    /**
     * @param string        $orderHash
     * @param Lieferadresse $shipping
     * @param Cart          $cart
     * @return PurchaseUnit
     */
    protected function createPurchase(
        string $orderHash,
        Lieferadresse $shipping,
        Cart $cart
    ): PurchaseUnit {
        $currency = Frontend::getCurrency();
        $merchant = Frontend::getCustomerGroup()->isMerchant();
        $amount   = AmountWithBreakdown::createFromCart($cart, $currency->getCode(), $merchant);
        $purchase = (new PurchaseUnit())
            ->setReferenceId(PurchaseUnit::REFERENCE_DEFAULT)
            ->setAmount($amount)
            ->setCustomId($orderHash)
            ->setDescription(Helper::getInstance($this->plugin)->getDescriptionFromCart($cart))
            ->setShipping((new Shipping())
                ->setName(
                    \trim(Text::unhtmlentities($shipping->cVorname) . ' ' . Text::unhtmlentities($shipping->cNachname))
                )
                ->setAddress(Address::createFromOrderAddress($shipping)));

        if ($this->usePurchaseItems()) {
            $purchase->addItemsFromCart($cart, $currency, $amount);
        }

        return $purchase;
    }

    /**
     * @param Order       $ppOrder
     * @param string      $bnCode
     * @return OrderCreateResponse
     * @throws AuthorizationException | GuzzleException | PPCRequestException
     */
    protected function createOrder(
        Order $ppOrder,
        string $bnCode = MerchantCredentials::BNCODE_CHECKOUT
    ): OrderCreateResponse {
        $client  = new PPCClient(Shop::Container()->get(EnvironmentInterface::class), $this->logger);
        $options = [];
        $this->storePPOrder($ppOrder);

        try {
            $options['PayPal-Request-Id'] = Transaction::instance()->getTransactionId(Transaction::CONTEXT_CREATE);
        } catch (TransactionException $e) {
            unset($options['PayPal-Request-Id']);
        }

        $this->logger->write(\LOGLEVEL_DEBUG, 'createOrder:', $ppOrder);

        return new OrderCreateResponse($client->send(new OrderCreateRequest(
            Token::getInstance()->getToken(),
            $ppOrder,
            $bnCode
        ), $options));
    }

    /**
     * @param OrderGetResponse $patchOrder
     * @param Customer         $customer
     * @param Cart             $cart
     * @param string           $orderHash
     * @return void
     * @throws AuthorizationException
     * @throws GuzzleException
     * @throws PPCRequestException|UnexpectedResponseException
     */
    protected function patchOrder(
        OrderFullResponse $patchOrder,
        Customer $customer,
        Cart $cart,
        string $orderHash
    ): void {
        $shipping      = Frontend::getDeliveryAddress();
        $payer         = $this->createPayer($customer);
        $orderId       = $patchOrder->getId();
        $shippingName  = \trim(
            Text::unhtmlentities($shipping->cVorname) . ' ' . Text::unhtmlentities($shipping->cNachname)
        );
        $shippingAddr  = Address::createFromOrderAddress($shipping);
        $orderShipping = $patchOrder->getPurchase()->getShipping();
        $patches       = [
            new PatchPurchase($this->createPurchase($orderHash, $shipping, $cart)),
        ];
        if ($patchOrder->getStatus() === OrderStatus::STATUS_CREATED && !$payer->isEmpty()) {
            $patches[] = new PatchPayer($payer, $patchOrder->getPayer() === null ? Patch::OP_ADD : Patch::OP_REPLACE);
        }
        if (!$shippingAddr->isEmpty()) {
            $patches[] = new PatchShippingAddress(
                $shippingAddr,
                $orderShipping === null ? Patch::OP_ADD : Patch::OP_REPLACE
            );
        }
        if (!empty($shippingName)) {
            if ($orderShipping === null) {
                $patches[] = new PatchShippingName($shippingName, Patch::OP_ADD);
            } else {
                $patches[] = new PatchShippingName(
                    $shippingName,
                    $orderShipping->getName() === null ? Patch::OP_ADD : Patch::OP_REPLACE
                );
            }
        }

        $this->logger->write(\LOGLEVEL_DEBUG, 'patchOrder:', $patches);
        $client = new PPCClient(Shop::Container()->get(EnvironmentInterface::class));

        new OrderPatchResponse($client->send(
            new OrderPatchRequest(Token::getInstance()->getToken(), $orderId, $patches)
        ));
    }

    /**
     * @param string $orderId
     * @param string $orderNumber
     * @param string $bnCode
     * @return OrderCaptureResponse
     * @throws Exception | GuzzleException
     */
    protected function captureOrder(
        string $orderId,
        string $orderNumber,
        string $bnCode = MerchantCredentials::BNCODE_CHECKOUT
    ): OrderCaptureResponse {
        $ppOrder     = $this->getPPOrder($orderId);
        $ppInvoiceId = $ppOrder !== null ? $ppOrder->getPurchase()->getInvoiceId() : null;
        $client      = new PPCClient(Shop::Container()->get(EnvironmentInterface::class));
        $patches     = [
            new PatchInvoiceId($orderNumber, $ppInvoiceId === null ? Patch::OP_ADD : Patch::OP_REPLACE),
        ];
        $this->logger->write(\LOGLEVEL_DEBUG, 'captureOrder - patchInvoiceId:', $patches);
        $client->send(new OrderPatchRequest(Token::getInstance()->getToken(), $orderId, $patches));
        if ($ppOrder !== null) {
            $ppOrder->getPurchase()->setInvoiceId($orderNumber);
        }

        $this->logger->write(\LOGLEVEL_DEBUG, 'captureOrder:', $ppOrder);

        return new OrderCaptureResponse(
            $client->send(new OrderCaptureRequest(Token::getInstance()->getToken(), $orderId, $bnCode))
        );
    }

    /**
     * @inheritDoc
     */
    public function verifyPPOrder(string $orderId): OrderGetResponse
    {
        $client = new PPCClient(Shop::Container()->get(EnvironmentInterface::class));

        return new OrderGetResponse($client->send(new OrderGetRequest(Token::getInstance()->getToken(), $orderId)));
    }

    /**
     * @param string $fundingSource
     * @return string
     */
    protected function validateFundingSource(string $fundingSource): string
    {
        if ($fundingSource === '') {
            return $this->getFundingSource();
        }
        if ($fundingSource !== $this->getFundingSource()) {
            $this->unsetCache();
            $this->setFundingSource($fundingSource);
        }

        return $fundingSource;
    }

    /**
     * @inheritDoc
     */
    public function validatePaymentConfiguration(PaymentMethod $method): bool
    {
        if ($this->paymentDuringOrderSupported()
            && Request::postInt('nWaehrendBestellung', $method->getDuringOrder() ? 1 : 0) === 1
        ) {
            return true;
        }
        if ($this->paymentAfterOrderSupported()
            && Request::postInt('nWaehrendBestellung', $method->getDuringOrder() ? 1 : 0) === 0) {
            return true;
        }

        Shop::Container()->getAlertService()->addAlert(
            Alert::TYPE_WARNING,
            $this->paymentDuringOrderSupported()
                ? \sprintf(\__('Zahlung nach Bestellabschluß wird von %s nicht unterstützt.'), $method->getName())
                : \sprintf(\__('Zahlung vor Bestellabschluß wird von %s nicht unterstützt.'), $method->getName()),
            'duringCheckoutNotSupported'
        );
        if (isset($_POST['nWaehrendBestellung']) && Request::postInt('einstellungen_bearbeiten') > 0) {
            $_POST['nWaehrendBestellung'] = $this->paymentDuringOrderSupported() ? '1' : '0';
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function validatePayerData(Customer $customer, ?Lieferadresse $shippingAdr = null, ?Cart $cart = null): void
    {
    }

    /**
     * @inheritDoc
     */
    public function setFundingSource(string $fundingSource): void
    {
        if ($fundingSource !== '') {
            $this->addCache('ppcFundingSource', $fundingSource);
        }
    }

    /**
     * @inheritDoc
     */
    public function getFundingSource(): string
    {
        return $this->getCache('ppcFundingSource') ?? '';
    }

    /**
     * @inheritDoc
     */
    public function getBNCode(string $default = MerchantCredentials::BNCODE_CHECKOUT): string
    {
        return $this->getCache('BNCode') ?? $default;
    }

    /**
     * @inheritDoc
     */
    public function renderBackendInformation(JTLSmarty $smarty, PluginInterface $plugin, Renderer $renderer): void
    {
        // nothing do to...
    }

    /**
     * @inheritDoc
     */
    public function isAssigned(string $shippingClasses = '', int $customerGroupID = 0, int $shippingMethodID = 0): bool
    {
        $key = $shippingClasses . '_' . $customerGroupID . '_' . $shippingMethodID;
        static $assigned;

        if (($assigned[$key] ?? null) === null) {
            $queryShipping      = '';
            $queryCustomerGroup = '';
            $params             = [
                'paymentID' => $this->getMethod()->getMethodID(),
            ];
            if ($shippingClasses !== '') {
                $queryShipping = "AND (tversandart.cVersandklassen = '-1'
                    OR tversandart.cVersandklassen LIKE :shippingClass1
                    OR tversandart.cVersandklassen LIKE :shippingClass2
                )";

                $params['shippingClass1'] = '% ' . $shippingClasses . ' %';
                $params['shippingClass2'] = '% ' . $shippingClasses;
            }
            if ($customerGroupID > 0) {
                $queryCustomerGroup = "AND (tversandart.cKundengruppen = '-1'
                    OR tversandart.cKundengruppen LIKE :customerGroup
                )";

                $params['customerGroup'] = '%;' . $customerGroupID . ';%';
            }
            if ($shippingMethodID > 0) {
                $queryShipping       .= ' AND tversandartzahlungsart.kVersandart = :shippingID';
                $params['shippingID'] = $shippingMethodID;
            }

            $result         = Shop::Container()->getDB()->getSingleObject(
                'SELECT COUNT(tversandart.kVersandart) AS cnt
                    FROM tversandartzahlungsart
                    INNER JOIN tversandart
                        ON tversandartzahlungsart.kVersandart = tversandart.kVersandart
                    WHERE tversandartzahlungsart.kZahlungsart = :paymentID
                    ' . $queryShipping . '
                    ' . $queryCustomerGroup,
                $params
            );
            $assigned[$key] = $result && (int)$result->cnt > 0;
        }

        return $assigned[$key];
    }

    /**
     * @inheritDoc
     */
    public function getMethod(): PaymentMethod
    {
        return $this->method;
    }

    /**
     * @inheritDoc
     */
    public function storePPOrder(?Order $order): void
    {
        Frontend::set($this->moduleID . '.ppOrder', $order);
        if ($order !== null) {
            $customId  = $order->getCustomId();
            $processId = \strpos($customId, '_') === 0 ? \substr($customId, 1) : $customId;
            Shop::Container()->getDB()->update('tzahlungsid', 'cId', $processId, (object)[
                'txn_id' => $order->getId(),
            ]);
        }
    }

    /**
     * @inheritDoc
     */
    public function resetPPOrder(?string $orderId = null): void
    {
        if ($orderId !== null) {
            /** @var Order $order */
            $order = Frontend::get($this->moduleID . '.ppOrder');
            if ($order->getId() === $orderId) {
                Frontend::set($this->moduleID . '.ppOrder', null);
            }
        } else {
            $this->unsetCache();
        }
    }

    /**
     * @inheritDoc
     */
    public function getPPOrder(?string $orderId = null): ?Order
    {
        /** @var Order $order */
        $order = Frontend::get($this->moduleID . '.ppOrder');

        if ($order === null) {
            return null;
        }

        return ($orderId === null || $order->getId() === $orderId) ? $order : null;
    }

    /**
     * @inheritDoc
     */
    public function getPPHash(Order $order): string
    {
        $paymentHash = ($paymentHash = $order->getCustomId()) === ''
            ? $order->getPurchase()->getCustomId()
            : $paymentHash;

        return \strpos($paymentHash, '_') === 0 ? \substr($paymentHash, 1) : $paymentHash;
    }

    /**
     * @inheritDoc
     */
    public function setHandling(int $handling): void
    {
        $this->handling = $handling;
    }

    /**
     * @inheritDoc
     */
    public function getHandling(): int
    {
        return $this->handling ?? self::HANDLING_DEFAULT;
    }

    /**
     * @inheritDoc
     */
    public function get3DSAuthResult(string $authAction): string
    {
        if (\in_array($authAction, [AuthResult::AUTHACTION_CONTINUE, AuthResult::AUTHACTION_REJECT], true)) {
            return $authAction;
        }

        $prefix = BackendUIsettings::BACKEND_SETTINGS_SECTION_ACDCDISPLAY . '_';
        switch ($authAction) {
            case AuthResult::AUTHACTION_ERROR:
            case AuthResult::AUTHACTION_CANCEL:
                return $this->config->getPrefixedConfigItem(
                    $prefix . $authAction,
                    AuthResult::AUTHACTION_REJECT
                );
            case AuthResult::AUTHACTION_SKIP:
            case AuthResult::AUTHACTION_NOTSUPPORTED:
            case AuthResult::AUTHACTION_UNABLETOCOMPLETE:
            case AuthResult::AUTHACTION_NOTELIGIBLE:
                return $this->config->getPrefixedConfigItem(
                    $prefix . $authAction,
                    AuthResult::AUTHACTION_CONTINUE
                );
        }

        return AuthResult::AUTHACTION_REJECT;
    }

    /**
     * @inheritDoc
     */
    public function checkPaymentState(): string
    {
        $ppOrder = $this->getPPOrder();
        if ($ppOrder === null || $ppOrder->getId() === null || $ppOrder->getStatus() === OrderStatus::STATUS_UNKONWN) {
            return OrderStatus::STATUS_UNKONWN;
        }

        try {
            $response = $this->verifyPPOrder($ppOrder->getId());
            $this->logger->write(\LOGLEVEL_DEBUG, 'checkPaymentState: verifyOrderResponse', $response);
            $ppOrder
                ->setStatus($response->getStatus())
                ->setPurchases($response->getPurchases());
            $paymentSources = $response->getPaymentSourceNames();
            foreach ($paymentSources as $paymentName) {
                $ppOrder->setPaymentSource($paymentName, $response->getPaymentSource($paymentName));
            }
        } catch (Exception | GuzzleException $e) {
            $this->logger->write(\LOGLEVEL_ERROR, 'checkPaymentState: verifyOrderResponse failed' . $e->getMessage());
        }

        if (\in_array($ppOrder->getStatus(), [OrderStatus::STATUS_CREATED, OrderStatus::STATUS_APPROVED], true)) {
            return $ppOrder->getStatus();
        }

        if ($ppOrder->getStatus() === OrderStatus::STATUS_COMPLETED) {
            if ($ppOrder->getOrderId() > 0) {
                return OrderStatus::STATUS_SAVED;
            }

            return OrderStatus::STATUS_COMPLETED;
        }

        return $ppOrder->getStatus();
    }

    /**
     * @inheritDoc
     */
    public function updatePaymentState(string $paymentHash, Bestellung $order): void
    {
        $db = Shop::Container()->getDB();
        $db->update('tzahlungsession', 'cZahlungsID', $paymentHash, (object)[
            'kBestellung'  => (int)$order->kBestellung,
            'cSID'         => \session_id(),
        ]);
        if ($order->cStatus === \BESTELLUNG_STATUS_BEZAHLT) {
            $db->update('tzahlungsid', 'cId', $paymentHash, (object)[
                'kBestellung' => (int)$order->kBestellung,
            ]);
        }
    }

    /**
     * @inheritDoc
     */
    public function handlePaymentStateTimeout(object $stateResult): void
    {
    }

    /**
     * @inheritDoc
     */
    public function getPaymentStateURL(): ?string
    {
        /** @var LinkInterface $stateLink */
        $stateLink = $this->plugin->getLinks()->getLinks()->first(static function (LinkInterface $link) {
            return $link->getTemplate() === 'pendingpayment.tpl';
        });

        return $stateLink !== null ? $stateLink->getURL() . '?payment=' . $this->getMethod()->getMethodID() : null;
    }

    /**
     * @inheritDoc
     */
    public function getPaymentCancelURL(): string
    {
        return Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php') . '?editZahlungsart=1';
    }

    /**
     * @inheritDoc
     */
    public function unsetCache(?string $cKey = null)
    {
        if ($cKey === null) {
            Frontend::set($this->moduleID . '.ppOrder', null);
        }

        return parent::unsetCache($cKey);
    }

    /**
     * @inheritDoc
     */
    public function generateHash(Bestellung $order): string
    {
        $hash = parent::generateHash($order);
        $db   = Shop::Container()->getDB();
        $db->delete('tzahlungsid', 'cId', $hash);
        $db->insert('tzahlungsid', (object)[
            'kBestellung'  => $order->kBestellung ?? 0,
            'kZahlungsart' => $this->getMethod()->getMethodID(),
            'cId'          => \strpos($hash, '_') === 0 ? \substr($hash, 1) : $hash,
            'txn_id'       => '',
            'dDatum'       => 'NOW()',
        ]);

        return $hash;
    }

    /**
     * @inheritDoc
     */
    public function redirectOnCancel(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function redirectOnPaymentSuccess(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getReturnURL(Bestellung $order): string
    {
        if (Shop::getSettings([\CONF_KAUFABWICKLUNG])['kaufabwicklung']['bestellabschluss_abschlussseite'] === 'A') {
            $oZahlungsID = Shop::Container()->getDB()->queryPrepared(
                'SELECT cId
                    FROM tbestellid
                    WHERE kBestellung = :orderId',
                [
                    'orderId' => $order->kBestellung,
                ],
                ReturnType::SINGLE_OBJECT
            );
            if (\is_object($oZahlungsID)) {
                return Shop::getURL() . '/bestellabschluss.php?i=' . $oZahlungsID->cId;
            }
        }

        if (empty($order->BestellstatusURL)) {
            $cUID = $this->getCUID();

            return $cUID !== null
                ? Shop::Container()->getLinkService()->getStaticRoute('status.php') . '?uid=' . $cUID
                : Shop::Container()->getLinkService()->getStaticRoute('jtl.php') . '?bestellungen=1';
        }

        return $order->BestellstatusURL;
    }

    /**
     * @inheritDoc
     */
    public function addIncomingPayment(Bestellung $order, object $payment)
    {
        $payData = $payment;

        $order->cZahlungsartName = $this->getMethod()->getName();
        if ($payment instanceof Order) {
            $capture = $payment->getPurchase()->getCapture() ?? new Capture();
            $amount  = $capture->getAmount();
            $fee     = $amount->getBreakdownItem('paypal_fee');
            $payer   = $payment->getPayer();
            $payData = (object)[
                'fBetrag'           => $amount->getValue(),
                'fZahlungsgebuehr'  => $fee !== null ? $fee->getValue() : 0,
                'cISO'              => $amount->getCurrencyCode(),
                'cZahler'           => $payer !== null ? $payer->getSurname() . ', ' . $payer->getEmail() : '',
                'cHinweis'          => $capture->getId(),
            ];
        }

        return parent::addIncomingPayment($order, $payData);
    }

    /**
     * @inheritDoc
     */
    public function setOrderStatusToPaid(Bestellung $order)
    {
        Shop::Container()->getDB()->update(
            'tbestellung',
            'kBestellung',
            $order->kBestellung,
            (object)[
                'cStatus'          => \BESTELLUNG_STATUS_BEZAHLT,
                'dBezahltDatum'    => 'NOW()',
                'cZahlungsartName' => $this->getMethod()->getName(),
            ]
        );

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function finalizeOrder(Bestellung $order, string $hash, array $args): bool
    {
        $ppOrder = $this->getPPOrder();
        if ($ppOrder === null || empty($ppOrder->getId())) {
            $this->logger->write(\LOGLEVEL_DEBUG, 'finalizeOrder: ppOrder is empty or has no id.');
            Shop::Container()->getDB()->delete('tzahlungsession', 'cZahlungsID', $hash);

            return false;
        }

        $ppOrder->unsetLink('paymentRedirect');
        if ($ppOrder->getStatus() !== OrderStatus::STATUS_COMPLETED) {
            // ToDo: Not the expected order state ...
            $this->logger->write(\LOGLEVEL_ERROR, 'finalizeOrder: UnexpectedOrderState get '
                . $ppOrder->getStatus() . ' expected ' . OrderStatus::STATUS_APPROVED);

            return false;
        }

        $this->deletePaymentHash($hash);
        $ppOrder->setLink((object)[
            'rel'  => 'paymentRedirect',
            'href' => $this->getReturnURL($order),
        ]);
        $order->cBestellNr = $ppOrder->getPurchase()->getInvoiceId();

        return true;
    }

    /**
     * @inheritDoc
     */
    public function finalizeOrderInDB(Bestellung $order): void
    {
        $ppOrder = $this->getPPOrder();
        if ($ppOrder !== null) {
            $order->cBestellNr = $ppOrder->getInvoiceId();
        }
    }

    /**
     * @inheritDoc
     */
    public function sendMail(int $orderID, string $type, $additional = null)
    {
        if (\strpos($type, 'kPlugin_' . $this->plugin->getID()) === 0) {
            $order = new Bestellung($orderID);
            $order->fuelleBestellung(false);
            $data   = (object)[
                'tkunde'      => new Customer($order->kKunde),
                'tbestellung' => $order,
            ];
            $mailer = Shop::Container()->get(Mailer::class);
            $mail   = new Mail();
            $mailer->send($mail->createFromTemplateID($type, $data));

            return $this;
        }

        return parent::sendMail($orderID, $type, $additional);
    }

    /**
     * @param Bestellung $order
     */
    protected function sendDeclineMail(Bestellung $order): void
    {
        if ($order->Zahlungsart->nMailSenden & \ZAHLUNGSART_MAIL_STORNO) {
            $this->sendMail($order->kBestellung, 'kPlugin_' . $this->plugin->getID() . '_declinepayment');
        }
    }

    /**
     * @param ClientErrorResponse $response
     * @return bool
     */
    protected function showErrorResponse(ClientErrorResponse $response): bool
    {
        $errDetail = $response->getDetail();
        if ($errDetail === null) {
            return false;
        }

        $this->logger->write(\LOGLEVEL_NOTICE, $errDetail->getDescription(), $response);
        $issue = $errDetail->getIssue() ?? '';
        $msg   = Handler::getBackendTranslation($issue);
        if ($msg === $issue) {
            $msg = Handler::getBackendTranslation($errDetail->getDescription() ?? '');
        }
        if ($msg !== '') {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                $msg,
                'createOrderRequest',
                ['saveInSession' => true]
            );

            return true;
        }

        return false;
    }

    /**
     * @param Capture $capture
     * @param object  $paymentSession
     * @return bool
     */
    protected function handleCaptureCompleted(Capture $capture, object $paymentSession): bool
    {
        $payStatus        = $capture->getStatus();
        $paymentProcessId = $paymentSession->cZahlungsID;

        if ($payStatus !== OrderStatus::STATUS_COMPLETED) {
            $this->logger->write(
                \LOGLEVEL_ERROR,
                'Webhook::handleCaptureCompleted - UnexpectedOrderState: get '
                . $payStatus . ' expected ' . OrderStatus::STATUS_COMPLETED
            );

            return false;
        }

        if ((int)$paymentSession->kBestellung === 0) {
            // No order created for this payment - what to do?
            $this->logger->write(
                \LOGLEVEL_DEBUG,
                'Webhook::handleCaptureCompleted - no payment, no order for process id '
                    . $paymentProcessId . ', try to create...'
            );

            try {
                // call frontent url...
                (new Client(
                    ['verify' => !((\constant('DEFAULT_CURL_OPT_VERIFYHOST') ?? 2) === 0)]
                ))->get($this->getNotificationURL('_' . $paymentProcessId));
                $this->logger->write(
                    \LOGLEVEL_DEBUG,
                    'Webhook::handleCaptureCompleted - payment and order for process id '
                        . $paymentProcessId . ' created'
                );
            } catch (GuzzleException $e) {
                $this->logger->write(\LOGLEVEL_ERROR, 'Webhook::handleCaptureCompleted - order creation failed: '
                    . $e->getMessage());

                return false;
            }
        }

        // Order created but not payed - add payment for the order
        $this->logger->write(\LOGLEVEL_DEBUG, 'Webhook::handleCaptureCompleted - Payment for process id '
            . $paymentProcessId . ' will be finished.');

        $payID = $capture->getId();
        $db    = Shop::Container()->getDB();
        $payed = $db->select(
            'tzahlungseingang',
            ['kBestellung', 'cHinweis'],
            [(int)$paymentSession->kBestellung, $payID]
        );
        if (!$payed) {
            $amount  = $capture->getAmount();
            $fee     = $amount->getBreakdownItem('paypal_fee');
            $order   = new Bestellung((int)$paymentSession->kBestellung);
            $payData = (object)[
                'fBetrag'           => $amount->getValue(),
                'fZahlungsgebuehr'  => $fee !== null ? $fee->getValue() : 0,
                'cISO'              => $amount->getCurrencyCode(),
                'cZahler'           => '',
                'cHinweis'          => $payID,
            ];
            $db->delete('tzahlungsession', 'cZahlungsID', $paymentProcessId);
            $this->deletePaymentHash($paymentProcessId);
            $this->addIncomingPayment($order, $payData);
            $this->setOrderStatusToPaid($order);
            $this->sendConfirmationMail($order);
        }

        return true;
    }

    /**
     * @param Capture $capture
     * @param object  $paymentSession
     * @return bool
     */
    protected function handleCaptureDenied(Capture $capture, object $paymentSession): bool
    {
        $payStatus        = $capture->getStatus();
        $paymentProcessId = $paymentSession->cZahlungsID;

        if ($payStatus !== OrderStatus::STATUS_DECLINED) {
            $this->logger->write(
                \LOGLEVEL_ERROR,
                'Webhook::handleCaptureDenied - UnexpectedOrderState: get '
                . $payStatus . ' expected ' . OrderStatus::STATUS_DECLINED
            );

            return false;
        }

        if ((int)$paymentSession->kBestellung === 0) {
            // No order created for this payment - what to do?
            $this->logger->write(\LOGLEVEL_DEBUG, 'Webhook::handleCaptureDenied - no payment, no order for process id '
                . $paymentProcessId . ', nothing to do...');

            return false;
        }

        // Order created but not payed - decline payment for the order
        $this->logger->write(\LOGLEVEL_DEBUG, 'Webhook::handleCaptureDenied - Payment for process id '
            . $paymentProcessId . ' will be declined.');

        $payID = $capture->getId();
        $db    = Shop::Container()->getDB();
        $db->delete(
            'tzahlungseingang',
            ['kBestellung', 'cHinweis'],
            [(int)$paymentSession->kBestellung, $payID]
        );
        $db->delete('tzahlungsession', 'cZahlungsID', $paymentProcessId);
        $this->deletePaymentHash($paymentProcessId);
        $this->sendDeclineMail(new Bestellung((int)$paymentSession->kBestellung, true));

        return true;
    }

    /**
     * @inheritDoc
     */
    public function handleCaptureWebhook(string $eventType, Capture $capture, object $paymentSession): bool
    {
        $this->logger->write(\LOGLEVEL_DEBUG, 'handleCaptureWebhook', $paymentSession);

        $db = Shop::Container()->getDB();
        if ($this->isSessionPayed($paymentSession)) {
            // Order finished and payed
            $db->delete('tzahlungsession', 'cZahlungsID', $paymentSession->cZahlungsID);
            $this->deletePaymentHash($paymentSession->cZahlungsID);
            $this->logger->write(\LOGLEVEL_DEBUG, 'handleCaptureWebhook - Payment for process id '
                . $paymentSession->cZahlungsID . ' already finished.');

            return true;
        }

        switch ($eventType) {
            case EventType::CAPTURE_COMPLETED:
                return $this->handleCaptureCompleted($capture, $paymentSession);
            case EventType::CAPTURE_DENIED:
            case EventType::CAPTURE_REVERSED:
                return $this->handleCaptureDenied($capture, $paymentSession);
        }

        $this->logger->write(\LOGLEVEL_DEBUG, 'handleCaptureWebhook - event type '
            . $eventType . ' not supported');

        return false;
    }
}
