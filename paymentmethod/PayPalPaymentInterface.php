<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\paymentmethod;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use JTL\Backend\NotificationEntry;
use JTL\Cart\Cart;
use JTL\Catalog\Product\Artikel;
use JTL\Checkout\Bestellung;
use JTL\Checkout\Lieferadresse;
use JTL\Customer\Customer;
use JTL\Plugin\Data\PaymentMethod;
use JTL\Plugin\Payment\MethodInterface;
use JTL\Plugin\PluginInterface;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_paypal_commerce\adminmenu\Renderer;
use Plugin\jtl_paypal_commerce\frontend\PaymentFrontendInterface;
use Plugin\jtl_paypal_commerce\PPC\Authorization\MerchantCredentials;
use Plugin\jtl_paypal_commerce\PPC\Configuration;
use Plugin\jtl_paypal_commerce\PPC\Order\Capture;
use Plugin\jtl_paypal_commerce\PPC\Order\Order;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderGetResponse;

/**
 * Class PayPalPaymentInterface
 * @package Plugin\jtl_paypal_commerce\paymentmethod
 */
interface PayPalPaymentInterface extends MethodInterface
{
    public const HANDLING_DEFAULT = 1;
    public const HANDLING_WEBHOOK = 2;

    /**
     * @return bool
     */
    public function paymentDuringOrderSupported(): bool;

    /**
     * @return bool
     */
    public function paymentAfterOrderSupported(): bool;

    /**
     * @param string|null $isoCode
     * @return string
     */
    public function mappedLocalizedPaymentName(?string $isoCode = null): string;

    /**
     * @param string|null $isoCode
     * @return string
     */
    public function getLocalizedPaymentName(?string $isoCode = null): string;

    /**
     * @param PaymentMethod $method
     * @return bool
     */
    public function validatePaymentConfiguration(PaymentMethod $method): bool;

    /**
     * @param Customer           $customer
     * @param Lieferadresse|null $shippingAdr
     * @param Cart|null          $cart
     * @throws InvalidPayerDataException
     */
    public function validatePayerData(Customer $customer, ?Lieferadresse $shippingAdr = null, ?Cart $cart = null): void;

    /**
     * @param PluginInterface $plugin
     * @param bool            $force
     * @return NotificationEntry|null
     */
    public function getBackendNotification(PluginInterface $plugin, bool $force = false): ?NotificationEntry;

    /**
     * @param JTLSmarty       $smarty
     * @param PluginInterface $plugin
     * @param Renderer        $renderer
     * @return void
     */
    public function renderBackendInformation(JTLSmarty $smarty, PluginInterface $plugin, Renderer $renderer): void;

    /**
     * @param string $shippingClasses
     * @param int    $customerGroupID
     * @param int    $shippingMethodID
     * @return bool
     */
    public function isAssigned(string $shippingClasses = '', int $customerGroupID = 0, int $shippingMethodID = 0): bool;

    /**
     * @return PaymentMethod
     */
    public function getMethod(): PaymentMethod;

    /**
     * @param Configuration $config
     * @param JTLSmarty     $smarty
     * @return PaymentFrontendInterface
     */
    public function getFrontendInterface(Configuration $config, JTLSmarty $smarty): PaymentFrontendInterface;

    /**
     * @param Order|null $order
     */
    public function storePPOrder(?Order $order): void;

    /**
     * @param string|null $orderId
     * @return Order|null
     */
    public function getPPOrder(?string $orderId = null): ?Order;

    /**
     * @param Order $order
     * @return string
     */
    public function getPPHash(Order $order): string;

    /**
     * @param Customer $customer
     * @param Cart     $cart
     * @param string   $fundingSource
     * @param string   $shippingContext
     * @param string   $payAction
     * @param string   $bnCode
     * @return string|null
     */
    public function createPPOrder(
        Customer $customer,
        Cart $cart,
        string $fundingSource,
        string $shippingContext,
        string $payAction,
        string $bnCode = MerchantCredentials::BNCODE_CHECKOUT
    ): ?string;

    /**
     * @param string $orderId
     * @return OrderGetResponse
     * @throws Exception | GuzzleException
     */
    public function verifyPPOrder(string $orderId): OrderGetResponse;

    /**
     * @param string|null $orderId
     * @return void
     */
    public function resetPPOrder(?string $orderId = null): void;

    /**
     * @param string $fundingSource
     */
    public function setFundingSource(string $fundingSource): void;

    /**
     * @return string
     */
    public function getFundingSource(): string;

    /**
     * @param string $default
     * @return string
     */
    public function getBNCode(string $default = MerchantCredentials::BNCODE_CHECKOUT): string;

    /**
     * @return string
     */
    public function checkPaymentState(): string;

    /**
     * @param object $stateResult
     * @return void
     */
    public function handlePaymentStateTimeout(object $stateResult): void;

    /**
     * @param string     $paymentHash
     * @param Bestellung $order
     * @return void
     */
    public function updatePaymentState(string $paymentHash, Bestellung $order): void;

    /**
     * @return string|null
     */
    public function getPaymentStateURL(): ?string;

    /**
     * @return string
     */
    public function getPaymentCancelURL(): string;

    /**
     * @param int $handling - one of the HANDLING_-constants
     */
    public function setHandling(int $handling): void;

    /**
     * @return int
     */
    public function getHandling(): int;

    /**
     * @param string $authAction
     * @return string
     */
    public function get3DSAuthResult(string $authAction): string;

    /**
     * @param object $customer
     * @param Cart   $cart
     * @return bool
     */
    public function isValidExpressPayment(object $customer, Cart $cart): bool;

    /**
     * @param object       $customer
     * @param Artikel|null $product
     * @return bool
    */
    public function isValidExpressProduct(object $customer, ?Artikel $product): bool;

    /**
     * @param object $customer
     * @param Cart   $cart
     * @return bool
     */
    public function isValidBannerPayment(object $customer, Cart $cart): bool;

    /**
     * @param object       $customer
     * @param Artikel|null $product
     * @return bool
     */
    public function isValidBannerProduct(object $customer, ?Artikel $product): bool;

    /**
     * @param Bestellung $order
     * @return void
     */
    public function finalizeOrderInDB(Bestellung $order): void;

    /**
     * @param string  $eventType
     * @param Capture $capture
     * @param object  $paymentSession
     * @return bool
     */
    public function handleCaptureWebhook(string $eventType, Capture $capture, object $paymentSession): bool;
}
