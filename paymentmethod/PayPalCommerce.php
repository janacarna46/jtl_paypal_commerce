<?php /** @noinspection PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\paymentmethod;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use JsonException;
use JTL\Alert\Alert;
use JTL\Cart\Cart;
use JTL\Catalog\Product\Artikel;
use JTL\Checkout\Bestellung;
use JTL\Customer\Customer;
use JTL\Customer\CustomerGroup;
use JTL\Helpers\GeneralObject;
use JTL\Helpers\Request;
use JTL\Helpers\ShippingMethod;
use JTL\Session\Frontend;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_paypal_commerce\frontend\Handler;
use Plugin\jtl_paypal_commerce\frontend\PaymentFrontendInterface;
use Plugin\jtl_paypal_commerce\frontend\PPCPFrontend;
use Plugin\jtl_paypal_commerce\PPC\Authorization\AuthorizationException;
use Plugin\jtl_paypal_commerce\PPC\Authorization\MerchantCredentials;
use Plugin\jtl_paypal_commerce\PPC\Authorization\Token;
use Plugin\jtl_paypal_commerce\PPC\Configuration;
use Plugin\jtl_paypal_commerce\PPC\Order\Order;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderStatus;
use Plugin\jtl_paypal_commerce\PPC\Order\Payer;
use Plugin\jtl_paypal_commerce\PPC\Request\ClientErrorResponse;
use Plugin\jtl_paypal_commerce\PPC\Request\PPCRequestException;
use Plugin\jtl_paypal_commerce\PPC\Request\UnexpectedResponseException;

/**
 * Class PayPalCommerce
 * @package Plugin\jtl_paypal_commerce\paymentmethod
 */
class PayPalCommerce extends PayPalPayment
{
    /**
     * @inheritDoc
     */
    public function paymentDuringOrderSupported(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function paymentAfterOrderSupported(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function mappedLocalizedPaymentName(?string $isoCode = null): string
    {
        return Handler::getBackendTranslation('Standardzahlarten');
    }

    /**
     * @inheritDoc
     */
    protected function usePurchaseItems(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    protected function isSessionPayed(object $paymentSession): bool
    {
        return (int)$paymentSession->nBezahlt > 0;
    }

    /**
     * @param bool $getAll
     * @return array
     */
    protected function getValidOrderProcessStates(bool $getAll = true): array
    {
        return $getAll
            ? [
                OrderStatus::STATUS_APPROVED,
                OrderStatus::STATUS_COMPLETED
            ]
            : [
                OrderStatus::STATUS_APPROVED
            ];
    }

    /**
     * @param Order               $ppOrder
     * @param PPCRequestException $exception
     * @return void
     */
    protected function handleOvercharge(Order $ppOrder, PPCRequestException $exception): void
    {
        $detail = $exception->getDetail();
        if ($detail === null || $detail->getIssue() !== 'PAYER_ACTION_REQUIRED') {
            return;
        }

        $this->resetPPOrder($ppOrder->getId());
        $localization = $this->plugin->getLocalization();
        Shop::Container()->getAlertService()->addAlert(
            Alert::TYPE_ERROR,
            $localization->getTranslation('jtl_paypal_commerce_payer_action_required'),
            'handleOvercharge',
            [
                'saveInSession' => true,
                'linkText'      => $localization->getTranslation('jtl_paypal_commerce_psd2overcharge'),
            ]
        );

        Helper::redirectAndExit(
            Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php')
        );
        exit();
    }

    /**
     * @inheritDoc
     */
    public function isValidIntern(array $args_arr = []): bool
    {
        if (!parent::isValidIntern($args_arr)) {
            return false;
        }

        try {
            return $this->method->getDuringOrder() && Token::getInstance()->getToken() !== null;
        } catch (AuthorizationException $e) {
            $this->logger->write(\LOGLEVEL_ERROR, 'AuthorizationException:' . $e->getMessage());

            return false;
        }
    }

    /**
     * @inheritDoc
     */
    public function isValid(object $customer, Cart $cart): bool
    {
        if (Request::postInt('resetPayment') === 1) {
            $this->resetPPOrder();
            unset($_POST['resetPayment']);

            return false;
        }

        return parent::isValid($customer, $cart);
    }

    /**
     * @inheritDoc
     */
    public function isValidExpressPayment(object $customer, Cart $cart): bool
    {
        if (!$this->isValid($customer, $cart)) {
            return false;
        }

        foreach ($cart->PositionenArr as $cartItem) {
            if ((int)($cartItem->Artikel->FunktionsAttribute['no_paypalexpress'] ?? 0) === 1) {
                return false;
            }
        }

        return $this->isAssigned(
            ShippingMethod::getShippingClasses($cart),
            (int)($customer->kKundengruppe ?? 0) > 0
                ? (int)$customer->kKundengruppe
                : CustomerGroup::getDefaultGroupID()
        );
    }

    /**
     * @inheritDoc
     */
    public function isValidExpressProduct(object $customer, ?Artikel $product): bool
    {
        if (!$this->isValidIntern()) {
            return false;
        }

        if ($product === null || $product->bHasKonfig || $product->inWarenkorbLegbar <= 0) {
            return false;
        }

        $cart = GeneralObject::deepCopy(Frontend::getCart());
        $cart->fuegeEin($product->getID(), \max($product->fMindestbestellmenge, 1), $product->Attribute);

        return $this->isValidExpressPayment($customer, $cart);
    }

    /**
     * @inheritDoc
     */
    public function isValidBannerPayment(object $customer, Cart $cart): bool
    {
        return $this->isValid($customer, $cart) && $this->isAssigned(
            ShippingMethod::getShippingClasses($cart),
            (int)($customer->kKundengruppe ?? 0) > 0
                ? (int)$customer->kKundengruppe
                : CustomerGroup::getDefaultGroupID()
        );
    }

    /**
     * @inheritDoc
     */
    public function isValidBannerProduct(object $customer, ?Artikel $product): bool
    {
        if (!$this->isValidIntern()) {
            return false;
        }

        if ($product === null || $product->bHasKonfig) {
            return false;
        }

        $cart = GeneralObject::deepCopy(Frontend::getCart());
        if ($product->inWarenkorbLegbar > 0) {
            $cart->fuegeEin($product->getID(), \max($product->fMindestbestellmenge, 1), $product->Attribute);
        }

        return $this->isValidBannerPayment($customer, $cart);
    }

    /**
     * @inheritDoc
     */
    public function getFrontendInterface(Configuration $config, JTLSmarty $smarty): PaymentFrontendInterface
    {
        return new PPCPFrontend($this->plugin, $this, $smarty);
    }

    /**
     * @inheritDoc
     */
    public function createPPOrder(
        Customer $customer,
        Cart $cart,
        string $fundingSource,
        string $shippingContext,
        string $payAction,
        string $bnCode = MerchantCredentials::BNCODE_CHECKOUT
    ): ?string {
        $fundingSource = $this->validateFundingSource($fundingSource);
        if ($fundingSource === '') {
            $this->logger->write(\LOGLEVEL_ERROR, 'createPPOrder: funding source is empty');

            return null;
        }

        $ppOrder   = $this->getPPOrder();
        $ppOrderId = $ppOrder !== null ? $ppOrder->getId() : null;
        $orderHash = $this->generateHash(new Bestellung());
        $this->addCache('orderHash', $orderHash);
        try {
            if ($ppOrderId === null) {
                $response = $this->createOrder(
                    (new Order())
                        ->setPayer($this->createPayer($customer))
                        ->addPurchase($this->createPurchase($orderHash, Frontend::getDeliveryAddress(), $cart))
                        ->setAppContext($this->createAppContext(
                            $customer->kSprache ?? Shop::Lang()->currentLanguageID,
                            $shippingContext,
                            $payAction
                        ))
                        ->setIntent(Order::INTENT_CAPTURE),
                    $bnCode
                );
                $ppOrder  = $this->getPPOrder();
                $this->logger->write(\LOGLEVEL_DEBUG, 'createPPOrder: createOrderResponse', $response);
            } else {
                $response = $this->verifyPPOrder($ppOrderId);
                if (\in_array($response->getStatus(), [
                    OrderStatus::STATUS_CREATED,
                    OrderStatus::STATUS_APPROVED
                ], true)) {
                    $this->patchOrder($response, Frontend::getCustomer(), $cart, $orderHash);
                    $response = $this->verifyPPOrder($ppOrderId);
                }
                $this->logger->write(\LOGLEVEL_DEBUG, 'createPPOrder: verifyOrderResponse', $response);
            }

            $ppOrderId = $response->getId();
            if ($ppOrder !== null) {
                $ppOrder->setId($ppOrderId)
                        ->setStatus($response->getStatus())
                        ->setPurchases($response->getPurchases());
            } else {
                $ppOrder = (new Order($response->getData()))
                    ->setAppContext($this->createAppContext($customer->kSprache, $shippingContext, $payAction));
                $this->storePPOrder($ppOrder);
            }

            if (!\in_array($response->getStatus(), [
                OrderStatus::STATUS_CREATED,
                OrderStatus::STATUS_APPROVED
            ], true)) {
                // ToDo: Not the expected order state ...
                $this->logger->write(\LOGLEVEL_ERROR, 'createPPOrder: UnexpectedOrderState get '
                    . $response->getStatus() . ' expected ' . OrderStatus::STATUS_CREATED);
                if ($response->getStatus() === OrderStatus::STATUS_APPROVED) {
                    // At this time this is an inkonsistent state! Reload checkout page after pay!?
                    // ...or an error occured during capture the order.
                    $this->unsetCache();
                }
                if ($response->getStatus() === OrderStatus::STATUS_COMPLETED) {
                    // Payment completed!? Page Refresh before order completion!?
                    $this->unsetCache();
                }

                return null;
            }

            return $ppOrderId;
        } catch (PPCRequestException $e) {
            $this->unsetCache();
            try {
                $this->logger->write(\LOGLEVEL_NOTICE, 'createPPOrder: ' . $e->getName(), $e->getResponse()->getData());
            } catch (JsonException | UnexpectedResponseException $e2) {
                $this->logger->write(\LOGLEVEL_NOTICE, 'createPPOrder: ' . $e->getName(), $e->getResponse());
            }
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                Handler::getBackendTranslation($e->getMessage()),
                'createOrderRequest',
                ['saveInSession' => true]
            );
        } catch (Exception | GuzzleException $e) {
            $this->unsetCache();
            $this->logger->write(\LOGLEVEL_NOTICE, 'createPPOrder: OrderResponseFailed - ' . $e->getMessage());
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                $e->getMessage(),
                'createOrderRequest',
                ['saveInSession' => true]
            );
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function preparePaymentProcess(Bestellung $order): void
    {
        parent::preparePaymentProcess($order);

        $ppOrder = $this->getPPOrder();
        if ($ppOrder === null || empty($ppOrder->getId())) {
            return;
        }

        $orderNumber = \baueBestellnummer();
        try {
            $response = $this->verifyPPOrder($ppOrder->getId());
            $this->logger->write(\LOGLEVEL_DEBUG, 'preparePaymentProcess: verifyOrderResponse', $response);

            if (!\in_array($response->getStatus(), $this->getValidOrderProcessStates(), true)) {
                // ToDo: Not the expected order state ...
                $this->logger->write(\LOGLEVEL_ERROR, 'preparePaymentProcess: UnexpectedOrderState get '
                    . $response->getStatus() . ' expected ' . OrderStatus::STATUS_APPROVED);

                return;
            }

            if (\in_array($response->getStatus(), $this->getValidOrderProcessStates(false), true)) {
                try {
                    // ToDo: for debug purposes only - must be removed!!!
                    if (\defined('THROW_PAYER_ACTION_REQUIRED')
                        && $this->getFundingSource() === 'paypal'
                        && ($this->getCache('THROW_PAYER_ACTION_REQUIRED') ?? 'N') === 'N'
                    ) {
                        $this->addCache('THROW_PAYER_ACTION_REQUIRED', 'Y');
                        throw new PPCRequestException(new ClientErrorResponse(new Response(422, [], '{"name": "UNPROCESSABLE_ENTITY","message": "The requested action could not be performed, is semantically incorrect, or failed business validation.","debug_id": "0815","details": [{"issue": "PAYER_ACTION_REQUIRED","description": "Transaction cannot be completed successfully, instruct the buyer to return to PayPal."}]}')), []);
                    }

                    $response = $this->captureOrder(
                        $ppOrder->getId(),
                        $orderNumber,
                        $this->getCache('BNCode') ?? MerchantCredentials::BNCODE_CHECKOUT
                    );
                } catch (Exception | GuzzleException $e) {
                    if ($e instanceof PPCRequestException) {
                        $this->handleOvercharge($ppOrder, $e);
                        $this->logger->write(
                            \LOGLEVEL_NOTICE,
                            'preparePaymentProcess: OrderCaptureResponseFailed',
                            $e->getDetail()
                        );
                    } else {
                        $this->logger->write(
                            \LOGLEVEL_NOTICE,
                            'preparePaymentProcess: OrderCaptureResponseFailed - ' . $e->getMessage()
                        );
                    }
                    // The API-Call failed - try to get the state in case of payment was still succesfully captured
                    $response = $this->verifyPPOrder($ppOrder->getId());
                    if ($response->getStatus() !== OrderStatus::STATUS_COMPLETED) {
                        Shop::Container()->getAlertService()->addAlert(
                            Alert::TYPE_ERROR,
                            $this->plugin->getLocalization()->getTranslation('jtl_paypal_commerce_payment_error'),
                            'preparePaymentProcess',
                            ['saveInSession' => true]
                        );

                        Helper::redirectAndExit(
                            Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php')
                        );
                        exit();
                    }
                }
                $this->logger->write(\LOGLEVEL_DEBUG, 'preparePaymentProcess: captureOrderResponse', $response);
            }

            $ppOrder->setStatus($response->getStatus())
                    ->setPurchases($response->getPurchases())
                    ->setPayer($response->getPayer() ?? ($ppOrder->getPayer() ?? new Payer()));
            if ($response->getStatus() !== OrderStatus::STATUS_COMPLETED) {
                // ToDo: Not the expected order state ...
                $this->logger->write(\LOGLEVEL_ERROR, 'preparePaymentProcess: UnexpectedOrderState get '
                    . $response->getStatus() . ' expected ' . OrderStatus::STATUS_COMPLETED);

                return;
            }

            $orderHash   = $this->getCache('orderHash') ?? $this->generateHash($order);
            $redirectUrl = $this->getNotificationURL($orderHash);
            $ppOrder->getPurchase()->setCustomId($orderHash);
            $ppOrder->setLink((object)[
                'rel'  => 'paymentRedirect',
                'href' => $redirectUrl,
            ]);

            Helper::redirectAndExit($redirectUrl);
            exit();
        } catch (Exception | GuzzleException $e) {
            $this->logger->write(
                \LOGLEVEL_NOTICE,
                'preparePaymentProcess: OrderCaptureResponseFailed - ' . $e->getMessage()
            );
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                $this->plugin->getLocalization()->getTranslation('jtl_paypal_commerce_payment_error'),
                'preparePaymentProcess',
                ['saveInSession' => true]
            );

            Helper::redirectAndExit(Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php'));
            exit();
        }
    }

    /**
     * @inheritDoc
     */
    public function handleNotification(Bestellung $order, string $hash, array $args): void
    {
        parent::handleNotification($order, $hash, $args);

        $ppOrder = $this->getPPOrder();
        if ($ppOrder === null || $ppOrder->getPurchase()->getCustomId() !== $hash) {
            $this->logger->write(\LOGLEVEL_DEBUG, 'handleNotification: ppOrder is empty or custom id does not match.');
            $this->unsetCache();

            return;
        }
        if ($ppOrder->getOrderId() > 0) {
            // ToDo: Order already handled...

            return;
        }

        $ppOrder->unsetLink('paymentRedirect');
        $ppOrder->setOrderId($order->kBestellung);
        $this->addIncomingPayment($order, $ppOrder);
        $this->setOrderStatusToPaid($order);
        $this->sendConfirmationMail($order);
        $this->unsetCache();
    }
}
