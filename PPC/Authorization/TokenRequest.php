<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Authorization;

use GuzzleHttp\Psr7\Uri;
use Plugin\jtl_paypal_commerce\PPC\Environment\EnvironmentInterface;
use Plugin\jtl_paypal_commerce\PPC\Logger;
use Plugin\jtl_paypal_commerce\PPC\Request\MethodType;
use Plugin\jtl_paypal_commerce\PPC\Request\PPCRequest;

/**
 * Class TokenRequest
 * @package Plugin\jtl_paypal_commerce\PPC\Authorization
 */
class TokenRequest extends PPCRequest
{
    /**
     * TokenRequest constructor.
     * @param EnvironmentInterface $environment
     * @param string|null          $token
     */
    public function __construct(EnvironmentInterface $environment, ?string $token = null)
    {
        $uriString = '/v1/oauth2/token';
        $head      = [
            'Authorization' => 'Basic ' . $environment->getAuthorizationString(),
            'Content-Type'  => 'application/x-www-form-urlencoded',
        ];
        $body      = \http_build_query(
            $token === null
            ? [
                'grant_type' => 'client_credentials',
            ]
            : [
                'grant_type'    => 'refresh_token',
                'refresh_token' => $token,
            ]
        );

        (new Logger(Logger::TYPE_ONBOARDING))->write(\LOGLEVEL_DEBUG, 'Refresh Token.. ', $uriString);
        parent::__construct(new Uri($uriString), MethodType::POST, $head, $body);
    }
}
