<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Webhook;

use Plugin\jtl_paypal_commerce\PPC\Request\AuthorizedRequest;
use Plugin\jtl_paypal_commerce\PPC\Request\MethodType;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\JSON;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\SerializerInterface;

/**
 * Class WebhookDetailsRequest
 * @package Plugin\jtl_paypal_commerce\PPC\Webhook
 */
class WebhookDetailsRequest extends AuthorizedRequest
{
    /** @var string */
    private $webhookId;

    /**
     * WebhookDetailsRequest constructor.
     * @param string $token
     * @param string $webhookId
     */
    public function __construct(string $token, string $webhookId)
    {
        $this->webhookId = $webhookId;
        parent::__construct($token, MethodType::GET);
    }

    /**
     * @return SerializerInterface
     */
    protected function initBody(): SerializerInterface
    {
        return new JSON();
    }

    /**
     * @return string
     */
    protected function getPath(): string
    {
        return '/v1/notifications/webhooks/' . $this->webhookId;
    }
}
