<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Order\Payment;

use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\JSON;

/**
 * Class CardDetails
 * @package Plugin\jtl_paypal_commerce\PPC\Order\Payment
 */
class CardDetails extends JSON
{
    /**
     * CardPaymentSource constructor
     * @param object|null $data
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data ?? (object)[]);
    }

    /**
     * @inheritDoc
     */
    public function setData($data)
    {
        parent::setData($data);

        $authResult = $this->getData()->authentication_result ?? null;
        if ($authResult !== null && !($authResult instanceof AuthResult)) {
            $this->setAuthResult(new AuthResult($authResult));
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->getData()->type ?? '';
    }

    /**
     * @param string $type
     * @return self
     */
    public function setType(string $type): self
    {
        $this->data->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->getData()->brand ?? '';
    }

    /**
     * @param string $brand
     * @return self
     */
    public function setBrand(string $brand): self
    {
        $this->data->brand = $brand;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastDigits(): string
    {
        return $this->getData()->last_digits ?? '';
    }

    /**
     * @param string $lastDigits
     * @return self
     */
    public function setLastDigits(string $lastDigits): self
    {
        $this->data->last_digits = $lastDigits;

        return $this;
    }

    /**
     * @return AuthResult|null
     */
    public function getAuthResult(): ?AuthResult
    {
        return $this->getData()->authentication_result ?? null;
    }

    /**
     * @param AuthResult $authResult
     * @return self
     */
    public function setAuthResult(AuthResult $authResult): self
    {
        $this->data->authentication_result = $authResult;

        return $this;
    }
}
