<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Order\Purchase;

use JTL\Cart\Cart;
use JTL\Cart\CartItem;
use JTL\Catalog\Currency;
use JTL\Helpers\Tax;
use JTL\Shop;
use Plugin\jtl_paypal_commerce\PPC\Order\Amount;
use Plugin\jtl_paypal_commerce\PPC\Order\AmountWithBreakdown;
use Plugin\jtl_paypal_commerce\PPC\Order\Capture;
use Plugin\jtl_paypal_commerce\PPC\Order\InvalidAmountException;
use Plugin\jtl_paypal_commerce\PPC\Order\Shipping;
use Plugin\jtl_paypal_commerce\PPC\PPCHelper;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\JSON;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\SerializerInterface;
use function Functional\first;

/**
 * Class PurchaseUnit
 * @package Plugin\jtl_paypal_commerce\PPC\Order
 */
class PurchaseUnit extends JSON
{
    public const REFERENCE_DEFAULT = 'default';

    /**
     * PurchaseUnit constructor.
     * @param object|null $data
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data ?? (object)[]);
    }

    /**
     * @inheritDoc
     */
    public function setData($data)
    {
        parent::setData($data);

        $shippingData = $this->getData()->shipping ?? null;
        if ($shippingData !== null && !($shippingData instanceof Shipping)) {
            $this->setShipping(new Shipping($shippingData));
        }
        $amountData = $this->getData()->amount ?? null;
        if ($amountData !== null && !($amountData instanceof Amount)) {
            $this->setAmount((isset($amountData->breakdown)
                ? new AmountWithBreakdown($amountData)
                : new Amount())->setData($amountData));
        }
        $items = $this->getData()->items ?? [];
        foreach (\array_keys($items) as $key) {
            if ($items[$key] !== null && !($items[$key] instanceof PurchaseItem)) {
                $items[$key] = new PurchaseItem($items[$key]);
            }
        }
        $this->setItems($items);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getReferenceId(): ?string
    {
        return $this->data->reference_id ?? null;
    }

    /**
     * @param string|null $referenceId
     * @return PurchaseUnit
     */
    public function setReferenceId(?string $referenceId): self
    {
        if ($referenceId === null) {
            unset($this->data->reference_id);
        } else {
            $this->data->reference_id = \mb_substr($referenceId, 0, 256);
        }

        return $this;
    }

    /**
     * @return Amount
     */
    public function getAmount(): Amount
    {
        return $this->data->amount;
    }

    /**
     * @param Amount $amount
     * @return PurchaseUnit
     */
    public function setAmount(Amount $amount): self
    {
        $this->data->amount = $amount;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomId(): ?string
    {
        return $this->data->custom_id ?? null;
    }

    /**
     * @param string|null $customId
     * @return PurchaseUnit
     */
    public function setCustomId(?string $customId): self
    {
        if ($customId === null) {
            unset($this->data->custom_id);
        } else {
            $this->data->custom_id = \mb_substr($customId, 0, 127);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->data->description ?? '';
    }

    /**
     * @param string|null $description
     * @return PurchaseUnit
     */
    public function setDescription(?string $description): self
    {
        if ($description === null) {
            unset($this->data->description);
        } else {
            $this->data->description = PPCHelper::shortenStr($description, 127);
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInvoiceId(): ?string
    {
        return $this->data->invoice_id ?? null;
    }

    /**
     * @param string|null $invoiceId
     * @return PurchaseUnit
     */
    public function setInvoiceId(?string $invoiceId): self
    {
        if ($invoiceId === null) {
            unset($this->data->invoice_id);
        } else {
            $this->data->invoice_id = \mb_substr($invoiceId, 0, 127);
        }

        return $this;
    }

    /**
     * @return Shipping|null
     */
    public function getShipping(): ?Shipping
    {
        return $this->data->shipping ?? null;
    }

    /**
     * @param Shipping|null $shipping
     * @return PurchaseUnit
     */
    public function setShipping(?Shipping $shipping): self
    {
        if ($shipping === null) {
            unset($this->data->shipping);
        } else {
            $this->data->shipping = $shipping;
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSoftDescriptor(): ?string
    {
        return $this->data->soft_descriptor ?? null;
    }

    /**
     * @param string|null $softDescriptor
     * @return PurchaseUnit
     */
    public function setSoftDescriptor(?string $softDescriptor): self
    {
        if ($softDescriptor === null) {
            unset($this->data->soft_descriptor);
        } else {
            $this->data->soft_descriptor = PPCHelper::shortenStr($softDescriptor, 22);
        }

        return $this;
    }

    /**
     * @return array|null
     */
    public function getCaptures(): ?array
    {
        $captures = $this->getData()->payments->captures ?? null;

        if ($captures === null) {
            return null;
        }

        foreach (\array_keys($captures) as $key) {
            if (!($captures[$key] instanceof Capture)) {
                $captures[$key] = new Capture($captures[$key]);
            }
        }

        return $captures;
    }

    /**
     * @param string|null $captureId
     * @return Capture|null
     */
    public function getCapture(?string $captureId = null): ?Capture
    {
        $captures = $this->getCaptures();

        if ($captures === null || count($captures) === 0) {
            return null;
        }

        return first($captures, static function (Capture $item) use ($captureId) {
            return $captureId === null || $item->getId() === $captureId;
        });
    }

    /**
     * @return PurchaseItem[]
     */
    public function getItems(): array
    {
        return $this->data->items;
    }

    /**
     * @param PurchaseItem[] $items
     * @return self
     */
    public function setItems(array $items): self
    {
        $this->data->items = $items;

        return $this;
    }

    /**
     * @param PurchaseItem $item
     * @return self
     */
    public function addItem(PurchaseItem $item): self
    {
        $this->data->items[] = $item;

        return $this;
    }

    /**
     * ToDo: The function currently does not support merchants!!! Will be used only for PUI!
     * @param Cart     $cart
     * @param Currency $currency
     * @return PurchaseItem[]
     */
    public static function createItemsFromCart(Cart $cart, Currency $currency): array
    {
        $items        = [];
        $currencyCode = $currency->getCode();
        foreach ($cart->PositionenArr as $cartPos) {
            if (\in_array($cartPos->nPosTyp, [
                \C_WARENKORBPOS_TYP_ARTIKEL,
                \C_WARENKORBPOS_TYP_GRATISGESCHENK
            ], true)) {
                $taxRate = CartItem::getTaxRate($cartPos);
                $name    = \is_array($cartPos->cName) ? $cartPos->cName[Shop::getLanguageCode()] : $cartPos->cName;
                $netto   = $cartPos->fPreisEinzelNetto * $cartPos->nAnzahl;
                $gross   = Tax::getGross($netto, $taxRate);
                // because PayPal does not support fractional quantities
                // and to avoid rounding differences: quantity will always set to 1
                $quantity = 1;
                if ($cartPos->nAnzahl > 1) {
                    $qty   = \number_format(
                        $cartPos->nAnzahl,
                        \is_float($cartPos->nAnzahl) ? 2 : 0,
                        $currency->getDecimalSeparator(),
                        ''
                    );
                    $name .= ' (' . $qty . ' ' . ($cartPos->cEinheit ?? 'x') . ')';
                }

                $items[] = (new PurchaseItem())
                    ->setName($name)
                    ->setCategory(PurchaseItem::CATEGORY_PHYSICAL) // Todo: set correct item category
                    ->setQuantity($quantity)
                    ->setAmount((new Amount())
                        ->setValue(Currency::convertCurrency($netto, $currencyCode))
                        ->setCurrencyCode($currencyCode))
                    ->setTax((new Amount())
                        ->setValue(Currency::convertCurrency($gross - $netto, $currencyCode))
                        ->setCurrencyCode($currencyCode))
                    ->setTaxrate($taxRate);
            }
        }

        return $items;
    }

    /**
     * ToDo: The function currently does not support merchants!!! Will be used only for PUI!
     * @param Cart                $cart
     * @param Currency            $currency
     * @param AmountWithBreakdown $amount
     * @return self
     */
    public function addItemsFromCart(Cart $cart, Currency $currency, AmountWithBreakdown $amount): self
    {
        $this->setItems(self::createItemsFromCart($cart, $currency));
        $amount->setItemTotal($amount->getItemTotal()->setValue(\round($this->getTotalItemValue(), 2)))
            ->setTaxTotal($amount->getTaxTotal()->setValue(\round($this->getTotalItemTax(), 2)))
            ->adjustTotal();

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalItemValue(): float
    {
        $total = 0.0;
        foreach ($this->getItems() as $item) {
            $total += (\round($item->getAmount()->getValue(), 2) * $item->getQuantity());
        }

        return $total;
    }

    /**
     * @return float
     */
    public function getTotalItemTax(): float
    {
        $total = 0.0;
        foreach ($this->getItems() as $item) {
            $total += (\round($item->getTax()->getValue(), 2) * $item->getQuantity());
        }

        return $total;
    }

    /**
     * @return bool
     */
    public function validateItems(): bool
    {
        $amount = $this->getAmount();
        if ($amount instanceof AmountWithBreakdown) {
            if (!$amount->validateBreakdown()) {
                return false;
            }

            return \round($amount->getItemTotal()->getValue(), 2) === \round($this->getTotalItemValue(), 2)
                && \round($amount->getTaxTotal()->getValue(), 2) === \round($this->getTotalItemTax(), 2);
        }

        return \count($this->getItems()) === 0;
    }

    /**
     * @return bool
     */
    public function validateCurrencyCode(): bool
    {
        $amount = $this->getAmount();
        if ($amount instanceof AmountWithBreakdown && !$amount->validateCurrencyCode()) {
            return false;
        }

        $cCode = $amount->getCurrencyCode();
        foreach ($this->getItems() as $item) {
            if ($item->getAmount()->getCurrencyCode() !== $cCode) {
                return false;
            }
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        $data = clone $this->getData();

        if (empty($data->shipping) || ($data->shipping instanceof SerializerInterface && $data->shipping->isEmpty())) {
            unset($data->shipping);
        }
        if (!\is_array($data->items) || \count($data->items) === 0) {
            unset($data->items);
        } elseif (!$this->validateItems()) {
            $amount = $this->getAmount();
            throw new InvalidAmountException(
                \sprintf(
                    'Total amount of %f differs from %f.',
                    \round(($amount instanceof AmountWithBreakdown)
                        ? $amount->getItemTotal()->getValue()
                        : $this->getAmount()->getValue(), 2),
                    \round($this->getTotalItemValue(), 2)
                )
            );
        } elseif (!$this->validateCurrencyCode()) {
            throw new InvalidAmountException(
                'Currency code of total amount differs from currency code in items.'
            );
        }

        return $data;
    }
}
