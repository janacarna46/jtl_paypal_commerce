<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC;

use Exception;
use Illuminate\Support\Collection;
use JTL\Shop;
use Plugin\jtl_paypal_commerce\frontend\CheckoutPage;

class BackendUIsettings
{
    public const BACKEND_SETTINGS_PANEL_CREDENTIALS         = 'credentials';
    public const BACKEND_SETTINGS_PANEL_DISPLAY             = 'display';
    public const BACKEND_SETTINGS_PANEL_EXPRESSBUY          = 'expressBuy';
    public const BACKEND_SETTINGS_PANEL_ACDC                = 'ACDC';
    public const BACKEND_SETTINGS_PANEL_INSTALMENTBANNER    = 'instalmentBanner';
    public const BACKEND_SETTINGS_PANEL_GENERAL             = 'general';
    public const BACKEND_SETTINGS_PANEL_PAYMENTMETHODSPANEL = 'paymentMethodsPanel';
    public const BACKEND_SETTINGS_PANELS                    = [
        self::BACKEND_SETTINGS_PANEL_CREDENTIALS,
        self::BACKEND_SETTINGS_PANEL_DISPLAY,
        self::BACKEND_SETTINGS_PANEL_EXPRESSBUY,
        self::BACKEND_SETTINGS_PANEL_ACDC,
        self::BACKEND_SETTINGS_PANEL_INSTALMENTBANNER,
        self::BACKEND_SETTINGS_PANEL_GENERAL,
        self::BACKEND_SETTINGS_PANEL_PAYMENTMETHODSPANEL
    ];

    public const BACKEND_SETTINGS_SECTION_CREDENTIALS          = 'credentials';
    public const BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS     = 'smartPaymentButtons';
    public const BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY    = 'expressBuyDisplay';
    public const BACKEND_SETTINGS_SECTION_ACDCDISPLAY          = 'ACDCDisplay';
    public const BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP = 'instalmentBannerDisplay';
    public const BACKEND_SETTINGS_SECTION_CONSENTMANAGER       = 'consentManager';
    public const BACKEND_SETTINGS_SECTION_GENERAL              = 'general';
    public const BACKEND_SETTINGS_SECTION_PAYMENTMETHODS       = 'paymentMethods';
    public const BACKEND_SETTINGS_SECTIONS                     = [
        self::BACKEND_SETTINGS_SECTION_CREDENTIALS,
        self::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS,
        self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY,
        self::BACKEND_SETTINGS_SECTION_ACDCDISPLAY,
        self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
        self::BACKEND_SETTINGS_SECTION_CONSENTMANAGER,
        self::BACKEND_SETTINGS_SECTION_GENERAL,
        self::BACKEND_SETTINGS_SECTION_PAYMENTMETHODS
    ];

    public const COMPONENT_FUNDING_ELIGIBILITY = 'funding-eligibility';
    public const COMPONENT_BUTTONS             = 'buttons';
    public const COMPONENT_HOSTED_FIELDS       = 'hosted-fields';
    public const COMPONENT_MARKS               = 'marks';
    public const COMPONENT_MESSAGES            = 'messages';

    /**
     * @return Collection
     * @throws Exception
     */
    public static function getDefaultSettings(): Collection
    {
        $template    = Shop::Container()->getTemplateService()->getActiveTemplate()->getName() ?? 'custom';
        $tplDefaults = self::getTemplateDefaults($template);

        return (new Collection([
            'merchantID'                                                 => [
                'value'       => '',
                'type'        => 'password',
                'class'       => '',
                'panel'       => self::BACKEND_SETTINGS_PANEL_CREDENTIALS,
                'section'     => self::BACKEND_SETTINGS_SECTION_CREDENTIALS,
                'sort'        => 1,
                'label'       => \__('Merchant ID'),
                'description' => \__('Ihre PayPal Merchant ID'),
            ],
            'clientID'                                                   => [
                'value'       => '',
                'type'        => 'password',
                'class'       => '',
                'panel'       => self::BACKEND_SETTINGS_PANEL_CREDENTIALS,
                'section'     => self::BACKEND_SETTINGS_SECTION_CREDENTIALS,
                'sort'        => 1,
                'label'       => \__('Client ID'),
                'description' => \__('Ihre PayPal Client ID'),
            ],
            'clientSecret'                                               => [
                'value'       => '',
                'type'        => 'password',
                'class'       => '',
                'sort'        => 2,
                'panel'       => self::BACKEND_SETTINGS_PANEL_CREDENTIALS,
                'section'     => self::BACKEND_SETTINGS_SECTION_CREDENTIALS,
                'label'       => \__('Client Secret'),
                'description' => \__('Ihr PayPal Client Secret')
            ],
            self::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS . '_shape'  => [
                'value'        => 'rect',
                'type'         => 'selectbox',
                'class'        => '',
                'panel'        => self::BACKEND_SETTINGS_PANEL_DISPLAY,
                'section'      => self::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS,
                'sort'         => 1,
                'description'  => \__('Hier können Sie die Form der Zahlungsbuttons ändern'),
                'label'        => \ucfirst(\__('Form')),
                'options'      => [
                    [
                        'value' => 'pill',
                        'label' => \__('abgerundet'),
                    ],
                    [
                        'value' => 'rect',
                        'label' => \__('rechteckig (abgerundete Ecken)'),
                    ],
                ]
            ],
            self::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS . '_color'  => [
                'value'     => 'blue',
                'type'      => 'selectbox',
                'class'     => '',
                'panel'     => self::BACKEND_SETTINGS_PANEL_DISPLAY,
                'section'   => self::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS,
                'sort'      => 2,
                'label'     => \__('Farbschema'),
                'loadAfter'    => 'snippets/buttonPreview.tpl',
                'description' => \__('Hier können Sie die Farbe der Zahlungsbuttons ändern'),
                'options'  => [
                    [
                        'value' => 'blue',
                        'label' => \__('blau'),
                    ],
                    [
                        'value' => 'black',
                        'label' => \__('schwarz'),
                    ],
                    [
                        'value' => 'white',
                        'label' => \__('weiß'),
                    ],
                    [
                        'value' => 'silver',
                        'label' => \__('silber'),
                    ],
                    [
                        'value' => 'gold',
                        'label' => \__('gold'),
                    ],
                ]
            ],
            self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY . '_activate'             => [
                'value'        => 'Y',
                'type'         => 'selectbox',
                'class'        => '',
                'panel'        => self::BACKEND_SETTINGS_PANEL_EXPRESSBUY,
                'section'      => self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY,
                'sort'         => 10,
                'description'  => \__('Hier können Sie Expresskauf aktivieren'),
                'label'        => \__('Expresskauf aktivieren'),
                'loadAfter' => 'snippets/selectToggleScript.tpl',
                'options'      => [
                    [
                        'value' => 'Y',
                        'label' => \__('Ja'),
                    ],
                    [
                        'value' => 'N',
                        'label' => \__('Nein'),
                    ],
                ]
            ],
            self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY . '_handleInvoiceAddress' => [
                'value'        => 'E',
                'type'         => 'selectbox',
                'panel'        => self::BACKEND_SETTINGS_PANEL_EXPRESSBUY,
                'section'      => self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY,
                'sort'         => 15,
                'class'        => 'd-none '
                    . self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY
                    . '_activate_toggle',
                'description'  => \__(
                    'Hier legen Sie fest, wie fehlende Rechnungsadressdaten vervollständigt werden sollen'
                ),
                'label'        => \__('Fehlende Adressdaten vervollständigen'),
                'options'      => [
                    [
                        'value' => 'E',
                        'label' => \__('Aus Lieferadresse ergänzen und zur Überprüfung anzeigen'),
                    ],
                    [
                        'value' => 'Y',
                        'label' => \__('Aus Lieferadresse ergänzen und fortfahren'),
                    ],
                    [
                        'value' => 'N',
                        'label' => \__('Fehlende Daten durch Kunden ergänzen lassen'),
                    ],
                ]
            ],
            self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY . '_showInProductDetails' => [
                'value'        => 'Y',
                'type'         => 'selectbox',
                'panel'        => self::BACKEND_SETTINGS_PANEL_EXPRESSBUY,
                'section'      => self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY,
                'sort'         => 20,
                'class'        => 'd-none '
                    . self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY
                    . '_activate_toggle singleActivator_'
                    . self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY,
                'description'  => \__('Expresskauf in den Artikeldetails anzeigen'),
                'label'        => \__('Expresskauf in den Artikeldetails anzeigen'),
                'loadAfter'    => 'snippets/advancedSettings.tpl',
                'options'      => [
                    [
                        'value' => 'Y',
                        'label' => \__('Ja'),
                    ],
                    [
                        'value' => 'N',
                        'label' => \__('Nein'),
                    ],
                ]
            ],
            self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY . '_showInCart'           => [
                'value'        => 'Y',
                'type'         => 'selectbox',
                'panel'        => self::BACKEND_SETTINGS_PANEL_EXPRESSBUY,
                'section'      => self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY,
                'sort'         => 30,
                'class'        => 'd-none '
                    . self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY
                    . '_activate_toggle singleActivator_'
                    . self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY,
                'description'  => \__('Expresskauf im Warenkorb anzeigen'),
                'label'        => \__('Expresskauf im Warenkorb anzeigen'),
                'loadAfter'    => 'snippets/advancedSettings.tpl',
                'options'      => [
                    [
                        'value' => 'Y',
                        'label' => \__('Ja'),
                    ],
                    [
                        'value' => 'N',
                        'label' => \__('Nein'),
                    ],
                ]
            ],
            self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY . '_showInOrderProcess'   => [
                'value'        => 'Y',
                'type'         => 'selectbox',
                'panel'        => self::BACKEND_SETTINGS_PANEL_EXPRESSBUY,
                'section'      => self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY,
                'sort'         => 40,
                'class'        => 'd-none '
                    . self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY
                    . '_activate_toggle singleActivator_'
                    . self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY,
                'description'  => \__('Anzeige im Bestellvorgang'),
                'label'        => \__('Anzeige im Bestellvorgang'),
                'loadAfter'    => 'snippets/advancedSettings.tpl',
                'options'      => [
                    [
                        'value' => 'Y',
                        'label' => \__('Ja'),
                    ],
                    [
                        'value' => 'N',
                        'label' => \__('Nein'),
                    ],
                ]
            ],
            self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY . '_showInMiniCart'       => [
                'value'        => 'Y',
                'type'         => 'selectbox',
                'panel'        => self::BACKEND_SETTINGS_PANEL_EXPRESSBUY,
                'section'      => self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY,
                'vars'         => [
                    'section' => self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY
                ],
                'sort'         => 50,
                'class'        => 'd-none '
                    . self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY
                    . '_activate_toggle singleActivator_'
                    . self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY,
                'description'  => \__('Expresskauf im Mini-Warenkorb anzeigen'),
                'label'        => \__('Expresskauf im Mini-Warenkorb anzeigen'),
                'loadAfter'    => ['snippets/advancedSettings.tpl','snippets/advancedSettingsScript.tpl'],
                'options'      => [
                    [
                        'value' => 'Y',
                        'label' => \__('Ja'),
                    ],
                    [
                        'value' => 'N',
                        'label' => \__('Nein'),
                    ],
                ]
            ],
            self::BACKEND_SETTINGS_SECTION_ACDCDISPLAY . '_activate3DSecure'              => [
                'value'        => 'Y',
                'type'         => 'selectbox',
                'class'        => 'singleActivator_'
                    . self::BACKEND_SETTINGS_SECTION_ACDCDISPLAY,
                'panel'        => self::BACKEND_SETTINGS_PANEL_ACDC,
                'section'      => self::BACKEND_SETTINGS_SECTION_ACDCDISPLAY,
                'sort'         => 10,
                'description'  => \__('3D Secure aktivieren (Empfohlen)'),
                'label'        => \__('3D Secure aktivieren'),
                'loadAfter'    => 'snippets/selectToggleScript.tpl',
                'options'      => [
                    [
                        'value' => 'Y',
                        'label' => \__('Ja (Empfohlen)'),
                    ],
                    [
                        'value' => 'N',
                        'label' => \__('Nein'),
                    ],
                ],
            ],
            self::BACKEND_SETTINGS_SECTION_ACDCDISPLAY . '_mode3DSecure'              => [
                'value'        => 'SCA_WHEN_REQUIRED',
                'type'         => 'selectbox',
                'class'        => 'd-none '
                    . self::BACKEND_SETTINGS_SECTION_ACDCDISPLAY
                    . '_activate3DSecure_toggle',
                'panel'        => self::BACKEND_SETTINGS_PANEL_ACDC,
                'section'      => self::BACKEND_SETTINGS_SECTION_ACDCDISPLAY,
                'sort'         => 20,
                'description'  => \__('3D Secure SCA Beschreibung'),
                'label'        => \__('3D Secure SCA'),
                'options'      => [
                    [
                        'value' => 'SCA_WHEN_REQUIRED',
                        'label' => \__('Wenn benötigt (Empfohlen)'),
                    ],
                    [
                        'value' => 'SCA_ALWAYS',
                        'label' => \__('Immer'),
                    ],
                ],
            ],
            self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP . '_activate'             => [
                'value'        => 'Y',
                'type'         => 'selectbox',
                'class'        => '',
                'panel'        => self::BACKEND_SETTINGS_PANEL_INSTALMENTBANNER,
                'section'      => self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'sort'         => 10,
                'description'  => \__('PayPal Ratenzahlung Banner aktivieren'),
                'label'        => \__('PayPal Ratenzahlung Banner aktivieren'),
                'loadAfter'    => 'snippets/selectToggleScript.tpl',
                'options'      => [
                    [
                        'value' => 'Y',
                        'label' => \__('Ja'),
                    ],
                    [
                        'value' => 'N',
                        'label' => \__('Nein'),
                    ],
                ],
            ],
            self::BACKEND_SETTINGS_SECTION_CONSENTMANAGER . '_activate'       => [
                'value'        => 'N',
                'type'         => 'selectbox',
                'panel'        => self::BACKEND_SETTINGS_PANEL_GENERAL,
                'section'      => self::BACKEND_SETTINGS_SECTION_GENERAL,
                'sort'         => 15,
                'class'        => '',
                'description'  => \__('consentManagerActivateDescription'),
                'label'        => \__('Consent-Manager aktivieren'),
                'options'      => [
                    [
                        'value' => 'Y',
                        'label' => \__('Ja'),
                    ],
                    [
                        'value' => 'N',
                        'label' => \__('Nein'),
                    ],
                ]
            ],
            self::BACKEND_SETTINGS_SECTION_GENERAL . '_templateSupport'       => [
                'value'        => $template,
                'type'         => 'selectbox',
                'vars'         => [
                    'scopes'      => CheckoutPage::PAGE_SCOPES,
                    'tplDefaults' => self::getTemplateDefaults(),
                    'sections'    => [
                        self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY,
                        self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP
                    ],
                ],
                'class'        => '',
                'panel'        => self::BACKEND_SETTINGS_PANEL_GENERAL,
                'section'      => self::BACKEND_SETTINGS_SECTION_GENERAL,
                'sort'         => 20,
                'description'  => \__('templateSupportDescription'),
                'label'        => \__('Template Unterstützung'),
                'loadAfter'    => 'snippets/templateSupportScript.tpl',
                'options'      => [
                    [
                        'value' => 'NOVA',
                        'label' => \__('NOVA'),
                    ],
                    [
                        'value' => 'custom',
                        'label' => \__('Individuell'),
                    ],
                ]
            ],
            self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP . '_showInProductDetails' => [
                'value'        => 'Y',
                'type'         => 'selectbox',
                'panel'        => self::BACKEND_SETTINGS_PANEL_INSTALMENTBANNER,
                'section'      => self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'sort'         => 20,
                'class'        => 'd-none '
                    . self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP
                    . '_activate_toggle singleActivator_'
                    . self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'description'  => \__('Anzeige auf der Artikeldetailseite'),
                'label'        => \__('Anzeige auf der Artikeldetailseite'),
                'loadAfter'    => 'snippets/advancedSettings.tpl',
                'options'      => [
                    [
                        'value' => 'Y',
                        'label' => \__('Ja'),
                    ],
                    [
                        'value' => 'N',
                        'label' => \__('Nein'),
                    ],
                ]
            ],
            self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP . '_showInCart'           => [
                'value'        => 'Y',
                'type'         => 'selectbox',
                'panel'        => self::BACKEND_SETTINGS_PANEL_INSTALMENTBANNER,
                'section'      => self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'sort'         => 30,
                'class'        => 'd-none '
                    . self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP
                    . '_activate_toggle singleActivator_'
                    . self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'description'  => \__('Anzeige im Warenkorb'),
                'label'        => \__('Anzeige im Warenkorb'),
                'loadAfter'    => 'snippets/advancedSettings.tpl',
                'options'      => [
                    [
                        'value' => 'Y',
                        'label' => \__('Ja'),
                    ],
                    [
                        'value' => 'N',
                        'label' => \__('Nein'),
                    ],
                ]
            ],
            self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP . '_showInOrderProcess'   => [
                'value'        => 'Y',
                'type'         => 'selectbox',
                'panel'        => self::BACKEND_SETTINGS_PANEL_INSTALMENTBANNER,
                'section'      => self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'sort'         => 40,
                'class'        => 'd-none '
                    . self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP
                    . '_activate_toggle singleActivator_'
                    . self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'description'  => \__('Anzeige im Bestellvorgang'),
                'label'        => \__('Anzeige im Bestellvorgang'),
                'loadAfter'    => 'snippets/advancedSettings.tpl',
                'options'      => [
                    [
                        'value' => 'Y',
                        'label' => \__('Ja'),
                    ],
                    [
                        'value' => 'N',
                        'label' => \__('Nein'),
                    ],
                ]
            ],
            self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP . '_showInMiniCart'       => [
                'value'        => 'Y',
                'type'         => 'selectbox',
                'panel'        => self::BACKEND_SETTINGS_PANEL_INSTALMENTBANNER,
                'vars'         => [
                    'section' => self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP
                ],
                'section'      => self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'sort'         => 50,
                'class'        => 'd-none '
                    . self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP
                    . '_activate_toggle singleActivator_'
                    . self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'description'  => \__('Anzeige im Mini-Warenkorb'),
                'label'        => \__('Anzeige im Mini-Warenkorb'),
                'loadAfter'    => ['snippets/advancedSettings.tpl','snippets/advancedSettingsScript.tpl'],
                'options'      => [
                    [
                        'value' => 'Y',
                        'label' => \__('Ja'),
                    ],
                    [
                        'value' => 'N',
                        'label' => \__('Nein'),
                    ],
                ]
            ],
            self::BACKEND_SETTINGS_SECTION_PAYMENTMETHODS . '_enabled'                => [
                'value'       => APM::APM_ALL,
                'type'        => 'activationList',
                'vars'        => [
                    'selectGroups' => [
                        'cards_fundingSource'  => APM::APM_CARDS,
                        'credit_fundingSource' => APM::APM_CREDIT,
                        'bank_fundingSource'   => APM::APM_BANK
                    ]
                ],
                'class'       => '',
                'panel'       => self::BACKEND_SETTINGS_PANEL_PAYMENTMETHODSPANEL,
                'section'     => self::BACKEND_SETTINGS_SECTION_PAYMENTMETHODS,
                'sort'        => 70,
                'label'       => \__('paymentMethodsPanel'),
                'label2'      => \__('paymentMethodsPanelCountries'),
                'description' => \__('Legen Sie hier fest, welche Zahlungsarten generell angezeigt werden sollen. '
                    . 'Die Tatsächliche Anzeige der Zahlungsarten regelt PayPal anhand der Lokation des Benutzers.'),
                'options'     => (new Collection(APM::APM_ALL))
                    ->map(static function ($item) {
                        return [
                            'label'    => \__($item),
                            'value'    => $item,
                            'extended' => (new Collection(APM::APM_COUNTRIES))
                                ->map(static function ($countries) {
                                    return \implode(', ', \array_map(static function ($country) {
                                        return \__($country);
                                    }, $countries));
                                })->toArray()
                        ];
                    })->toArray()
            ]

        ]))->merge(
            self::getBannerDisplaySettings($tplDefaults),
        )->merge(
            self::getECSDisplaySettings($tplDefaults),
        );
    }

    /**
     * @param array $tplDefaults
     * @param string|null $selection
     * @return array
     */
    public static function getECSDisplaySettings(array $tplDefaults, string $selection = null): array
    {
        $tplDefaults = $tplDefaults[self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY];
        $scopes      = new Collection(CheckoutPage::PAGE_SCOPES);
        $settings    = [];
        $scopes      = empty($selection) ? $scopes :
            $scopes->filter(static function ($item, $key) use ($selection) {
                return $key === $selection;
            });
        $class       = '';

        $scopes = $scopes->map(static function ($item, $key) use ($class, $tplDefaults) {
            switch ($item) {
                case CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS:
                    $selector = $tplDefaults['selector'][CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS];
                    $method   = $tplDefaults['method'][CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS];
                    $class   .= CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS . '_advancedSetting';
                    $sort     = 21;
                    break;
                case CheckoutPage::PAGE_SCOPE_MINICART:
                    $selector = $tplDefaults['selector'][CheckoutPage::PAGE_SCOPE_MINICART];
                    $method   = $tplDefaults['method'][CheckoutPage::PAGE_SCOPE_MINICART];
                    $class   .= CheckoutPage::PAGE_SCOPE_MINICART . '_advancedSetting';
                    $sort     = 51;
                    break;
                case CheckoutPage::PAGE_SCOPE_CART:
                    $selector = $tplDefaults['selector'][CheckoutPage::PAGE_SCOPE_CART];
                    $method   = $tplDefaults['method'][CheckoutPage::PAGE_SCOPE_CART];
                    $class   .= CheckoutPage::PAGE_SCOPE_CART . '_advancedSetting';
                    $sort     = 31;
                    break;
                case CheckoutPage::PAGE_SCOPE_ORDERPROCESS:
                    $selector = $tplDefaults['selector'][CheckoutPage::PAGE_SCOPE_ORDERPROCESS];
                    $method   = $tplDefaults['method'][CheckoutPage::PAGE_SCOPE_ORDERPROCESS];
                    $class   .= CheckoutPage::PAGE_SCOPE_ORDERPROCESS . '_advancedSetting';
                    $sort     = 41;
                    break;
                default:
                    $selector = '';
                    $method   = 'after';
                    $sort     = 0;
            }

            return [
                'name'     => $item,
                'selector' => $selector,
                'method'   => $method,
                'sort'     => $sort,
                'class'    => $class,
            ];
        })->toArray();

        foreach ($scopes as $scope) {
            $settings[self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY . '_' . $scope['name'] . '_phpqSelector'] = [
                'wrapperStart' => '<div class="advancedSettingsWrapper_'
                    . self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY
                    . ' card-border mb-5 pt-3">',
                'value'       => $scope['selector'],
                'type'        => 'text',
                'class'       => $scope['class'],
                'panel'       => self::BACKEND_SETTINGS_PANEL_EXPRESSBUY,
                'section'     => self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY,
                'sort'        => $scope['sort']++,
                'description' => \__('phpqSelectorDescription'),
                'label'       => \__('phpqSelector'),
            ];
            $settings[self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY . '_' . $scope['name'] . '_phpqMethod']   = [
                'wrapperEnd'  => '</div>',
                'value'       => $scope['method'],
                'type'        => 'selectbox',
                'class'       => $scope['class'],
                'panel'       => self::BACKEND_SETTINGS_PANEL_EXPRESSBUY,
                'section'     => self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY,
                'sort'        => $scope['sort']++,
                'description' => \__('phpqMethodDescription'),
                'label'       => \__('phpqMethod'),
                'options'     => [
                    [
                        'value' => 'after',
                        'label' => \__('phpq_after'),
                    ],
                    [
                        'value' => 'append',
                        'label' => \__('phpq_append'),
                    ],
                    [
                        'value' => 'before',
                        'label' => \__('phpq_before'),
                    ],
                    [
                        'value' => 'prepend',
                        'label' => \__('phpq_prepend'),
                    ],
                ]
            ];
        }

        return $settings;
    }

    /**
     * @param array $tplDefaults
     * @param string|null $selection
     * @return array
     */
    public static function getBannerDisplaySettings(array $tplDefaults, string $selection = null): array
    {
        $tplDefaults = $tplDefaults[self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP];
        $scopes      = new Collection(CheckoutPage::PAGE_SCOPES);
        $settings    = [];
        $scopes      = empty($selection) ? $scopes :
            $scopes->filter(static function ($item, $key) use ($selection) {
                return $key === $selection;
            });
        $class       = '';

        $scopes = $scopes->map(static function ($item, $key) use ($class, $tplDefaults) {
            switch ($item) {
                case CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS:
                    $selector = $tplDefaults['selector'][CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS];
                    $method   = $tplDefaults['method'][CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS];
                    $class   .= CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS . '_advancedSetting';
                    $sort     = 21;
                    $values   = [
                        'textColor' => 'black'
                    ];
                    break;
                case CheckoutPage::PAGE_SCOPE_MINICART:
                    $selector = $tplDefaults['selector'][CheckoutPage::PAGE_SCOPE_MINICART];
                    $method   = $tplDefaults['method'][CheckoutPage::PAGE_SCOPE_MINICART];
                    $class   .= CheckoutPage::PAGE_SCOPE_MINICART . '_advancedSetting';
                    $values   = [
                        'layout'    => 'text',
                        'textColor' => 'black'
                    ];
                    $sort     = 51;
                    break;
                case CheckoutPage::PAGE_SCOPE_CART:
                    $selector = $tplDefaults['selector'][CheckoutPage::PAGE_SCOPE_CART];
                    $method   = $tplDefaults['method'][CheckoutPage::PAGE_SCOPE_CART];
                    $class   .= CheckoutPage::PAGE_SCOPE_CART . '_advancedSetting';
                    $values   = [
                        'layout'    => 'text',
                        'textColor' => 'black'
                    ];
                    $sort     = 31;

                    break;
                case CheckoutPage::PAGE_SCOPE_ORDERPROCESS:
                    $selector = $tplDefaults['selector'][CheckoutPage::PAGE_SCOPE_ORDERPROCESS];
                    $method   = $tplDefaults['method'][CheckoutPage::PAGE_SCOPE_ORDERPROCESS];
                    $class   .= CheckoutPage::PAGE_SCOPE_ORDERPROCESS . '_advancedSetting';
                    $values   = [
                        'textColor'   => 'black',
                        'layoutRatio' => '20x1',
                    ];
                    $sort     = 41;
                    break;
                default:
                    $selector = '';
                    $method   = 'after';
                    $sort     = 0;
            }

            return [
                'name'     => $item,
                'selector' => $selector,
                'method'   => $method,
                'sort'     => $sort,
                'class'    => $class,
                'values'   => $values ?? null,
            ];
        })->toArray();

        foreach ($scopes as $scope) {
            $settings[self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP . '_' . $scope['name'] . '_minAmount']   = [
                'wrapperStart' => '<div class="advancedSettingsWrapper_'
                    . self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP
                    . ' card-border mb-5 pt-3">',
                'value'        => 1,
                'class'        => $scope['class'],
                'type'         => 'text',
                'panel'        => self::BACKEND_SETTINGS_PANEL_INSTALMENTBANNER,
                'section'      => self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'sort'         => $scope['sort']++,
                'description'  => \__('minAmountDescription'),
                'label'        => \__('minAmount'),
            ];
            $settings[self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP . '_' . $scope['name'] . '_layout']      = [
                'value'       => $scope['values']['layout'] ?? 'flex',
                'type'        => 'selectbox',
                'class'       => $scope['class'],
                'panel'       => self::BACKEND_SETTINGS_PANEL_INSTALMENTBANNER,
                'section'     => self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'sort'        => $scope['sort']++,
                'description' => \__('layoutDescription'),
                'label'       => \__('layout'),
                'options'     => [
                    [
                        'value' => 'flex',
                        'label' => \__('layout_flex'),
                    ],
                    [
                        'value' => 'text',
                        'label' => \__('layout_text'),
                    ],
                ]
            ];
            $settings[self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP . '_' . $scope['name'] . '_logoType']    = [
                'value'       => 'primary',
                'type'        => 'selectbox',
                'class'       => $scope['class'],
                'panel'       => self::BACKEND_SETTINGS_PANEL_INSTALMENTBANNER,
                'section'     => self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'sort'        => $scope['sort']++,
                'description' => \__('logoTypeDescription'),
                'label'       => \__('logoType'),
                'options'     => [
                    [
                        'value' => 'primary',
                        'label' => \__('logoType_primary'),
                    ],
                    [
                        'value' => 'alternative',
                        'label' => \__('logoType_alternative'),
                    ],
                    [
                        'value' => 'inline',
                        'label' => \__('logoType_inline'),
                    ],
                    [
                        'value' => 'none',
                        'label' => \__('logoType_none'),
                    ],
                ]
            ];
            $settings[self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP . '_' . $scope['name'] . '_textSize']    = [
                'value'       => 12,
                'type'        => 'text',
                'class'       => $scope['class'],
                'panel'       => self::BACKEND_SETTINGS_PANEL_INSTALMENTBANNER,
                'section'     => self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'sort'        => $scope['sort']++,
                'description' => \__('textSizeDescription'),
                'label'       => \__('textSize'),
            ];
            $settings[self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP . '_' . $scope['name'] . '_textColor']   = [
                'value'       => $scope['values']['textColor'] ?? 'white',
                'type'        => 'selectbox',
                'class'       => $scope['class'],
                'panel'       => self::BACKEND_SETTINGS_PANEL_INSTALMENTBANNER,
                'section'     => self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'sort'        => $scope['sort']++,
                'description' => \__('textColor'),
                'label'       => \__('textColor'),
                'options'     => [
                    [
                        'value' => 'white',
                        'label' => \__('textColor_white'),
                    ],
                    [
                        'value' => 'black',
                        'label' => \__('textColor_black'),
                    ],
                ]
            ];
            $settings[self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP . '_' . $scope['name'] . '_layoutRatio'] = [
                'value'       => $scope['values']['layoutRatio'] ?? '8x1',
                'type'        => 'selectbox',
                'class'       => isset($scope['exclude']['layoutRatio']) ? 'd-none' : $scope['class'],
                'panel'       => self::BACKEND_SETTINGS_PANEL_INSTALMENTBANNER,
                'section'     => self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'sort'        => $scope['sort']++,
                'description' => \__('layoutRatioDescription'),
                'label'       => \__('layoutRatio'),
                'options'     => [
                    [
                        'value' => '1x1',
                        'label' => \__('1x1'),
                    ],
                    [
                        'value' => '1x4',
                        'label' => \__('1x4'),
                    ],
                    [
                        'value' => '8x1',
                        'label' => \__('8x1'),
                    ],
                    [
                        'value' => '20x1',
                        'label' => \__('20x1'),
                    ],
                ]
            ];
            $settings[self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP . '_' . $scope['name'] . '_layoutType']  = [
                'value'       => 'white',
                'type'        => 'selectbox',
                'class'       => isset($scope['exclude']['layoutType']) ? 'd-none' : $scope['class'],
                'panel'       => self::BACKEND_SETTINGS_PANEL_INSTALMENTBANNER,
                'section'     => self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'sort'        => $scope['sort']++,
                'description' => \__('layoutTypeDescription'),
                'label'       => \__('layoutType'),
                'options'     => [
                    [
                        'value' => 'white',
                        'label' => \__('layoutType_white'),
                    ],
                    [
                        'value' => 'black',
                        'label' => \__('layoutType_black'),
                    ],
                    [
                        'value' => 'blue',
                        'label' => \__('layoutType_blue'),
                    ],
                    [
                        'value' => 'grey',
                        'label' => \__('layoutType_grey'),
                    ],
                ]
            ];

            $settings[self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP . '_' . $scope['name'] . '_phpqSelector'] = [
                'value'       => $scope['selector'],
                'type'        => 'text',
                'class'       => $scope['class'],
                'panel'       => self::BACKEND_SETTINGS_PANEL_INSTALMENTBANNER,
                'section'     => self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'sort'        => $scope['sort']++,
                'description' => \__('phpqSelectorDescription'),
                'label'       => \__('phpqSelector'),
            ];
            $settings[self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP . '_' . $scope['name'] . '_phpqMethod']   = [
                'wrapperEnd'  => '</div>',
                'value'       => $scope['method'],
                'vars'        => [
                    'scope' => $scope['name']
                ],
                'type'        => 'selectbox',
                'class'       => $scope['class'],
                'panel'       => self::BACKEND_SETTINGS_PANEL_INSTALMENTBANNER,
                'section'     => self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
                'sort'        => $scope['sort']++,
                'description' => \__('phpqMethodDescription'),
                'label'       => \__('phpqMethod'),
                'loadAfter'   => 'snippets/instalmentBannerPreview.tpl',
                'options'     => [
                    [
                        'value' => 'after',
                        'label' => \__('phpq_after'),
                    ],
                    [
                        'value' => 'append',
                        'label' => \__('phpq_append'),
                    ],
                    [
                        'value' => 'before',
                        'label' => \__('phpq_before'),
                    ],
                    [
                        'value' => 'prepend',
                        'label' => \__('phpq_prepend'),
                    ],
                ]
            ];
        }
        return $settings;
    }

    /**
     * @param string|null $tpl
     * @return array
     */
    public static function getTemplateDefaults(string $tpl = null): array
    {
        $defaults = [
            'NOVA' => [
                self::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP => [
                    'selector' => [
                        CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS => '#add-to-cart.product-buy',
                        CheckoutPage::PAGE_SCOPE_CART => '#cart-checkout-btn',
                        CheckoutPage::PAGE_SCOPE_ORDERPROCESS => '#fieldset-payment .row',
                        CheckoutPage::PAGE_SCOPE_MINICART => '.cart-dropdown-buttons:first',
                    ],
                    'method' => [
                        CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS => 'after',
                        CheckoutPage::PAGE_SCOPE_CART => 'after',
                        CheckoutPage::PAGE_SCOPE_ORDERPROCESS => 'after',
                        CheckoutPage::PAGE_SCOPE_MINICART => 'after',
                        ]
                ],
                self::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY => [
                    'selector' => [
                        CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS => '#add-to-cart',
                        CheckoutPage::PAGE_SCOPE_CART => '#cart-checkout-btn',
                        CheckoutPage::PAGE_SCOPE_ORDERPROCESS => '#order_register_or_login',
                        CheckoutPage::PAGE_SCOPE_MINICART => '.cart-dropdown-buttons:first',
                    ],
                    'method' => [
                        CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS => 'append',
                        CheckoutPage::PAGE_SCOPE_CART => 'after',
                        CheckoutPage::PAGE_SCOPE_ORDERPROCESS => 'after',
                        CheckoutPage::PAGE_SCOPE_MINICART => 'append',
                    ],
                ]
            ],
        ];

        return isset($defaults[$tpl], $tpl) ? $defaults[$tpl] : (isset($tpl) ? $defaults['NOVA'] : $defaults);
    }
}
