<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce;

use Exception;
use JTL\Backend\AdminIO;
use JTL\Backend\Notification;
use JTL\Backend\NotificationEntry;
use JTL\Events\Dispatcher;
use JTL\Events\Event;
use JTL\Exceptions\CircularReferenceException;
use JTL\Exceptions\ServiceNotFoundException;
use JTL\Helpers\Form;
use JTL\Helpers\Request;
use JTL\Link\LinkInterface;
use JTL\Plugin\Bootstrapper;
use JTL\Plugin\Plugin;
use JTL\Services\Container;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_paypal_commerce\adminmenu\Controller;
use Plugin\jtl_paypal_commerce\adminmenu\Renderer;
use Plugin\jtl_paypal_commerce\adminmenu\TabNotAvailException;
use Plugin\jtl_paypal_commerce\CronJob\CronHelper;
use Plugin\jtl_paypal_commerce\paymentmethod\Helper;
use Plugin\jtl_paypal_commerce\PPC\BackendUIsettings;
use Plugin\jtl_paypal_commerce\PPC\Configuration;
use Plugin\jtl_paypal_commerce\PPC\Environment\EnvironmentInterface;
use Plugin\jtl_paypal_commerce\PPC\Environment\ProductionEnvironment;
use Plugin\jtl_paypal_commerce\PPC\Environment\SandboxEnvironment;
use Plugin\jtl_paypal_commerce\adminmenu\Handler as BackendHandler;
use Plugin\jtl_paypal_commerce\frontend\Handler as FrontendHandler;
use Plugin\jtl_paypal_commerce\PPC\Webhook\Webhook;

/**
 * Class Bootstrap
 * @package Plugin\jtl_paypal_commerce
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @inheritDoc
     */
    public function boot(Dispatcher $dispatcher): void
    {
        parent::boot($dispatcher);

        $plugin = $this->getPlugin();
        /** @noinspection PhpIncludeInspection */
        require_once $plugin->getPaths()->getBasePath() . 'vendor/autoload.php';
        $this->registerContainer();

        $cronHelper = CronHelper::getInstance(Shop::Container()->get(Configuration::class));
        $dispatcher->listen(Event::MAP_CRONJOB_TYPE, [$cronHelper, 'mappingCronjobType']);
        $dispatcher->listen(Event::GET_AVAILABLE_CRONJOBS, [$cronHelper, 'availableCronjobType']);
        if (Shop::isFrontend()) {
            $handler = new FrontendHandler($plugin, $this->getDB());
            $dispatcher->listen('shop.hook.' . \HOOK_BESTELLVORGANG_PAGE_STEPVERSAND, [$handler, 'pageStepShipping']);
            $dispatcher->listen('shop.hook.' . \HOOK_BESTELLVORGANG_PAGE_STEPZAHLUNG, [$handler, 'pageStepPayment']);
            $dispatcher->listen(
                'shop.hook.' . \HOOK_BESTELLVORGANG_PAGE_STEPLIEFERADRESSE,
                [$handler, 'pageStepAddress']
            );
            $dispatcher->listen(
                'shop.hook.' . \HOOK_BESTELLVORGANG_PAGE_STEPBESTAETIGUNG,
                [$handler, 'pageStepConfirm']
            );
            $dispatcher->listen('shop.hook.' . \HOOK_ARTIKEL_PAGE, [$handler, 'pageStepProductDetails']);
            $dispatcher->listen('shop.hook.' . \HOOK_WARENKORB_PAGE, [$handler, 'pageStepCart']);
            $dispatcher->listen('shop.hook.' . \HOOK_JTL_PAGE, [$handler, 'pageCustomerAccount']);
            $dispatcher->listen('shop.hook.' . \HOOK_SMARTY_OUTPUTFILTER, [$handler, 'smarty']);
            $dispatcher->listen('shop.hook.' . \HOOK_IO_HANDLE_REQUEST, [$handler, 'ioRequest']);
            $dispatcher->listen('shop.hook.' . \CONSENT_MANAGER_GET_ACTIVE_ITEMS, [$handler, 'addConsentItem']);
            $dispatcher->listen('shop.hook.' . \HOOK_BESTELLUNGEN_XML_BEARBEITESET, [$handler, 'updateOrder']);
            $dispatcher->listen(
                'shop.hook.' . \HOOK_BESTELLABSCHLUSS_INC_BESTELLUNGINDB,
                [$handler, 'saveOrder']
            );
            $dispatcher->listen(
                'shop.hook.' . \HOOK_BESTELLABSCHLUSS_INC_BESTELLUNGINDB_ENDE,
                [$handler, 'updateSession']
            );
        } else {
            $handler = new BackendHandler($plugin, $this->getDB());
            $dispatcher->listen(
                'shop.hook.' . \HOOK_IO_HANDLE_REQUEST_ADMIN,
                static function (array $args) use ($handler) {
                    /** @var AdminIO $io */
                    $io = $args['io'];
                    $io->register('jtl_ppc_infos_handleAjax', [$handler, 'handleAjax']);
                    $io->register('jtl_ppc_carrier_mapping', [$handler, 'handleCarrierMapping']);
                }
            );
            $dispatcher->listen('backend.notification', [$this, 'checkPaymentNotifications']);
            $task = Request::postVar('task');
            if (Form::validateToken() && (
                \str_contains($_SERVER['SCRIPT_FILENAME'], '/zahlungsarten.php') !== false ||
                \str_contains($_SERVER['REDIRECT_URL'] ?? $_SERVER['SCRIPT_NAME'], 'paymentmethods') !== false)
            ) {
                Controller::getInstance($this->getPlugin())->run('checkPayment');
            } elseif ($task !== null && Form::validateToken()
                && Request::postInt('kPlugin', Request::getInt('kPlugin')) === $plugin->getID()
            ) {
                Controller::getInstance($this->getPlugin())->run($task);
            }
        }
    }

    /**
     * writes the default settings to the DB during installation,
     * as defined in @see BackendUIsettings
     */
    public function installed(): void
    {
        parent::installed();
        $config = Configuration::getInstance($this->getPlugin(), Shop::Container()->getDB());

        try {
            $settingArray = (BackendUIsettings::getDefaultSettings())->toArray();
        } catch (Exception $e) {
            $settingArray = [];
        }
        $defaultSettings = [];
        foreach ($settingArray as $settingName => $setting) {
            if ($setting['value'] !== '') {
                $defaultSettings[$settingName] = $setting['value'];
            }
        }
        $config->saveConfigItems($defaultSettings);
    }

    /**
     * @inheritDoc
     */
    public function uninstalled(bool $deleteData = true)
    {
        parent::uninstalled($deleteData);

        CronHelper::dropCron();
    }

    /**
     * @return void
     */
    protected function registerContainer(): void
    {
        $container = Shop::Container();
        try {
            $container->setSingleton(Configuration::class, function (Container $container) {
                return Configuration::getInstance($this->getPlugin(), $container->getDB());
            });
            $container->setSingleton(EnvironmentInterface::class, function (Container $container) {
                /** @var Configuration $config */
                $config       = $container->get(Configuration::class);
                $clientType   = $config->getWorkingMode();
                $clientID     = $config->getClientID($clientType);
                $clientSecret = $config->getClientSecret($clientType);

                return $clientType === Configuration::WORKING_MODE_PRODUCTION
                    ? new ProductionEnvironment($clientID, $clientSecret, \md5(\session_id()))
                    : new SandboxEnvironment($clientID, $clientSecret, \md5(\session_id()));
            });
        } catch (Exception $e) {
            try {
                Shop::Container()->getLogService()->addAlert('Can not register service. (' . $e->getMessage() . ')');
            } catch (CircularReferenceException | ServiceNotFoundException $e) {
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function renderAdminMenuTab(string $tabName, int $menuID, JTLSmarty $smarty): string
    {
        $tabMapping = Renderer::TAB_MAPPINGS;
        if (isset($tabMapping[$tabName])) {
            try {
                return (new Renderer($this->getPlugin(), $menuID, $smarty))->render($tabMapping[$tabName]);
            } catch (TabNotAvailException $e) {
                /** @var Plugin $plugin */
                $plugin = $smarty->getTemplateVars('oPlugin');
                if ($plugin !== null) {
                    $plugin->getAdminMenu()->removeItem($menuID);
                }
            }
        }

        return parent::renderAdminMenuTab($tabName, $menuID, $smarty);
    }

    /**
     * @return void
     */
    public function checkPaymentNotifications(): void
    {
        $notificationHelper = Notification::getInstance();
        /** @var Configuration $config */
        $config = Shop::Container()->get(Configuration::class);
        $plugin = $this->getPlugin();

        if (!$config->isAuthConfigured()) {
            $entry = new NotificationEntry(
                NotificationEntry::TYPE_INFO,
                \__($plugin->getMeta()->getName()),
                \__('Bitte schließen Sie die Konfiguration ab.'),
                Shop::getAdminURL() . '/plugin.php?kPlugin=' . $plugin->getID()
            );
            $entry->setPluginId($plugin->getPluginID());
            $notificationHelper->addNotify($entry);

            return;
        }

        $this->checkNotificationSandboxMode($notificationHelper);
        $this->checkNotificationWebhook($notificationHelper, $config);
    }

    /**
     * @param Notification $notificationHelper
     */
    private function checkNotificationSandboxMode(Notification $notificationHelper): void
    {
        $plugin        = $this->getPlugin();
        $paymentActive = false;

        foreach ($plugin->getPaymentMethods()->getMethods() as $paymentMethod) {
            $paypalPayment = Helper::getInstance($plugin)->getPaymentFromID($paymentMethod->getMethodID());
            if ($paypalPayment !== null) {
                $paymentActive = $paymentActive || $paypalPayment->isAssigned();
                if (($entry = $paypalPayment->getBackendNotification($plugin)) !== null) {
                    $notificationHelper->addNotify($entry);
                }
            }
        }

        /** @var EnvironmentInterface $environment */
        $environment = Shop::Container()->get(EnvironmentInterface::class);
        if ($paymentActive && $environment !== null && $environment->isSandbox()) {
            $entry = new NotificationEntry(
                NotificationEntry::TYPE_WARNING,
                \__($plugin->getMeta()->getName()),
                \__('Zahlung erfolgt im Sandbox Modus.'),
                Shop::getAdminURL() . '/plugin.php?kPlugin=' . $plugin->getID(),
                \md5($plugin->getID() . '_sandboxMode')
            );
            $entry->setPluginId($plugin->getPluginID());
            $notificationHelper->addNotify($entry);
        }
    }

    /**
     * @param Notification  $notificationHelper
     * @param Configuration $config
     */
    private function checkNotificationWebhook(Notification $notificationHelper, Configuration $config): void
    {
        $plugin         = $this->getPlugin();
        $webhookShopUrl = $config->getWebhookUrl();
        $webhookSeoLink = (new Webhook($this->getPlugin(), $config))->getWebHookURL();

        if ($webhookShopUrl !== $webhookSeoLink) {
            $webhookTab   = $plugin->getAdminMenu()->getItems()->filter(function ($elem) {
                return \strtoupper($elem->name) === \strtoupper('WEBHOOK');
            })->first();
            $webhookTabID = !empty($webhookTab->id) ? '#plugin-tab-' . $webhookTab->id : '';
            $entry        = new NotificationEntry(
                NotificationEntry::TYPE_DANGER,
                \__($plugin->getMeta()->getName()),
                \__('Bitte führen Sie eine Neuregistrierung Ihrer Webhook-URL durch!'),
                Shop::getAdminURL() . '/plugin.php?kPlugin=' . $plugin->getID() . $webhookTabID
            );
            $entry->setPluginId($plugin->getPluginID());
            $notificationHelper->addNotify($entry);
        }
    }

    /**
     * @inheritDoc
     */
    public function prepareFrontend(LinkInterface $link, JTLSmarty $smarty): bool
    {
        if ($link->getTemplate() === 'onboarding.tpl') {
            // Attention: this is a frontend link and is therefore executed in frontend context!
            Controller::getInstance($this->getPlugin(), $this->getDB())->run('FinishOnboarding');

            return true;
        }
        /** @var Configuration $config */
        $config = Shop::Container()->get(Configuration::class);
        if ($link->getTemplate() === 'pendingpayment.tpl') {
            parent::prepareFrontend($link, $smarty);

            $handler = new FrontendHandler($this->getPlugin(), $this->getDB(), $config);
            $handler->checkPaymentState($link, $smarty);

            return true;
        }
        if ($link->getTemplate() === 'webhook_PayPalCommerce.tpl') {
            $webhook = new Webhook($this->getPlugin(), $config);
            $webhook->handleCall(
                $config->getWebhookId(),
                \file_get_contents('php://input')
            );

            return true;
        }
        if ($link->getTemplate() === 'expresscheckout.tpl') {
            $handler = new FrontendHandler($this->getPlugin(), $this->getDB(), $config);
            $handler->handleECSOrder();

            return true;
        }

        return parent::prepareFrontend($link, $smarty);
    }
}
