<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\frontend;

use JTL\Cart\Cart;
use JTL\Catalog\Product\Artikel;
use JTL\Customer\Customer;
use JTL\Helpers\Request;
use Plugin\jtl_paypal_commerce\paymentmethod\Helper;
use Plugin\jtl_paypal_commerce\PPC\BackendUIsettings;
use Plugin\jtl_paypal_commerce\PPC\Order\AppContext;
use Plugin\jtl_paypal_commerce\PPC\Order\Transaction;

/**
 * Class PPCPFrontend
 * @package Plugin\jtl_paypal_commerce\frontend
 */
class PPCPFrontend extends AbstractPaymentFrontend
{
    /**
     * @inheritDoc
     */
    public function renderProductDetailsPage(Customer $customer, Cart $cart, ?Artikel $product): void
    {
        $components = [];

        if ($this->paymentMethod->isValidBannerProduct($customer, $product)
            && $this->frontend->renderInstalmentBanner(CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS)
        ) {
            $this->frontend->preloadInstalmentBannerJS(CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS);
            $components[] = BackendUIsettings::COMPONENT_MESSAGES;
        }
        if ($this->paymentMethod->isValidExpressProduct($customer, $product)
            && $this->frontend->renderProductDetailsButtons()
        ) {
            $components[] = BackendUIsettings::COMPONENT_BUTTONS;
            $components[] = BackendUIsettings::COMPONENT_FUNDING_ELIGIBILITY;
        }

        $components = $this->frontend->renderMiniCartComponents($this->paymentMethod, $components, $customer, $cart);
        if (count($components) > 0) {
            $this->frontend->preloadECSJS(CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS);
            $this->frontend->renderPayPalJsSDK($components, $this->paymentMethod->getBNCode(), false, true);
        }
    }

    /**
     * @inheritDoc
     */
    public function renderCartPage(Customer $customer, Cart $cart): void
    {
        if (!$this->paymentMethod->isValid($customer, $cart)) {
            return;
        }

        $components = [];

        if ($this->paymentMethod->isValidBannerPayment($customer, $cart)
            && $this->frontend->renderInstalmentBanner(CheckoutPage::PAGE_SCOPE_CART)
        ) {
            $this->frontend->preloadInstalmentBannerJS(CheckoutPage::PAGE_SCOPE_CART);
            $components[] = BackendUIsettings::COMPONENT_MESSAGES;
        }
        if ($this->paymentMethod->isValidExpressPayment($customer, $cart) && $this->frontend->renderCartButtons()) {
            $components[] = BackendUIsettings::COMPONENT_BUTTONS;
            $components[] = BackendUIsettings::COMPONENT_FUNDING_ELIGIBILITY;
        }

        $components = $this->frontend->renderMiniCartComponents($this->paymentMethod, $components, $customer, $cart);
        if (count($components) > 0) {
            $this->frontend->preloadECSJS(CheckoutPage::PAGE_SCOPE_CART);
            $this->frontend->renderPayPalJsSDK($components, $this->paymentMethod->getBNCode(), false, true);
        }
    }

    /**
     * @inheritDoc
     */
    public function renderAddressPage(Customer $customer, Cart $cart): void
    {
        if (!$this->paymentMethod->isValid($customer, $cart)) {
            return;
        }

        $components = [];
        $this->frontend->preloadECSJS(CheckoutPage::PAGE_SCOPE_ORDERPROCESS);
        if ($this->paymentMethod->isValidExpressPayment($customer, $cart)
            && $this->frontend->renderOrderProcessButtons()
        ) {
            $components[] = BackendUIsettings::COMPONENT_BUTTONS;
            $components[] = BackendUIsettings::COMPONENT_FUNDING_ELIGIBILITY;
        }
        $this->frontend->renderPayPalJsSDK($components, $this->paymentMethod->getBNCode(), false, true);
    }

    /**
     * @inheritDoc
     */
    public function renderShippingPage(Customer $customer, Cart $cart): void
    {
        if (!$this->paymentMethod->isValid($customer, $cart)) {
            return;
        }

        Transaction::instance()->clearAllTransactions();
        $components = [
            BackendUIsettings::COMPONENT_BUTTONS,
            BackendUIsettings::COMPONENT_FUNDING_ELIGIBILITY,
            BackendUIsettings::COMPONENT_MARKS,
            BackendUIsettings::COMPONENT_HOSTED_FIELDS,
        ];
        $this->frontend->renderPaymentButtons($this->paymentMethod);

        if ($this->paymentMethod->isValidBannerPayment($customer, $cart)
            && $this->frontend->renderInstalmentBanner(CheckoutPage::PAGE_SCOPE_ORDERPROCESS)
        ) {
            $this->frontend->preloadInstalmentBannerJS(CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS);
            $components[] = BackendUIsettings::COMPONENT_MESSAGES;
        }

        $this->frontend->renderPayPalJsSDK($components, $this->paymentMethod->getBNCode());
    }

    /**
     * @inheritDoc
     */
    public function renderConfirmationPage(int $paymentId, Customer $customer, Cart $cart): void
    {
        if ($this->paymentMethod->getMethod()->getMethodID() !== $paymentId) {
            return;
        }

        $fundingSource = Request::postVar('ppc-funding-source', '');
        if ($fundingSource === '' && Request::verifyGPCDataInt('wk') === 1) {
            // direct call from 1-click-checkout has no founding source and is not allowed
            Helper::redirectAndExit($this->paymentMethod->getPaymentCancelURL());
            exit();
        }

        $ppcOrderId = $this->paymentMethod->createPPOrder(
            $customer,
            $cart,
            $fundingSource,
            AppContext::SHIPPING_PROVIDED,
            AppContext::PAY_NOW,
            $this->paymentMethod->getBNCode()
        );
        if ($ppcOrderId === null) {
            // ToDo: Frontend error message
            Helper::redirectAndExit($this->paymentMethod->getPaymentCancelURL());
            exit();
        }
        $this->frontend->renderOrderConfirmationButtons($this->paymentMethod, $ppcOrderId);
        $this->frontend->renderPayPalJsSDK([
            BackendUIsettings::COMPONENT_BUTTONS
        ], $this->paymentMethod->getBNCode());
    }
}
