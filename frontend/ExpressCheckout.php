<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\frontend;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use JTL\Checkout\Bestellung;
use JTL\Customer\CustomerGroup;
use JTL\Helpers\ShippingMethod;
use JTL\Plugin\Data\PaymentMethod;
use JTL\Session\Frontend;
use JTL\Shop;
use Plugin\jtl_paypal_commerce\paymentmethod\PaymentmethodNotFoundException;
use Plugin\jtl_paypal_commerce\paymentmethod\PayPalPaymentInterface;
use Plugin\jtl_paypal_commerce\PPC\Authorization\MerchantCredentials;
use Plugin\jtl_paypal_commerce\PPC\BackendUIsettings;
use Plugin\jtl_paypal_commerce\PPC\Configuration;
use Plugin\jtl_paypal_commerce\PPC\Order\Address;
use Plugin\jtl_paypal_commerce\PPC\Order\Payer;
use Plugin\jtl_paypal_commerce\PPC\Order\Shipping;
use Plugin\jtl_paypal_commerce\PPC\Request\UnexpectedResponseException;

/**
 * Class ExpressCheckout
 * @package Plugin\jtl_paypal_commerce\frontend
 */
class ExpressCheckout
{
    /** @var PayPalPaymentInterface */
    private $payMethod;

    /** @var Configuration */
    private $config;

    /**
     * ExpressCheckout constructor.
     * @param PayPalPaymentInterface $payMethod
     * @param Configuration|null     $configuration
     */
    public function __construct(PayPalPaymentInterface $payMethod, ?Configuration $configuration = null)
    {
        $this->payMethod = $payMethod;
        $this->config    = $configuration ?? Shop::Container()->get(Configuration::class);
    }

    /**
     * @param string[] $address
     * @return object
     */
    private static function extractStreet(array $address): object
    {
        $street = \array_shift($address);
        $result = (object)[
            'name'    => '',
            'number'  => '',
            'name2'   => \array_shift($address) ?? '',
        ];
        if ($street !== null) {
            $re     = "/^(\\d*[\\wäöüß&; '\\-.]+)[,\\s]+(\\d+)\\s*([\\wäöüß&;\\-\\/]*)$/ui";
            $number = '';
            if (\preg_match($re, $street, $matches)) {
                $offset = \mb_strlen($matches[1]);
                $number = \mb_substr($street, $offset);
                $street = \mb_substr($street, 0, $offset);
            }

            $result->name   = \trim($street, '-:, ');
            $result->number = \trim($number, '-:, ');
        }

        return $result;
    }

    /**
     * @param string $name
     * @return object
     */
    private static function extractName(string $name): object
    {
        $parts = \explode(' ', $name, 2);
        if (\count($parts) === 1) {
            \array_unshift($parts, '');
        }

        return (object)[
            'givenName' => \trim($parts[0]),
            'surName'   => \trim($parts[1]),
        ];
    }

    /**
     * @param Address $address
     * @param array   $data
     */
    private static function mapAddressData(Address $address, array &$data): void
    {
        $street = self::extractStreet($address->getAddress());

        $data['strasse']      = $street->name;
        $data['hausnummer']   = $street->number;
        $data['adresszusatz'] = $street->name2;
        $data['bundesland']   = $address->getState();
        $data['plz']          = $address->getPostalCode();
        $data['ort']          = $address->getCity();
        $data['land']         = $address->getCountryCode();
    }

    /**
     * @param Payer $payer
     * @return array
     */
    public static function mapCustomerPostData(Payer $payer): array
    {
        $data    = [];
        $address = $payer->getAddress();

        $data['anrede']   = '';
        $data['vorname']  = $payer->getGivenName();
        $data['nachname'] = $payer->getSurname();
        $data['tel']      = $payer->getPhone()->getNumber();
        $data['email']    = $payer->getEmail();

        if ($address !== null) {
            self::mapAddressData($address, $data);
        }

        return $data;
    }

    /**
     * @param Shipping $shipping
     * @return array
     */
    public static function mapShippingPostData(Shipping $shipping): array
    {
        // ToDo: Handle phone number and email
        $data    = [];
        $address = $shipping->getAddress();
        $name    = self::extractName($shipping->getName());
        $conf    = Shop::getSettings([\CONF_KUNDEN]);

        $data['anrede']   = '';
        $data['vorname']  = $name->givenName;
        $data['nachname'] = $name->surName;
        $data['email']    = '';

        foreach (['tel', 'mobil', 'fax'] as $telType) {
            if ($conf['kunden']['lieferadresse_abfragen_' . $telType] !== 'N') {
                $data[$telType] = '';
            }
        }

        self::mapAddressData($address, $data);

        return $data;
    }

    /**
     * @param array $customerPost
     * @return array
     */
    private function checkoutCustomer(array $customerPost): array
    {
        $result   = \checkKundenFormularArray($customerPost, 0);
        $customer = \getKundendaten($customerPost, 0);

        $customer->kKundengruppe = Frontend::getCustomerGroup()->getID();
        $customer->kSprache      = Shop::getLanguageID();
        $customer->cAbgeholt     = 'N';
        $customer->cAktiv        = 'Y';
        $customer->cSperre       = 'N';
        $customer->nRegistriert  = 1;
        $customer->dErstellt     = \date_format(\date_create(), 'Y-m-d');

        unset($result['captcha']); // ignore captcha validation
        if (\count($result) === 0) {
            Frontend::set('Kunde', $customer);
        }

        return $result;
    }

    /**
     * @param array   $customerPost
     * @param array   $missingData
     * @param Address $address
     * @return array
     */
    private function applyMissingCustomer(array $customerPost, array $missingData, Address $address): array
    {
        $street = self::extractStreet($address->getAddress());
        if (isset($missingData['strasse'])) {
            $customerPost['strasse'] = $street->name;
        }
        if (isset($missingData['hausnummer'])) {
            $customerPost['hausnummer'] = $street->number;
        }
        if (isset($missingData['plz'])) {
            $customerPost['plz'] = $address->getPostalCode();
        }
        if (isset($missingData['ort'])) {
            $customerPost['ort'] = $address->getCity();
        }
        if (isset($missingData['land'])) {
            $customerPost['land'] = $address->getCountryCode();
        }

        return $customerPost;
    }

    /**
     * @param array $shippingPost
     * @return array
     */
    private function checkoutShipping(array $shippingPost): array
    {
        $result = \checkLieferFormularArray($shippingPost);
        $order  = Frontend::get('Bestellung') ?? new Bestellung();
        if (\count($result) === 0) {
            Frontend::set('Lieferadresse', \getLieferdaten($shippingPost));
            $order->kLieferadresse = -1;
        }
        Frontend::set('Bestellung', $order);

        return $result;
    }

    /**
     * @param Address       $address
     * @param PaymentMethod $payMethod
     */
    private function checkoutShippingMethod(Address $address, PaymentMethod $payMethod): void
    {
        $customerGroupId = Frontend::getCustomer()->getGroupID();
        $shippingMethod  = ShippingMethod::getFirstShippingMethod(
            ShippingMethod::getPossibleShippingMethods(
                $address->getCountryCode(),
                $address->getPostalCode(),
                ShippingMethod::getShippingClasses(Frontend::getCart()),
                $customerGroupId > 0 ? $customerGroupId : CustomerGroup::getDefaultGroupID()
            ),
            $payMethod->getMethodID()
        );

        if ($shippingMethod !== null) {
            Frontend::set('Versandart', $shippingMethod);
            Frontend::set('AktiveVersandart', $shippingMethod->kVersandart);
        }
    }

    /**
     * @param PaymentMethod $payMethod
     * @return array
     */
    private function checkoutPayment(PaymentMethod $payMethod): array
    {
        Frontend::set('Zahlungsart', $payMethod);
        Frontend::set('AktiveZahlungsart', $payMethod->getMethodID());

        return [
            'Zahlungsart'     => $payMethod->getMethodID(),
            'zahlungsartwahl' => '1',
        ];
    }

    /**
     * @return bool
     * @throws PaymentmethodNotFoundException
     */
    public function ecsCheckout(): bool
    {
        /** @noinspection PhpIncludeInspection */
        require_once \PFAD_INCLUDES . 'bestellvorgang_inc.php';

        try {
            if (($ppOrder = $this->payMethod->getPPOrder()) === null) {
                throw new Exception('Paymentmethod PayPalCommerce not found');
            }
            $ecsOrder = $this->payMethod->verifyPPOrder($ppOrder->getId());
            $payer    = $ecsOrder->getPayer();
            $shipping = $ecsOrder->getPurchase()->getShipping();
            $ppOrder->setStatus($ecsOrder->getStatus());
        } catch (Exception | GuzzleException | UnexpectedResponseException $e) {
            throw new PaymentmethodNotFoundException('Paymentmethod PayPalCommerce not found', $e->getCode(), $e);
        }

        $method         = $this->payMethod->getMethod();
        $customer       = Frontend::getCustomer();
        $handleAddress  = $this->config->getPrefixedConfigItem(
            BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY . '_handleInvoiceAddress',
            'E'
        );
        $missingData    = [];
        $customerPost   = [];
        $paymentOptions = $this->checkoutPayment($method);
        if (empty($customer->kKunde) && $payer !== null) {
            $customerPost = self::mapCustomerPostData($payer);
            $missingData  = \array_merge($missingData, $this->checkoutCustomer($customerPost));
            if ($shipping !== null && $handleAddress === 'Y' && \count($missingData) > 0) {
                $customerPost = $this->applyMissingCustomer($customerPost, $missingData, $shipping->getAddress());
                $missingData  = \array_merge([], $this->checkoutCustomer($customerPost));
            }
            if (\count($missingData) === 0) {
                $this->checkoutShippingMethod($payer->getAddress(), $method);
            } elseif ($shipping !== null && $handleAddress !== 'N') {
                $customerPost = $this->applyMissingCustomer($customerPost, $missingData, $shipping->getAddress());
            }
        }
        if ($shipping !== null) {
            $shippingPost = self::mapShippingPostData($shipping);
            $missingData  = \array_merge($missingData, $this->checkoutShipping($shippingPost));
            $this->checkoutShippingMethod($shipping->getAddress(), $method);
        }

        $this->payMethod->addCache('BNCode', MerchantCredentials::BNCODE_EXPRESS);
        if (\angabenKorrekt($missingData) === 0) {
            unset($_SESSION['Versandart'], $_SESSION['Zahlungsart'], $_SESSION['Lieferadresse']);
            $_SESSION['Bestellung']->kLieferadresse = 0;
            $_SESSION['checkout.register']          = 1;
            $_SESSION['checkout.cPost_arr']         = \array_merge($customerPost, ['shipping_address' => 0]);
            if ($handleAddress === 'N') {
                $_SESSION['checkout.fehlendeAngaben'] = $missingData;
            }

            return false;
        }

        \pruefeZahlungsartwahlStep($paymentOptions);

        return true;
    }
}
