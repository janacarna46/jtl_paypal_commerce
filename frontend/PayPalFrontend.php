<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\frontend;

use Exception;
use JTL\Cart\Cart;
use JTL\Customer\Customer;
use JTL\Exceptions\CircularReferenceException;
use JTL\Exceptions\ServiceNotFoundException;
use JTL\Link\LinkInterface;
use JTL\Plugin\PluginInterface;
use JTL\Session\Frontend;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_paypal_commerce\paymentmethod\Helper;
use Plugin\jtl_paypal_commerce\paymentmethod\PayPalPaymentInterface;
use Plugin\jtl_paypal_commerce\PPC\APM;
use Plugin\jtl_paypal_commerce\PPC\Authorization\ClientToken;
use Plugin\jtl_paypal_commerce\PPC\Authorization\MerchantCredentials;
use Plugin\jtl_paypal_commerce\PPC\BackendUIsettings;
use Plugin\jtl_paypal_commerce\PPC\Configuration;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderStatus;
use stdClass;
use function Functional\first;

/**
 * Class PayPalFrontend
 * @package Plugin\jtl_paypal_commerce\frontend
 */
class PayPalFrontend
{
    /** @var PluginInterface */
    private PluginInterface $plugin;

    /** @var Configuration */
    private Configuration $config;

    /** @var JTLSmarty */
    private JTLSmarty $smarty;

    /**
     * PayPalFrontend constructor
     * @param PluginInterface $plugin
     * @param Configuration   $config
     * @param JTLSmarty       $smarty
     */
    public function __construct(PluginInterface $plugin, Configuration $config, JTLSmarty $smarty)
    {
        $this->plugin = $plugin;
        $this->config = $config;
        $this->smarty = $smarty;
    }

    /**
     * @param string $page
     * @return bool
     */
    public function preloadECSJS(string $page): bool
    {
        $config = $this->config->mapFrontendSettings(BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY);

        if (!$this->config->checkComponentVisibility($config, $page) &&
            !$this->config->checkComponentVisibility($config, CheckoutPage::PAGE_SCOPE_MINICART)
        ) {
            return false;
        }
        try {
            \pq('body')->prepend(
                "<script src='" .
                $this->plugin->getPaths()->getFrontendURL() .
                "template/ecs/jsTemplates/standaloneButtonTemplate.js'></script>
                <script src='" .
                $this->plugin->getPaths()->getFrontendURL() .
                "template/ecs/jsTemplates/activeButtonLabelTemplate.js'></script>
                <script src='" .
                $this->plugin->getPaths()->getFrontendURL() .
                "template/ecs/init.js'></script>"
            );
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param string $page
     * @return bool
     */
    public function preloadInstalmentBannerJS(string $page): bool
    {
        $config = $this->config->mapFrontendSettings(BackendUIsettings::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP);

        if (!$this->config->checkComponentVisibility($config, $page) &&
            !$this->config->checkComponentVisibility($config, CheckoutPage::PAGE_SCOPE_MINICART)
        ) {
            return false;
        }
        try {
            \pq('body')->prepend(
                "<script src='" .
                $this->plugin->getPaths()->getFrontendURL() .
                "template/instalmentBanner/jsTemplates/instalmentBannerPlaceholder.js'></script>"
            );
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param string $page
     * @return bool
     */
    public function renderInstalmentBanner(string $page): bool
    {
        $mappedConfigs = $this->config->mapFrontendSettings(null, null, [
            BackendUIsettings::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP,
            BackendUIsettings::BACKEND_SETTINGS_SECTION_GENERAL
        ]);
        $config        = $mappedConfigs[BackendUIsettings::BACKEND_SETTINGS_SECTION_INSTALMENTBANNERDISP];
        try {
            $amount = $page === CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS ?
                \pq('meta[itemprop="price"]')->attr('content') :
                Frontend::getCart()->gibGesamtsummeWaren(!Frontend::getCustomerGroup()->isMerchant());
        } catch (Exception $e) {
            return false;
        }

        if (!$this->config->checkComponentVisibility($config, $page) || $amount < $config[$page . '_minAmount']) {
            return false;
        }

        $method   = $config[$page . '_phpqMethod'];
        $selector = $config[$page . '_phpqSelector'];

        $ppcFrontendPath = $this->plugin->getPaths()->getFrontendPath();

        $ppcStyle = [
            'placement' => 'product',
            'amount' => $amount,
            'style' => [
                'layout' => $config[$page . '_layout'],
                'logo' => [
                    'type' => $config[$page . '_logoType']
                ],
                'text' => [
                    'size' => $config[$page . '_textSize'],
                    'color' => $config[$page . '_textColor']
                ],
                'color' => $config[$page . '_layoutType'],
                'ratio' => $config[$page . '_layoutRatio']

            ]
        ];
        $ppcActiveTemplate = $mappedConfigs[BackendUIsettings::BACKEND_SETTINGS_SECTION_GENERAL]['templateSupport'];
        try {
            \pq($selector)->{$method}($this->smarty
                ->assign('ppcStyle', $ppcStyle)
                ->assign('ppcFrontendUrl', $this->plugin->getPaths()->getFrontendURL())
                ->assign('ppcFrontendPath', $ppcFrontendPath)
                ->assign('ppcConsentPlaceholder', $this->plugin->getLocalization()->getTranslation(
                    'jtl_paypal_commerce_instalment_banner_consent_placeholder'
                ))
                ->assign('ppcComponentName', $page)
                ->assign('ppcActiveTemplate', $ppcActiveTemplate)
                ->fetch($ppcFrontendPath . 'template/instalmentBanner/' . $page . '/banner.tpl'));
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param array       $components
     * @param string|null $bnCode
     * @param bool        $ppcCommit
     * @param bool        $isECS
     * @return void
     * @throws CircularReferenceException
     * @throws ServiceNotFoundException
     */
    public function renderPayPalJsSDK(
        array $components,
        ?string $bnCode = null,
        bool $ppcCommit = true,
        bool $isECS = false
    ): void {
        $logger     = Shop::Container()->getLogService();
        $components = \array_values(\array_unique($components));
        if (count($components) === 0) {
            return;
        }

        try {
            $locale = Frontend::getInstance()->getLanguage()->cISOSprache === 'ger' ? 'de_DE' : 'en_GB';
        } catch (Exception $e) {
            $locale = 'en_GB';
        }
        $cmActive          = Shop::getSettingValue(\CONF_CONSENTMANAGER, 'consent_manager_active') ?? 'N';
        $ppcConsentActive  = $cmActive === 'Y' && $this->config->getPrefixedConfigItem(
            BackendUIsettings::BACKEND_SETTINGS_SECTION_CONSENTMANAGER . '_activate'
        ) === 'Y';
        $ppcConsentGiven   = Shop::Container()->getConsentManager()->hasConsent(Configuration::CONSENT_ID);
        $config            = $this->config->mapFrontendSettings(BackendUIsettings::BACKEND_SETTINGS_SECTION_GENERAL);
        $ppcActiveTemplate = $config['templateSupport'];
        $APMs              = new APM($this->config);
        $clientToken       = null;

        if (\in_array(BackendUIsettings::COMPONENT_HOSTED_FIELDS, $components, true)) {
            try {
                $clientToken = ClientToken::getInstance()->getToken();
            } catch (Exception $e) {
                $logger->addRecord(
                    $logger::ERROR,
                    'fetch clientToken failed: ' . $e->getMessage()
                );
            }
        }

        try {
            \pq('body')
                ->append($this->smarty
                    ->assign('ppcComponents', $components)
                    ->assign('ppcFrontendUrl', $this->plugin->getPaths()->getFrontendURL())
                    ->assign('ppcCommit', $ppcCommit ? 'true' : 'false')
                    ->assign('ppcFundingDisabled', $APMs->getDisabled($isECS))
                    ->assign('ppcOrderLocale', $locale)
                    ->assign('ppcClientID', $this->config->getClientID())
                    ->assign('ppcClientToken', $clientToken)
                    ->assign('ppcConsentID', Configuration::CONSENT_ID)
                    ->assign('ppcConsentActive', $ppcConsentActive ? 'true' : 'false')
                    ->assign('ppcCurrency', Frontend::getCurrency()->getCode())
                    ->assign('ppcConsentGiven', $ppcConsentGiven ? 'true' : 'false')
                    ->assign('ppcActiveTemplate', $ppcActiveTemplate)
                    ->assign('ppcBNCode', $bnCode ?? MerchantCredentials::BNCODE_CHECKOUT)
                    ->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/paypalJsSDK.tpl'));
        } catch (Exception $e) {
            $logger->addRecord(
                $logger::ERROR,
                'phpquery rendering failed: renderPayPalJsSDK()'
            );

            return;
        }
    }

    /**
     * @return void
     */
    private function ECSDefaultSmartyAssignments() : void
    {
        /** @var LinkInterface $ecsLink */
        $ecsLink           = $this->plugin->getLinks()->getLinks()->first(static function (LinkInterface $link) {
            return $link->getTemplate() === 'expresscheckout.tpl';
        });
        $templateConfig    = $this->config->mapFrontendSettings(BackendUIsettings::BACKEND_SETTINGS_SECTION_GENERAL);
        $ppcActiveTemplate = $templateConfig['templateSupport'];

        $this->smarty
            ->assign('ppcFrontendUrl', $this->plugin->getPaths()->getFrontendURL())
            ->assign('ppcClientID', $this->config->getClientID())
            ->assign('ppcActiveTemplate', $ppcActiveTemplate)
            ->assign('ppcECSUrl', $ecsLink !== null ? $ecsLink->getURL() : '')
            ->assign(
                'ppcPreloadButtonLabelInactive',
                $this->plugin->getLocalization()->getTranslation(
                    'jtl_paypal_commerce_ecs_preload_button_label_inactive'
                )
            )
            ->assign(
                'ppcLoadingPlaceholder',
                $this->plugin->getLocalization()->getTranslation(
                    'jtl_paypal_commerce_ecs_loading_placeholder'
                )
            )
            ->assign(
                'ppcPreloadButtonLabelActive',
                $this->plugin->getLocalization()->getTranslation(
                    'jtl_paypal_commerce_ecs_preload_button_label_active'
                )
            );
    }

    /**
     * @return bool
     */
    private function renderMiniCartButtons(): bool
    {
        $config         = $this->config->mapFrontendSettings(null, null, [
            BackendUIsettings::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS,
            BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY
        ]);
        $expressBuyConf = $config[BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY];

        if (!$this->config->checkComponentVisibility($expressBuyConf, CheckoutPage::PAGE_SCOPE_MINICART)) {
            return false;
        }

        $method   = $expressBuyConf[CheckoutPage::PAGE_SCOPE_MINICART . '_phpqMethod'];
        $selector = $expressBuyConf[CheckoutPage::PAGE_SCOPE_MINICART . '_phpqSelector'];
        $tplPath  = 'template/ecs/miniCart.tpl';
        try {
            $this->ECSDefaultSmartyAssignments();
            \pq($selector)
                ->{$method}($this->smarty
                    ->assign('ppcNamespace', CheckoutPage::PAGE_SCOPE_MINICART)
                    ->assign('ppcConfig', $config[BackendUIsettings::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS])
                    ->assign('ppcPrice', Frontend::getCart()->gibGesamtsummeWaren(
                        !Frontend::getCustomerGroup()->isMerchant()
                    ))
                    ->fetch($this->plugin->getPaths()->getFrontendPath() . $tplPath));
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param PayPalPaymentInterface $payment
     * @param array                  $components
     * @param Customer               $customer
     * @param Cart                   $cart
     * @return array
     */
    public function renderMiniCartComponents(
        PayPalPaymentInterface $payment,
        array $components,
        Customer $customer,
        Cart $cart
    ): array {
        if ($payment->isValidExpressPayment($customer, $cart) && $this->renderMiniCartButtons()) {
            $components[] = BackendUIsettings::COMPONENT_BUTTONS;
            $components[] = BackendUIsettings::COMPONENT_FUNDING_ELIGIBILITY;
        }
        if ($payment->isValidBannerPayment($customer, $cart)
            && $this->renderInstalmentBanner(CheckoutPage::PAGE_SCOPE_MINICART)
        ) {
            $components[] = BackendUIsettings::COMPONENT_MESSAGES;
        }

        return $components;
    }

    /**
     * @return bool
     */
    public function renderProductDetailsButtons(): bool
    {
        $config         = $this->config->mapFrontendSettings(null, null, [
            BackendUIsettings::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS,
            BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY
        ]);
        $expressBuyConf = $config[BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY];

        if (!$this->config->checkComponentVisibility($expressBuyConf, CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS)) {
            return false;
        }

        $method   = $expressBuyConf[CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS . '_phpqMethod'];
        $selector = $expressBuyConf[CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS . '_phpqSelector'];

        try {
            $this->ECSDefaultSmartyAssignments();
            $localization = $this->plugin->getLocalization();
            \pq($selector)->{$method}($this->smarty
                ->assign('ppcNamespace', CheckoutPage::PAGE_SCOPE_PRODUCTDETAILS)
                ->assign('ppcConfig', $config[BackendUIsettings::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS])
                ->assign('ecs_wk_error_desc', \htmlentities($localization->getTranslation(
                    'jtl_paypal_commerce_ecs_wk_error_desc'
                )))
                ->assign('ecs_wk_error_title', \htmlentities($localization->getTranslation(
                    'jtl_paypal_commerce_ecs_wk_error_title'
                )))
                ->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/ecs/productDetails.tpl'));
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function renderCartButtons(): bool
    {
        $config         = $this->config->mapFrontendSettings(null, null, [
            BackendUIsettings::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS,
            BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY
        ]);
        $expressBuyConf = $config[BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY];

        if (!$this->config->checkComponentVisibility($expressBuyConf, CheckoutPage::PAGE_SCOPE_CART)) {
            return false;
        }

        $method   = $expressBuyConf[CheckoutPage::PAGE_SCOPE_CART . '_phpqMethod'];
        $selector = $expressBuyConf[CheckoutPage::PAGE_SCOPE_CART . '_phpqSelector'];

        try {
            $this->ECSDefaultSmartyAssignments();
            \pq($selector)->{$method}($this->smarty
                ->assign('ppcNamespace', CheckoutPage::PAGE_SCOPE_CART)
                ->assign('ppcConfig', $config[BackendUIsettings::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS])
                ->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/ecs/cart.tpl'));
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function renderOrderProcessButtons(): bool
    {
        $config         = $this->config->mapFrontendSettings(null, null, [
            BackendUIsettings::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS,
            BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY
        ]);
        $expressBuyConf = $config[BackendUIsettings::BACKEND_SETTINGS_SECTION_EXPRESSBUYDISPLAY];

        if (!$this->config->checkComponentVisibility($expressBuyConf, CheckoutPage::PAGE_SCOPE_ORDERPROCESS)) {
            return false;
        }

        $method   = $expressBuyConf[CheckoutPage::PAGE_SCOPE_ORDERPROCESS . '_phpqMethod'];
        $selector = $expressBuyConf[CheckoutPage::PAGE_SCOPE_ORDERPROCESS . '_phpqSelector'];

        try {
            $this->ECSDefaultSmartyAssignments();
            \pq($selector)
                ->{$method}($this->smarty
                    ->assign('ppcNamespace', CheckoutPage::PAGE_SCOPE_ORDERPROCESS)
                    ->assign('ppcConfig', $config[BackendUIsettings::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS])
                    ->assign('ppcPrice', Frontend::getCart()->gibGesamtsummeWaren(
                        !Frontend::getCustomerGroup()->isMerchant()
                    ))
                    ->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/ecs/orderProcess.tpl'));
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param PayPalPaymentInterface $payment
     * @param string                 $ppcOrderId
     * @return bool
     * @noinspection JsonEncodingApiUsageInspection
     */
    public function renderOrderConfirmationButtons(PayPalPaymentInterface $payment, string $ppcOrderId): bool
    {
        $curMethod = Frontend::get('Zahlungsart');
        if ((int)$curMethod->kZahlungsart !== $payment->getMethod()->getMethodID()) {
            return false;
        }

        $ppOrder = $payment->getPPOrder($ppcOrderId);
        if ($ppOrder === null) {
            // ToDo: Frontend error message
            Helper::redirectAndExit($payment->getPaymentCancelURL());
            exit();
        }

        if ($ppOrder->getStatus() === OrderStatus::STATUS_APPROVED) {
            // Order is approved (from express checkout?) -  no call to paypal necessary
            try {
                \pq('#complete-order-button')
                    ->after($this->smarty
                        ->assign('ppcStateURL', $payment->getPaymentStateURL() ?? '')
                        ->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/orderConfirmationSimple.tpl'));
            } catch (Exception $e) {
            }

            return true;
        }

        $templateConfig    = $this->config->mapFrontendSettings(BackendUIsettings::BACKEND_SETTINGS_SECTION_GENERAL);
        $ppcActiveTemplate = $templateConfig['templateSupport'];

        try {
            \pq('body')->prepend(
                "<script src='" .
                $this->plugin->getPaths()->getFrontendURL() .
                "template/ecs/init.js'></script>"
            );
            $this->ECSDefaultSmartyAssignments();
            \pq('#complete-order-button')
                ->after($this->smarty
                    ->assign('ppcConfig', \json_encode($this->config->mapFrontendSettings()))
                    ->assign('ppcOrderId', $ppcOrderId)
                    ->assign('ppcActiveTemplate', $ppcActiveTemplate)
                    ->assign('ppcStateURL', $payment->getPaymentStateURL() ?? '#')
                    ->assign('ppcCancelURL', $payment->getPaymentCancelURL())
                    ->assign('ppcFundingSource', $payment->getFundingSource())
                    ->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/orderConfirmation.tpl'));
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param PayPalPaymentInterface $payment
     * @return bool
     * @noinspection JsonEncodingApiUsageInspection
     */
    public function renderPaymentButtons(PayPalPaymentInterface $payment): bool
    {
        $methods  = $this->smarty->getTemplateVars('Zahlungsarten');
        $methodID = $payment->getMethod()->getMethodID();
        $method   = first($methods, static function (stdClass $method) use ($methodID) {
            return $method->kZahlungsart === $methodID;
        });
        $ppOrder  = $payment->getPPOrder();

        if ($method === null) {
            return false;
        }

        $ppcFundingMethodsMapping = \json_encode(
            Helper::getInstance($this->plugin)->getFundingMethodsMapping(Shop::getLanguageCode())->toArray()
        );
        try {
            //Hide all preloaded payment methods before SmartPaymentButtons are rendered
            \pq('.checkout-payment-method')->addClass('d-none');
            \pq('.checkout-shipping-form')->append(
                '<input id="ppc-funding-source_input" type="hidden" name="ppc-funding-source" '
                . 'value="' . $payment->getFundingSource() . '">'
            );
            \pq('#fieldset-payment')->append('<div class="jtl-spinner"><i class="fa fa-spinner fa-pulse"></i></div>');
            \pq('#result-wrapper')->append(
                $this->smarty
                    ->assign('zahlungsart', $method)
                    ->assign('ppcFundingMethodsMapping', $ppcFundingMethodsMapping)
                    ->assign('ppcModuleID', $payment->getMethod()->getModuleID())
                    ->assign('ppcMethodName', $payment->getMethod()->getName())
                    ->assign('ppcFrontendUrl', $this->plugin->getPaths()->getFrontendURL())
                    ->assign('ppcPaymentMethodID', $payment->getMethod()->getMethodID())
                    ->assign('ppcFundingSource', $payment->getFundingSource())
                    ->assign('ppcSingleFunding', $ppOrder !== null
                        && $ppOrder->getStatus() === OrderStatus::STATUS_APPROVED)
                    ->assign(
                        'ppcActiveTemplate',
                        Shop::Container()->getTemplateService()->getActiveTemplate()->getName()
                    )
                    ->fetch($this->plugin->getPaths()->getFrontendPath() . 'template/paymentButtons.tpl')
            );
        } catch (Exception $e) {
            // ToDo: Error handling
        }

        return true;
    }
}
