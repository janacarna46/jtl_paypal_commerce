<div class="spinner-border mr-2" id="ppc-loading-spinner-confirmation" role="status">
    <span class="sr-only">Loading...</span>
</div>
<div id="paypal-button-container" class="d-flex justify-content-center align-items-center ppcOrderConfirmation"></div>
<script>
    {literal}
    if (typeof(window.PPCcomponentInitializations) === 'undefined') {
        window.PPCcomponentInitializations = [];
    }

    (function () {
        {/literal}
        let ppcFundingSource = '{$ppcFundingSource}',
            ppcConfig        = {$ppcConfig},
            buttonActions    = null,
            ppcOrderId       = '{$ppcOrderId}',
            ppcStateURL      = '{$ppcStateURL}',
            ppcCancelURL     = '{$ppcCancelURL}';
        {literal}
        if (ppcFundingSource !== '') {
            $('#complete-order-button').hide();
            $('#ppc-loading-spinner-confirmation').show();
        }
        window.PPCcomponentInitializations.push(initOrderConfirmationButtons);
        $(document).ready(function() {
            $(window).trigger('ppc:componentInit', [initOrderConfirmationButtons, true]);
            {/literal}
            {assign var=alertOvercapture value=$alertList->getAlert('handleOvercharge')}
            {if $alertOvercapture !== null}
            eModal.alert({
                message: '{$alertOvercapture->getMessage()|htmlentities}',
                title: '{$alertOvercapture->getLinkText()}',
                buttons: [
                    {
                        text: '{lang key='continueOrder' section='account data'}',
                        close: true,
                        click: function() {
                            $.evo.smoothScrollToAnchor('#complete_order', false);
                        }
                    }
                ],
            });
            {/if}
            {literal}
        });

        function initOrderConfirmationButtons(ppc_jtl) {
            initButtons(
                ppc_jtl,
                ppcConfig.smartPaymentButtons,
                null,
                renderStandaloneButton,
                null,
                null,
                null,
                true,
                ppcFundingSource
            );
        }

        function renderStandaloneButton(ppc_jtl,fundingSource,ppcConfig) {
            $('#ppc-loading-spinner-confirmation').hide();
            ppc_jtl.Buttons({
                fundingSource: fundingSource,
                style: ppcConfig,
                ...customEvents(buttonActions, ppcOrderId, ppcStateURL)
            }).render('#paypal-button-container');
        }

        const customEvents = (buttonActions, ppcOrderId, ppcStateURL) => {
            return {
                onInit: function (data, actions) {
                    buttonActions = actions;
                },
                onClick: function (data, actions) {
                    return $('#complete_order')[0].checkValidity()
                        ? actions.resolve()
                        : actions.reject();
                },
                createOrder: function (data, actions) {
                    return ppcOrderId;
                },
                onApprove: function (data, actions) {
                    history.pushState(null, null, ppcStateURL);
                    let commentField = $('#comment'),
                        commentFieldHidden = $('#comment-hidden');
                    if (commentField && commentFieldHidden) {
                        commentFieldHidden.val(commentField.val());
                    }
                    $('#ppc-loading-spinner-confirmation').show();
                    $('#paypal-button-container').addClass('opacity-half');
                    buttonActions.disable();
                    $('form#complete_order').submit();
                },
                onCancel: function (data, actions) {
                    history.pushState(null, null, ppcCancelURL);
                    window.location.reload();
                },
                onError: function (data, actions) {
                    history.pushState(null, null, ppcCancelURL);
                    window.location.reload();
                },
            }
        }
    })()
    {/literal}
</script>
