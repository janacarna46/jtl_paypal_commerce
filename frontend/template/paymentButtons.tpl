<script>
    if (typeof(window.PPCcomponentInitializations) === 'undefined') {
        window.PPCcomponentInitializations = [];
    }

    (function() {
        let methodID             = {$ppcPaymentMethodID};
        let ppcModuleID          = '{$ppcModuleID}';
        let ppcOption            = '{$ppcFundingSource}';
        let paymentFundsMapping  = JSON.parse('{$ppcFundingMethodsMapping}');
        let ppcSingleFunding     = {if $ppcSingleFunding}true{else}false{/if};
        let ppcPaymentResetTitle = ppcSingleFunding ? '{__('Zahlungsart ändern')}' : '';
        let activePaymentMethod  = {if is_int($AktiveZahlungsart)}{$AktiveZahlungsart}{else}'{$AktiveZahlungsart}'{/if};
        let initInProgress       = false;
        let paymentFeeName       = '{if $zahlungsart->cGebuehrname|has_trans}{$zahlungsart->cGebuehrname|trans}{/if}';
        let paymentPriceLocalized = '{$zahlungsart->cPreisLocalized}';

        const paymentButtonsOptionTemplate = fundingSource => {
            let paymentOptionId = 'za_ppc_' + fundingSource;
            let fundingSourceTitle = paymentFundsMapping[fundingSource];

            let resetButton = ppcPaymentResetTitle !== ''
                ? `<div class="custom-control custom-button custom-control-inline">
                    <button class="btn btn-outline-secondary btn-block" name="resetPayment" value="1"
                    type="submit">${ ppcPaymentResetTitle}</button>
                    <\/div>`
                : '';
            return `
                <div class="col checkout-payment-method col-12 ppc-checkout-payment-method" id="${ paymentOptionId }">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input ppc-funding-source="${ fundingSource }" class="custom-control-input " type="radio"
                         id="${ paymentOptionId }_input" name="Zahlungsart" value="${ methodID }">
                        <label for="${ paymentOptionId }_input" class="custom-control-label d-flex align-items-center">
                            <div id="${ paymentOptionId }_img" class="align-items-center d-inline-block ppc-option-img"><\/div>
                            <div class="funding-name d-inline-block ml-1">
                                ${ fundingSourceTitle }
                            <\/div>
                            {if $zahlungsart->fAufpreis != 0}
                                <strong class="checkout-payment-method-badge">
                                    ${ paymentFeeName ? '<span>' + paymentFeeName + '<\/span>' : ''}
                                    ${ paymentPriceLocalized }
                                <\/strong>
                            {/if}
                        <\/label>
                    <\/div>
                    ${ resetButton }
                <\/div>
            `;
        }


        window.PPCcomponentInitializations.push(initShippingSelectionButtons);

        $(document).ready(function() {
            $(window).trigger('ppc:componentInit',[initShippingSelectionButtons, true]);
        });
        function initShippingSelectionButtons(ppc_jtl)
        {
            if (initInProgress) {
                return;
            }
            initInProgress = true;
            let initCallback = function () {
                let ppcPaymentContainer = $('#' + ppcModuleID);

                if (typeof ppc_jtl !== 'undefined') {
                    let ppcInserted = false;
                    let fundingSources;

                    try {
                        fundingSources = ppc_jtl.getFundingSources();
                    } catch (err) {
                        fundingSources = [];
                        $('.checkout-payment-method').removeClass('d-none');
                    }

                    let eligibleOptionIds = [];

                    fundingSources.sort(function (a, b) {
                        return b === 'paypal' ? -1 : 0;
                    }).forEach(function (fundingSource) {
                        if (typeof fundingSource === 'undefined'
                            || typeof paymentFundsMapping[fundingSource] === 'undefined'
                        ) {
                            return;
                        }
                        let mark = ppc_jtl.Marks({ fundingSource: fundingSource });
                        if (mark.isEligible() && (!ppcSingleFunding || ppcOption === fundingSource) &&
                            !ppcFundingDisabled.includes(fundingSource)
                        ) {
                            ppcInserted = true;
                            let paymentOptionId = 'za_ppc_' + fundingSource;
                            let template = paymentButtonsOptionTemplate(fundingSource);
                            eligibleOptionIds.push(paymentOptionId);
                            ppcPaymentContainer.after(template);
                            mark.render('#' + paymentOptionId + '_img');
                            if (methodID === activePaymentMethod) {
                                ppcOption = ppcOption === '' ? sessionStorage.getItem('chosenPPCPaymentOption') : ppcOption;
                                if (ppcOption !== null && ppcOption !== '') {
                                    $('#za_ppc_' + ppcOption + '_input').prop('checked', true);
                                }
                            }
                        }
                    });

                    setTimeout(() => {
                        let widestWidth = 0;

                        eligibleOptionIds.forEach((eligibleOptionId) => {
                            let elWidth = $('#' + eligibleOptionId + '_img').width();
                            if (widestWidth < elWidth) {
                                widestWidth = elWidth;
                            }
                        });

                        $('.ppc-option-img').each(function () {
                            $(this).css('width', widestWidth);
                        });
                    }, 0);

                    if (ppcInserted) {
                        ppcPaymentContainer.remove();
                        $('.checkout-payment-method').removeClass('d-none');
                    }

                    setTimeout(function () {
                        $('input[type=radio][name=Zahlungsart]').change(function () {
                            let attr = $(this).attr('ppc-funding-source');
                            let attrIsSet = typeof (attr) !== 'undefined' && attr !== false;

                            ppcOption = $(this).is(':checked') && attrIsSet ? attr : '';
                        });
                    });
                    $('#fieldset-payment .jtl-spinner').fadeOut(300,
                        function () {
                            $(this).remove();
                        }
                    );
                } else {
                    $('.checkout-payment-method').removeClass('d-none');
                }
                $('.checkout-shipping-form').on('submit', function (e) {
                    if (parseInt($('input[name="Zahlungsart"]:checked', $(this)).val()) === methodID) {
                        sessionStorage.setItem('chosenPPCPaymentOption', ppcOption);
                        $('#ppc-funding-source_input').val(ppcOption);
                    } else {
                        $('#ppc-funding-source_input').val('');
                    }
                });
            }

            initCallback();
        }
    })();
</script>
